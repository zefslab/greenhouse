﻿using System;

namespace ClimateControl.Api.Temperature
{
    [Serializable]
    public class BrokenSensorException : ApplicationException
    {
        public BrokenSensorException(){}

        public BrokenSensorException(string message) : base(message){}

        public BrokenSensorException(string message, Exception inner) : base(message, inner){}
    }
}
