﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClimateControl.Api
{
    /// <summary>
    /// Датчик 
    /// </summary>
    public interface ISensor
    {
        float GetValue();


    }
}
