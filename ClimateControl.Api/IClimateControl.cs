﻿using System.Collections.Generic;

namespace ClimateControl.Api.Temperature
{
    /// <summary>
    /// Система климат-контроля
    ///  </summary>
    public interface IClimateControl
    {
        float maxTemperatureHot { get; set; }
        float minTemperatureHot { get; set; }
        float maxTemperatureCold { get; set; }
        float minTemperatureCold { get; set; }
        /// <summary>
        /// Датчики температуры. Могут быть установлены датчики разного типа.
        /// Датчики могут быть установлены в разные места.
        /// </summary>
        IList<ITemperatureSensor> TemperatureSensors { get; set; }

        float GetTemperature(ITemperatureSensor sensor );

    }
}
