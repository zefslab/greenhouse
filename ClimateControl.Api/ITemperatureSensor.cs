﻿namespace ClimateControl.Api.Temperature
{
    /// <summary>
    /// Датчик температуры
    /// </summary>
    public interface ITemperatureSensor : ISensor
    {
        string Name { get; set; }
        /// <summary>
        /// Тип датчика температуры, если потребуется чисто для информации
        /// </summary>
        string Type { get; set; }
        
        /// <summary>
        /// Место установки датчика в теплице или за ее пределами
        /// TODO не понятно пока как и где указывать место установки датчика. Можно сделать
        /// сущность, котрая указывает место установки. 
        /// </summary>
        //GreenHouseSets Set { get; set; }
    }
}
