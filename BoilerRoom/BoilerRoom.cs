﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoilerRoom.Api;
using GreenHouse.Api;
using PumpRoom.Api;
using ClimateControl.Api.Temperature;

namespace BoilerRoom
{
    public class BoilerRoom : IBoilerRoom
    {
        public ITemperatureSensor OuterHeatSensor { private get; set; }
        public IList<IHeatGenerator> HeatGenerators { get; set; }
        public IList<IHeatConsumer> HeatConsumers { get; set; }
        public bool GetHeat(IHeatConsumer consumer)
        {
            throw new NotImplementedException();
        }

        public double GetOuterHeat()
        {
            double temperature;
            try
            {
                temperature = OuterHeatSensor.GetValue();
            }
            catch (BrokenSensorException ex)
            {
                // Отправить SMS уведомление хозяину о поломке
                throw ex;
            }
            

            if (temperature >= 95)
            {
                throw new OverHeatException();
            }
            return temperature;
        }
    }
}
