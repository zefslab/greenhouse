﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PumpRoom.Api
{
    public enum WaterType
    {
        Clean = 1,

        Technical = 2,

        Hot = 3
    }
}
