﻿namespace PumpRoom.Api
{
    /// <summary>
    /// Потребитель воды
    /// </summary>
    public interface IWaterConsumer
    {
        /// <summary>
        /// Номер вентиля, к которому подключен потребитель
        /// </summary>
        int ValveNumber { get; set; }
    }
}
