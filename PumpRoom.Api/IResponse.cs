﻿namespace PumpRoom.Api
{
    public interface IResponse
    {
        string Description { get; set; }
        bool OperationResult { get; set; }
    }
}