﻿using System.Collections.Generic;
using System.Dynamic;
using PumpRoom.Api;

namespace PumpRoom.Api
{
    /// <summary>
    /// Водоснабжающая станция
    /// </summary>
    public interface IPumpRoom
    {
        /// <summary>
        /// Зарегистрированные потребители воды
        /// </summary>
        IList<IWaterConsumer> WaterConsumers { get; set; }

        /// <summary>
        /// Запрос на подачу потребителю воды указанного качества
        /// </summary>
        /// <param name="consumer">потребитель</param>
        /// <param name="waterType">качество воды</param>
        /// <returns>возможность подачи воды</returns>
        IResponse GetWater(IWaterConsumer consumer, WaterType waterType);

        /// <summary>
        /// Запрос не перекрытие воды указанокго качества для конкретного потребителя
        /// </summary>
        /// <param name="consumer"></param>
        /// <param name="waterType"></param>
        /// <returns></returns>
        IResponse CutOfWater(IWaterConsumer consumer, WaterType waterType);

    }
}
