﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PumpRoom.Api
{
    /// <summary>
    /// Ответ подсистемы на запрос потребителя
    /// </summary>
    public class Response : IResponse
    {
        /// <summary>
        /// Результат выполнения зпрошенной операции
        /// </summary>
        public bool OperationResult { get; set; }

        /// <summary>
        /// Комментарий, в случае если результат отрицательный. Как правило может содержать
        /// причину невозможности выполнения запроса
        /// </summary>
        public string Description { get; set; }
             
    }
}
