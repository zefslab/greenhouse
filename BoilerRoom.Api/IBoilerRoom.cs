﻿using System.Collections.Generic;

namespace PumpRoom.Api
{
    /// <summary>
    /// Котельная, распределяющая тепло между потребителями.
    /// </summary>
    public interface IBoilerRoom
    {
        IList<IHeatGenerator> HeatGenerators { get; set; }

        IList<IHeatConsumer> HeatConsumers { get; set; }
        /// <summary>
        /// Запрос на подачу тепла указанному потребителю
        /// </summary>
        /// <param name="consumer">потребитель</param>
        /// <returns>возможность подачи тепла</returns>
        bool GetHeat(IHeatConsumer consumer);

        /// <summary>
        /// Возвращает показания температуры на выходе из системы
        /// </summary>
        /// <returns></returns>
        double GetOuterHeat();                

    }
}
