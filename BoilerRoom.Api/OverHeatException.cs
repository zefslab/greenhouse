﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoilerRoom.Api
{
    [Serializable]
    public class OverHeatException : ApplicationException
    {
        public OverHeatException()
        {
            
        }
        public OverHeatException(string message) : base(message)
        {

        }
        public OverHeatException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
