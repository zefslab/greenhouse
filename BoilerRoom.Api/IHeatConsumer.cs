﻿using System;

namespace PumpRoom.Api
{

    /// <summary>
    /// Объекты системы, являющиеся потребителями тепла
    /// </summary>
    public interface IHeatConsumer
    {
        /// <summary>
        /// Номер вентиля к которому подключен потребитель тепла.
        /// </summary>
        int ValveNumber { get; set; }

        event LowTemperatureHandler LowTemperature;
    }

    public delegate void LowTemperatureHandler(object sender, double temperature, EventArgs e);

}
