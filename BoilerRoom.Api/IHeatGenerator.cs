﻿namespace PumpRoom.Api
{
    /// <summary>
    /// Источник тепла. Возможны различные источники: твердотопливный, газовый, 
    /// электрический котлы, центральное отопление, солнечные батареи
    /// </summary>
    public interface IHeatGenerator
    {
    }
}
