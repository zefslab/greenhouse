﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PumpRoom.Api;

namespace BoilerRoom.Api
{
    public class BoilerRoom : IBoilerRoom
    {

        public void AddHeatConsummer(IHeatConsumer heatConsumer)
        {
            HeatConsumers.Add(heatConsumer);

            heatConsumer.LowTemperature += (sender, temperature, args) =>
            {
                GetHeat(heatConsumer);

                Console.WriteLine($"Включаем отопление потребителя тепла, т.к. температура {temperature} " +
                                  $"ниже установленного минимума");
            };
        }

        public IList<IHeatGenerator> HeatGenerators { get; set; }
        public IList<IHeatConsumer> HeatConsumers { get; set; }
        public bool GetHeat(IHeatConsumer consumer)
        {
            throw new NotImplementedException();
        }

        public double GetOuterHeat()
        {
            throw new NotImplementedException();
        }
    }
}
