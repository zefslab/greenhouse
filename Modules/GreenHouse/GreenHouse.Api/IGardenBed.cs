﻿using System;
using GreenHouse.Api;

namespace GreenHouse
{
    /// <summary>
    /// Грядка
    /// </summary>
    public interface IGardenBed
    {
        /// <summary>
        /// Растение посаженное на грядке
        /// </summary>
        IPlant Plant { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SwitchOn"></param>
        void Water(Boolean SwitchOn);
        
        /// <summary>
        /// Описание назначения грядки
        /// </summary>
        string Description { get; set; }

       
        

    }
}
