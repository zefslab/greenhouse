﻿namespace GreenHouse.Api
{
    public enum GreenHouseSets
    {
        Indoor = 1,
        Outdoor = 2,
        Underground = 3
    }
}
