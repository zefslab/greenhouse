﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClimateControl.Api.Temperature;
using PumpRoom.Api;

namespace GreenHouse.Api
{
    public  class BaseGreenHouse : IGreenHouse
    {
        int IHeatConsumer.ValveNumber { get; set; }
        int IWaterConsumer.ValveNumber { get; set; }
        public string Name { get; set; }
        public IList<IGardenBed> GardenBeds { get; set; }
        public IClimateControl ClimateControl { get; set; }
        public float GetTemperature(GreenHouseSets set)
        {
            return ReadTemperature();
        }

        private float ReadTemperature()
        {
            var temperature = 0.0F;
            // делается запрос непосредственно к датчику через драйвер

            if (temperature < ClimateControl.minTemperatureCold)
            {
                LowTemperature(this, temperature, new EventArgs());
            }

            return 0;
        }

        public event LowTemperatureHandler LowTemperature;

        public float GetHumidity(GreenHouseSets set)
        {
            throw new NotImplementedException();
        }
    }
}
