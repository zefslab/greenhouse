﻿using System.Collections.Generic;

namespace GreenHouse.Api.Ventilation
{
    /// <summary>
    /// Система управления проветриваением
    /// </summary>
    interface IVentilationSystem
    {
        /// <summary>
        /// Узлы проветривания. Они могут быть разных типов и принцыпов действия.
        /// </summary>
        IList<IVentilationNode> VentilationNodes { get; set; }
    }
}
