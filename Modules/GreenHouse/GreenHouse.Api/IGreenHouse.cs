﻿using System;
using System.Collections.Generic;
using BoilerRoom.Api;
using PumpRoom.Api;
using ClimateControl.Api.Temperature;

namespace GreenHouse.Api
{

    public interface IGreenHouse : IHeatConsumer, IWaterConsumer
    {
        string Name { get; set; }

        IList<IGardenBed> GardenBeds{ get; set; }

        IClimateControl ClimateControl { get; set; }

        /// <summary>
        /// Температура в указанном месте теплицы
        /// </summary>
        /// <param name="set"></param>
        /// <returns></returns>
        float GetTemperature(GreenHouseSets set);
        
        /// <summary>
        /// Влажность в теплице
        /// </summary>
        /// <param name="set"></param>
        /// <returns></returns>
        float GetHumidity(GreenHouseSets set);

       
    }
}
