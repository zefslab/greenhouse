﻿using System.Collections.Generic;
using GreenHouse.Api;

namespace SmartHouse.Api
{
    public interface ISmartHouse
    {
        IList<IGreenHouse>  GreenHouses { get; set; }
    }
}
