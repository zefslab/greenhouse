﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyControl.Api
{
    /// <summary>
    /// Служит для контроля за величиной нагрузки в электроцепях и при ее привышении
    /// допустимых значений отключает некритичные потребители
    /// </summary>
    public interface IEnergyControl
    {
    }
}
