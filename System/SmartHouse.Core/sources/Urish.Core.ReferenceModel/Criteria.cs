﻿using System;

namespace Urish.Core.ReferenceModel
{
    /// <summary>
    /// Класс-фабрика критериев. Используется, как вспомогательный класс 
    /// при построении цепочки критериев для запроса
    /// </summary>
    public static class Criteria
    {
        /// <summary>
        /// Метод-фабрика, возвращает нулевой критерий, который сам по себе
        /// не имеет эффекта в запросе, но позволяет начинать с него цепочку
        /// критериев. Этот метод предназначен для удобства при описании цепочек критериев.
        /// </summary>
        /// <typeparam name="T">Тип запрашиваемого объекта</typeparam>
        /// <returns></returns>
        public static ICriteria<T> New<T>()
        {
            return new NullCriteria<T>();
        }
    }

    /// <summary>
    /// Пустой критерий, не накладывающий никакой специфики на выполнение запроса.
    /// Используется, как заглушка при в начале составления цепочки критериев.
    /// </summary>
    /// <typeparam name="T">Тип запрашиваемого объекта</typeparam>
    public class NullCriteria<T> : ICriteria<T>
    {
    }

    public static class CriteriaExtension
    {
        /// <summary>
        /// Объединяет два объекта критерия и возвращает новый критерий,
        /// содержащий эффект обоих критериев
        /// </summary>
        /// <typeparam name="T">Тип запрашиваемого объекта</typeparam>
        /// <param name="criteria">Левый объект критерия</param>
        /// <param name="other">Правый объект критерия</param>
        /// <returns>Новый объект критерия</returns>
        public static ICriteria<T> Merge<T>(this ICriteria<T> criteria, ICriteria<T> other)
        {
            if (criteria == null)
                throw new ArgumentNullException("criteria");
            if (other == null)
                throw new ArgumentNullException("other");

            // пустой критерий при слиянии заменяется на второй
            if (criteria is NullCriteria<T>)
            {
                return other;
            }

            // набор критериев при слиянии пополняется вторым критерием
            if (criteria is CriteriaSet<T>)
            {
                (criteria as CriteriaSet<T>).Add(other);
                return criteria;
            }

            // два обычных критерия при слиянии образуют набор критериев
            var criteriaSet = new CriteriaSet<T>();
            criteriaSet.Add(criteria);
            criteriaSet.Add(other);
            return criteriaSet;
        }
    }
}
