﻿using System;

namespace Urish.Core.ReferenceModel.Attribute
{
    /// <summary>
    /// Атрибут для отметки справочных свойств в dto модели.
    /// Он используется для поиска и обработки таких свойств в процессе
    /// импорта данных.
    /// Таким образом достигается автоматизированная обработка всех справочных свойств 
    /// и приведение кодов локальных и региональных справочников к кодам мастер-справочников
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ReferencePropertyAttribute : System.Attribute
    {

    }
}
