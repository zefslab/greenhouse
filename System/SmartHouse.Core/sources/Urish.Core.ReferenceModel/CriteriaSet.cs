﻿using System.Collections;
using System.Collections.Generic;

namespace Urish.Core.ReferenceModel
{
    /// <summary>
    /// Объект критерия, представляющий собой набор критериев, из которых
    /// он состоит. По другому это составной критерий и используется, чтобы
    /// сложить эффекты нескольких отдельных критериев и передать их все вместе
    /// в одном запросе.
    /// </summary>
    /// <typeparam name="T">Тип запрашиваемого объекта</typeparam>
    public class CriteriaSet<T> : ICriteria<T>, ICollection<ICriteria<T>>
    {
        public ICollection<ICriteria<T>> Criterias { get; private set; }

        public CriteriaSet()
        {
            Criterias = new List<ICriteria<T>>();
        }


        public IEnumerator<ICriteria<T>> GetEnumerator()
        {
            return Criterias.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Criterias.GetEnumerator();
        }

        public void Add(ICriteria<T> item)
        {
            Criterias.Add(item);
        }

        public void Clear()
        {
            Criterias.Clear();
        }

        public bool Contains(ICriteria<T> item)
        {
            return Criterias.Contains(item);
        }

        public void CopyTo(ICriteria<T>[] array, int arrayIndex)
        {
            Criterias.CopyTo(array, arrayIndex);
        }

        public bool Remove(ICriteria<T> item)
        {
            return Criterias.Remove(item);
        }

        public int Count { get { return Criterias.Count; } }
        public bool IsReadOnly { get { return Criterias.IsReadOnly; } }
    }
}
