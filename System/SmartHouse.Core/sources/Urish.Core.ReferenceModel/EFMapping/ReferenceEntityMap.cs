﻿using System.Data.Entity.ModelConfiguration;
using Urish.Core.Api.Entity;
using Urish.Core.ReferenceModel;

namespace Urish.Core.Api.Dao.Mapping
{
    public class ReferenceEntityMap<TReferenceEntity> : EntityTypeConfiguration<TReferenceEntity>
        where TReferenceEntity : ReferenceEntity
    {
        public ReferenceEntityMap()
        {
            HasKey(h => h.VersionId);

            Property(h => h.VersionId).HasColumnName("version_id").HasColumnOrder(1);
            Property(h => h.RecordId).HasColumnName("record_id").HasColumnOrder(2);
            Property(h => h.IsDeleted).HasColumnName("is_deleted").HasColumnOrder(3);
            Property(h => h.Code).HasColumnName("code").HasColumnOrder(4);
            Property(h => h.ReleaseDateBegin).HasColumnName("release_date_begin").HasColumnOrder(5);
            Property(h => h.ReleaseDateEnd).HasColumnName("release_date_end").HasColumnOrder(6);
            Ignore(h => h.IsActual);
        }
    }
}
