﻿using System.Data.Entity.ModelConfiguration;
using Urish.Core.Api.Entity;
using Urish.Core.ReferenceModel;

namespace Urish.Core.Api.Dao.Mapping
{
    public class ReferenceNamedEntityMap<TReferenceEntity> : ReferenceEntityMap<TReferenceEntity>
        where TReferenceEntity : ReferenceNamedEntity
    {
        public ReferenceNamedEntityMap()
        {
            Property(h => h.Name).HasColumnName("name");
        }
    }
}
