﻿using System;

namespace Urish.Core.ReferenceModel
{
    /// <summary>
    /// TODO прокомменитровать класс - обобщить назначение его методов
    ///  </summary>
    public static class BaseCriterias
    {
        #region  Набор универсальных критериев пригодных для большинства справочников
        /// <summary>
        /// TODO прокомментировать каждый критерий
        /// </summary>
        public class ByRecordIdCriteria : ICriteria<ReferenceEntity>
        {
            public ByRecordIdCriteria(Guid recordId)
            {
                RecordId = recordId;
            }

            public Guid RecordId { get; private set; }
        }

        public class ByRecordIdsCriteria : ICriteria<ReferenceEntity>
        {
            public Guid[] RecordIds { get; private set; }

            public ByRecordIdsCriteria(params Guid[] recordIds)
            {
                RecordIds = recordIds;
            }
        }

        public class ByVersionIdCriteria : ICriteria<ReferenceEntity>
        {
            public ByVersionIdCriteria(Guid versionId)
            {
                VersionId = versionId;
            }

            public Guid VersionId { get; private set; }
        }

        public class ByVersionIdsCriteria : ICriteria<ReferenceEntity>
        {
            public ByVersionIdsCriteria(params Guid[] versionIds)
            {
                VersionIds = versionIds;
            }

            public Guid[] VersionIds { get; private set; }
        }

        public class ByCodeCriteria : ICriteria<ReferenceEntity>
        {
            public ByCodeCriteria(string code)
            {
                Code = code;
            }

            public string Code { get; private set; }
        }

        public class ByCodesCriteria : ICriteria<ReferenceEntity>
        {
            public ByCodesCriteria(params string[] codes)
            {
                Codes = codes;
            }

            public string[] Codes { get; private set; }
        }

        public class InActualCriteria : ICriteria<ReferenceEntity>
        {
        }

        public class InReleaseCriteria : ICriteria<ReferenceEntity>
        {
            public InReleaseCriteria(DateTime releaseDate)
            {
                ReleaseDate = releaseDate.Date;
            }

            public DateTime ReleaseDate { get; private set; }
        }

        public class WithDeletedCriteria : ICriteria<ReferenceEntity>
        {
        }
        #endregion

        /// <summary>
        /// Выбрать по заданному идентификатору записи
        /// </summary>
        /// <exception cref="ArgumentNullException"><paramref name="criteria"/> is <see langword="null" />.</exception>
        public static ICriteria<T> ByRecordId<T>(this ICriteria<T> criteria, Guid recordId) 
            where T : ReferenceEntity
        {
            if (criteria == null) throw new ArgumentNullException("criteria");

            return criteria.Merge(new ByRecordIdCriteria(recordId));
        }

        /// <summary>
        /// Выбрать по заданному набору идентификаторов записей
        /// </summary>
        /// <exception cref="ArgumentNullException"><paramref name="criteria"/> is <see langword="null" />.</exception>
        public static ICriteria<T> ByRecordIds<T>(this ICriteria<T> criteria, params Guid[] recordIds)
            where T : ReferenceEntity
        {
            if (criteria == null) throw new ArgumentNullException("criteria");

            return criteria.Merge(new ByRecordIdsCriteria(recordIds));
        }

        /// <summary>
        /// Выбрать по заданному идентификатору версии записи
        /// </summary>
        /// <exception cref="ArgumentNullException"><paramref name="criteria"/> is <see langword="null" />.</exception>
        public static ICriteria<T> ByVersionId<T>(this ICriteria<T> criteria, Guid versionId) 
            where T : ReferenceEntity
        {
            if (criteria == null) throw new ArgumentNullException("criteria");

            return criteria.Merge(new ByVersionIdCriteria(versionId));
        }

        /// <summary>
        /// Выбрать по заданному набору идентификаторов версий записей
        /// </summary>
        /// <exception cref="ArgumentNullException"><paramref name="criteria"/> is <see langword="null" />.</exception>
        public static ICriteria<T> ByVersionIds<T>(this ICriteria<T> criteria, params Guid[] versionIds)
            where T : ReferenceEntity
        {
            if (criteria == null) throw new ArgumentNullException("criteria");

            return criteria.Merge(new ByVersionIdsCriteria(versionIds));
        }

        /// <summary>
        /// Выбрать по заданному коду записи
        /// </summary>
        /// <exception cref="ArgumentNullException"><paramref name="criteria"/> is <see langword="null" />.</exception>
        public static ICriteria<T> ByCode<T>(this ICriteria<T> criteria, string code) 
            where T : ReferenceEntity
        {
            if (criteria == null) throw new ArgumentNullException("criteria");

            return criteria.Merge(new ByCodeCriteria(code));
        }

        /// <summary>
        /// Выбрать по заданному составному коду записи
        /// </summary>
        /// <exception cref="ArgumentNullException"><paramref name="criteria"/> is <see langword="null" />.</exception>
        public static ICriteria<T> ByCode<T>(this ICriteria<T> criteria, params string[] codes) 
            where T:ReferenceEntity
        {
            if (criteria == null) throw new ArgumentNullException("criteria");

            return criteria.Merge(new ByCodeCriteria(ReferenceEntity.MergeCodes(codes)));
        }

        /// <summary>
        /// Выбрать по заданному набору кодов записей
        /// </summary>
        /// <exception cref="ArgumentNullException"><paramref name="criteria"/> is <see langword="null" />.</exception>
        public static ICriteria<T> ByCodes<T>(this ICriteria<T> criteria, params string[] codes)
            where T : ReferenceEntity
        {
            if (criteria == null) throw new ArgumentNullException("criteria");

            return criteria.Merge(new ByCodesCriteria(codes));
        }

        /// <summary>
        /// Выбрать только из актуального справочника (последней имеющейся версии)
        /// </summary>
        /// <exception cref="ArgumentNullException"><paramref name="criteria"/> is <see langword="null" />.</exception>
        public static ICriteria<T> InActual<T>(this ICriteria<T> criteria) where T : ReferenceEntity
        {
            if (criteria == null) throw new ArgumentNullException("criteria");

            return criteria.Merge(new InActualCriteria());
        }

        /// <summary>
        /// Выбрать только из справочника указанной даты релиза
        /// </summary>
        /// <exception cref="ArgumentNullException"><paramref name="criteria"/> is <see langword="null" />.</exception>
        public static ICriteria<T> InRelease<T>(this ICriteria<T> criteria, DateTime releaseDate) where T : ReferenceEntity
        {
            if (criteria == null) throw new ArgumentNullException("criteria");

            return criteria.Merge(new InReleaseCriteria(releaseDate));
        }

        /// <summary>
        /// Выбрать включая удалённые версии записей
        /// </summary>
        /// <exception cref="ArgumentNullException"><paramref name="criteria"/> is <see langword="null" />.</exception>
        public static ICriteria<T> WithDeleted<T>(this ICriteria<T> criteria) where T : ReferenceEntity
        {
            if (criteria == null) throw new ArgumentNullException("criteria");

            return criteria.Merge(new WithDeletedCriteria());
        }
    }
}