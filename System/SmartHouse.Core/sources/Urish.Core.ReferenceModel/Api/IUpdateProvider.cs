using System.Collections.Generic;

namespace Urish.Core.ReferenceModel
{
    /// <summary>
    /// ��������� ����������� ����������, � ������ ������� 
    /// ������ ������������� ��������� ������ ����������
    /// ��� ������������ � ����������� ����� �������
    /// ���������� ������������ ���������� ���������� ��� 
    /// ���������� �������� ���������� ������������
    /// </summary>
    /// <typeparam name="T">��� ������ �����������, ��� �������� ��������� ������������� ����������</typeparam>
    public interface IUpdateProvider<T> 
        where T: ReferenceEntity
    {
        /// <summary>
        /// ���������� ������ �������� ��������� ����������
        /// </summary>
        /// <returns>����� �������� ����������</returns>
        IEnumerable<UpdateInfo> GetAvailableUpdates();

        /// <summary>
        /// ���������� ����� ���������� �� ����������� �������� ����������
        /// </summary>
        /// <param name="updateInfo">������, ����������� ���������� ����������</param>
        /// <returns>����� � ������� ����������</returns>
        UpdatePackage<T> GetUpdate(UpdateInfo updateInfo);
    }
}