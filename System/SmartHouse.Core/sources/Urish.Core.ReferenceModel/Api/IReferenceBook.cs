﻿using System;
using System.Collections.Generic;
using Urish.Core.Api.Dto;

namespace Urish.Core.ReferenceModel
{
    /// <summary>
    /// Базовый нетипизированный интерфейс классов справочников.
    /// </summary>
    public interface IReferenceBook
    {
        /// <summary>
        /// Возвращает набор всех дат релизов текущего справочника
        /// </summary>
        /// <returns>Набор дат релизов</returns>
        IEnumerable<DateTime> GetReleases();

        /// <summary>
        /// Возвращает самую последнюю дату релиза, имеющуюся в справочнике
        /// </summary>
        /// <returns>Последняя дата релиза, либо null если справочник пуст</returns>
        DateTime? GetActualRelease();

        /// <summary>
        /// Метод-запрос единичной записи из текущего справочника. 
        /// </summary>
        /// <param name="criteria">Критерий выборки записей</param>
        /// <returns>Запись, удовлетворяющая критерию выборки, либо null</returns>
        ReferenceEntity QueryRecord(ICriteria criteria);

        /// <summary>
        /// Метод-запрос набора записей из текущего справочника
        /// </summary>
        /// <param name="criteria">Критерий выборки записей</param>
        /// <returns>Набор записей, удовлетворяющий критерию выборки, либо пустой набор</returns>
        IEnumerable<ReferenceEntity> QueryRecords(ICriteria criteria);
    }

    /// <summary>
    /// Базовый типизированный интерфейс классов справочников
    /// </summary>
    /// <typeparam name="TRefData">Тип объекта записи справочника</typeparam>
    public interface IReferenceBook<TRefData> : IReferenceBook
        where TRefData : ReferenceEntity
    {
        /// <summary>
        /// Метод-запрос единичной записи из текущего справочника. 
        /// </summary>
        /// <param name="criteria">Типизированный критерий выборки записей</param>
        /// <returns>Набор записей, удовлетворяющий критерию выборки, либо пустой набор</returns>
        TRefData QueryRecord(ICriteria<TRefData> criteria);

        /// <summary>
        /// Метод-запрос набора записей из текущего справочника
        /// </summary>
        /// <param name="criteria">Типизированный критерий выборки записей</param>
        /// <returns>Набор записей, удовлетворяющий критерию выборки, либо пустой набор</returns>
        IEnumerable<TRefData> QueryRecords(ICriteria<TRefData> criteria);

        /// <summary>
        /// Выполняет обновление записей справочника согласно содержимому пакета обновления
        /// Добавляет новые версии для записей, на которые влияет обновление
        /// Этот метод может выполнить обновление только с текущей актуальной версии справочника 
        /// на любую более новую.
        /// </summary>
        /// <param name="package">Пакет с данными обновления</param>
        void Update(UpdatePackage<TRefData> package);

        /// <summary>
        /// Перекодирует справочное свойство какого либо типа в запись мастер справочника
        /// </summary>
        TRefData Transcode(BaseReferenceDto dto);
    }
}