﻿namespace Urish.Core.ReferenceModel
{
    /// <summary>
    /// Базовый интерфейс для классов критериев, используемых в 
    /// сервисах при запросах данных
    /// </summary>
    public interface ICriteria
    {
    }

    /// <summary>
    /// Типизированный вариант класса критериев, используется для
    /// разграничения классов критериев по типам сущностей,
    /// для которых они предназначаются
    /// </summary>
    /// <typeparam name="T">Тип сущности</typeparam>
    public interface ICriteria<in T> : ICriteria
    {
    }
}
