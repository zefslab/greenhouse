﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Microsoft.Practices.Unity;
using Urish.Core.Api.Utilities.Unity.Attrributes;

namespace Urish.Core.ReferenceModel.Service.Bl
{
    /// <summary>
    /// Провайдер исполуемый для составления мапингов и поиска соответсвий.
    /// Используется для соотнесения справочных свойств в dto модели локальным справочникам.
    /// </summary>
    [Export]
    [Configuration]//Для обеспечения постоянного времени жизни на время жизни приложения
    public class ReferenceMapsProvider
    {
        private readonly IUnityContainer _container;

        public ReferenceMapsProvider(IUnityContainer container)
        {
            if (container == null) 
                throw new ArgumentNullException("Не разрешается зависимость container через конструктор класса");
            
            _container = container;
        }

        /// <summary>
        /// Маппинг типов dto справочных свойств на типы записей справочников
        /// </summary>
        private IDictionary<Type, Type> _referenceMap = new Dictionary<Type, Type>();

        /// <summary>
        /// Регистрируется соответствие типа справочного свойства dto модели справочнику
        /// </summary>
        public bool RegisterReverence(Type dtoPropertyType, Type referenceRecordType)
        {
            if (_referenceMap.ContainsKey(dtoPropertyType))
            {
                //пока не допускается регистрация более одного локального справочника для одного типа dto
                return false;
            }

            _referenceMap.Add(dtoPropertyType, referenceRecordType);
            return true;
        }

        /// <summary>
        /// Выбор соответствия типа справочного свойства в dto структуре локальному справочнику.
        /// В общем случае выбор локального справочника зависит и от типа МИС.
        /// Но пока данные принимаются только из систем КОМИАЦ, то указание на тип не используется.
        /// </summary>
        /// <param name="referencePropertyDtoType">тип справочного свойства в dto объекте</param>
        /// <param name="infomationSystemType">тип информационной системы, из котрой приходят данные кодов</param>
        /// <returns></returns>
        public IReferenceBook GetReference(Type referencePropertyDtoType, string infomationSystemType)
        {
            //Обращение к таблице соответствий типов справочных свойств и наименований
            //типов информационных систем для получения инстанции справочника
            if (_referenceMap.ContainsKey(referencePropertyDtoType))
            {
                
                var type = typeof (IReferenceBook<>).MakeGenericType(_referenceMap[referencePropertyDtoType]);
                //собственно инстанцирование справочника на основе полученного типа записей
                return (IReferenceBook) _container.Resolve(type);
            }
            return null;
        }

    }
}
