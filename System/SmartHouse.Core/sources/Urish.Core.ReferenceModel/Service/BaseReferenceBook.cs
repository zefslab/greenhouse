﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Xml.Linq;
using Microsoft.Practices.Unity;
using Urish.Core.Api.Dao;
using Urish.Core.Api.Dto;
using Urish.Core.Api.Utilities;
using Urish.Core.Api.Utilities.Extentions;
using Urish.Core.ReferenceModel.Service.Bl;
using Urish.Diagnostic.Services.API;

namespace Urish.Core.ReferenceModel
{
    /// <summary>
    /// Базовая реализация объекта справочника, реализующая основные 
    /// функции работы со справочниками, и предназначенная для наследования
    /// классами конкретных справочников со своей расширенной структурой записей.
    /// </summary>
    /// <typeparam name="TRefData">Тип записи справочника</typeparam>
    public abstract class BaseReferenceBook<TRefData> : IReferenceBook<TRefData>
        where TRefData : ReferenceEntity
    {
        // Кеш дат релизов справочника
        private static DateTime[] _releaseCache;
        // Объект-блокировка доступа к разделяемому кешу дат релизов справочника
        private static object _releaseCacheLock = new object();

        protected bool DisposeContextAfterQuery = true;

        /// <summary>
        /// Провайдер идентификаторов, для назначения их новым записям справочника
        /// </summary>
        [Import]
        [Dependency]
        public IIdentifierProvider<Guid> IdentifierProvider { get; set; }

        /// <summary>
        /// Провайдер для получения соответсвий типов dto свойств локальным справочникам
        /// и соответсвий локальных справочников мастерсправочникам
        /// </summary>
        [Import]
        [Dependency]
        public ReferenceMapsProvider ReferenceMapsProvider { get; set; }

        [Import]
        [Dependency]
        public ILoggingService logger { get; set; }

         [Import]
        [Dependency]
        public IUnityContainer container { get; set; } //TODO убрать после тестирования
        
       


        /// <summary>
        /// Метод, возвращающий объект контекста данных, который используется 
        /// при обращении к базе данных. При каждом обращении должен возвращаться
        /// новый объект контекста!
        /// </summary>
        protected abstract DbConnectionBase GetConnection();

        /// <summary>
        /// Флаг, указывающий, что при выполнении запроса в результат должны включаться
        /// записи, помеченные как удалённые. По умолчанию они не возвращаются.
        /// </summary>
        protected bool WithDeleted;

        /// <summary>
        /// Список объектов, описывающих связи между записями текущего справочника
        /// и записями других справочников. Используется в процессе обновления справочника
        /// для актуализации идентификаторов 
        /// </summary>
        private IList<RelationDefinitionInfo> _relationDefinitionInfos = new List<RelationDefinitionInfo>();


        /// <summary>
        /// Метод-запрос набора записей из текущего справочника
        /// </summary>
        /// <param name="criteria">Типизированный критерий выборки записей</param>
        /// <returns>Набор записей, удовлетворяющий критерию выборки, либо пустой набор</returns>
        /// <exception cref="ArgumentNullException">criteria is <see langword="null" />.</exception>
        /// <exception cref="NotSupportedCriteriaException">Критерий данного типа не поддерживается</exception>
        public ReferenceEntity QueryRecord(ICriteria criteria)
        {
            if (criteria == null) throw new ArgumentNullException("criteria");

            if (!(criteria is ICriteria<TRefData>))
                throw new NotSupportedCriteriaException(criteria);

            return QueryRecord((ICriteria<TRefData>)criteria);
        }

        /// <summary>
        /// Метод-запрос единичной записи из текущего справочника. 
        /// </summary>
        /// <param name="criteria">Типизированный критерий выборки записей</param>
        /// <returns>Набор записей, удовлетворяющий критерию выборки, либо пустой набор</returns>
        /// <exception cref="ArgumentNullException"><paramref name="criteria"/> is <see langword="null" />.</exception>
        public virtual TRefData QueryRecord(ICriteria<TRefData> criteria)
        {
            if (criteria == null) throw new ArgumentNullException("criteria");

            var connection = GetConnection();
            try
            {
                var query = connection.GetSet<TRefData>().AsQueryable();
                query = (DisposeContextAfterQuery ? query.AsNoTracking() : query);
                query = _ApplyCriteria(query, criteria);
                return ExcludeDeleted(query).FirstOrDefault();
            }
            finally
            {
                if (DisposeContextAfterQuery) connection.Dispose();
            }
        }

        /// <summary>
        /// Метод-запрос набора записей из текущего справочника
        /// </summary>
        /// <param name="criteria">Критерий выборки записей</param>
        /// <exception cref="NotSupportedCriteriaException">Критерий данного типа не поддерживается</exception>
        /// <exception cref="ArgumentNullException"><paramref name="criteria"/> is <see langword="null" />.</exception>
        public IEnumerable<ReferenceEntity> QueryRecords(ICriteria criteria)
        {
            if (criteria == null) throw new ArgumentNullException("criteria");

            if (!(criteria is ICriteria<TRefData>))
                throw new NotSupportedCriteriaException(criteria);

            return QueryRecords((ICriteria<TRefData>)criteria);
        }

        /// <summary>
        /// Метод-запрос набора записей из текущего справочника
        /// </summary>
        /// <param name="criteria">Критерий выборки записей</param>
        /// <returns>Набор записей, удовлетворяющий критерию выборки, либо пустой набор</returns>
        /// <exception cref="ArgumentNullException"><paramref name="criteria"/> is <see langword="null" />.</exception>
        public virtual IEnumerable<TRefData> QueryRecords(ICriteria<TRefData> criteria)
        {
            if (criteria == null) throw new ArgumentNullException("criteria");

            var connection = GetConnection();
            try
            {
                var query = connection.GetSet<TRefData>().AsQueryable();
                query = (DisposeContextAfterQuery ? query.AsNoTracking() : query);

                query = _ApplyCriteria(query, criteria);
                return ExcludeDeleted(query).ToArray();
            }
            finally
            {
                if (DisposeContextAfterQuery) connection.Dispose();
            }
        }

        /// <summary>
        /// Возвращает набор всех дат релизов текущего справочника
        /// </summary>
        /// <returns>Набор дат релизов</returns>
        public IEnumerable<DateTime> GetReleases()
        {
            lock (_releaseCacheLock)
            {
                if (_releaseCache != null) return _releaseCache;


                var connection = GetConnection();
                try
                {
                    _releaseCache =
                        connection.GetSet<TRefData>().Select(o => o.ReleaseDateBegin)
                            .Distinct().OrderByDescending(o => o).ToArray();
                }
                finally
                {
                    if(DisposeContextAfterQuery) connection.Dispose();
                }
                return _releaseCache;
            }
        }

        /// <summary>
        /// Возвращает самую последнюю дату релиза, имеющуюся в справочнике
        /// </summary>
        /// <returns>Последняя дата релиза, либо null если справочник пуст</returns>
        public DateTime? GetActualRelease()
        {
            var releases = GetReleases() as DateTime[];
            // ReSharper disable once PossibleNullReferenceException
            return releases.Length > 0 ? releases[0] : (DateTime?)null;
        }

        /// <summary>
        /// Перекодирует справочное свойство какого либо типа в запись мастер справочника
        /// </summary>
        /// <param name="dto"></param>
        public TRefData Transcode(BaseReferenceDto dto) 
        {
            if (dto == null)
            {
                logger.Debug<BaseReferenceDto>(String.Format("Нет данных для перекодировки справочника {0}",
                                                             typeof(TRefData).Name));

#if DEBUG
                Mconsole.WriteLine(new Mstring(String.Format("Нет данных для перекодировки справочника {0}",
                    typeof(TRefData).Name), ConsoleColor.DarkRed));
#endif
                return null;
            }

            //Запрос к провайдеру на получение ссылки на справочник связанный по типу dto записи
            var reference = ReferenceMapsProvider.GetReference(dto.GetType(), "Указать тип информационной системы");

            if (reference == null)
            {
                logger.Error<BaseReferenceDto>(String.Format("В таблице мапингов не найден  справочник, " +
                                                             "соответвующий типу dto свойства {0}",
                                                              dto.GetType().Name));

#if DEBUG
               
                Mconsole.WriteLine(new Mstring(
                    String.Format("В таблице мапингов не найден  справочник, соответвующий типу dto свойства {0}",
                    dto.GetType().Name), ConsoleColor.DarkRed));
#endif

                return null;
            }

            //получение записи из связанного справочника по коду или составному коду в dto свойстве

            var codes = new [] {dto.Code}.Concat(dto.ExtraCodes.EmptyIfNull());
            var referenceRecord = reference.QueryRecord(_searchByCode(codes));

            if (referenceRecord == null)
            {
                var codesString = String.Join(",", codes);
                
                if (codesString != "")
                {
                    logger.Error<BaseReferenceDto>(String.Format(
                        "Не найдена запись в справочнике: {0} по коду: {1}", typeof (TRefData), codesString));

#if DEBUG
                    if (!String.IsNullOrEmpty(dto.Code))
                    {
                        //Логирование отсутствующих кодов справочников в отдельный файл для отдела Глуховой
                        var myOperationLog = container.Resolve<XElement>("myOperationLog");
                        //Добавляем новый код только если его не существует в списке залогированных
                        if (myOperationLog.Elements()
                            .FirstOrDefault(x => x.Attribute("Reference").Value == typeof (TRefData).Name
                                                 && x.Value == codesString) == null)
                        {
                            myOperationLog.Add(
                                new XElement("ReferenceCodeNotFound", String.Join(",", codes),
                                    new XAttribute("DateTime", DateTime.Now),
                                    new XAttribute("Reference", typeof (TRefData).Name)));
                        }
                    }

                    Mconsole.WriteLine(new Mstring(String.Format(
                        "Не найдена запись в справочнике: {0} по коду: {1}", typeof (TRefData), dto.Code),
                        ConsoleColor.DarkRed));
                }
#endif
                return null;
            }
            
            //Нужно понять, связанный справочник является мастер справочником или одним из справочников
            //котрые имеют коды перекодировки на мастер справочник
            bool isMasterReference = !(referenceRecord is ITransCode);

            if (isMasterReference)
            {

                return (TRefData)referenceRecord;
            }

            // Так как справочник из мапинга оказался не мастер справочник, то по коду перекодировки из текущего 
            // (мастер) справочника находится запись
            var resultRecord =
                this.QueryRecord(Criteria.New<ReferenceEntity>().ByCode(((ITransCode) referenceRecord).TransCode));
#if DEBUG
            if (((ITransCode) referenceRecord).TransCode == String.Empty)
            {
                Mconsole.WriteLine(new Mstring(String.Format("Нет кода перекодировки в справочнике {0} для справочника {1} ",
                                                reference.GetType().Name,
                                                this.GetType().Name),
                                ConsoleColor.DarkRed));

            }
            else if (resultRecord == null)
            {
                Mconsole.WriteLine(
                    new Mstring(String.Format("В связанном справочнике {0} не найдена запись с кодом {1} ",
                        this.GetType().Name,
                        ((ITransCode) referenceRecord).TransCode),
                        ConsoleColor.DarkRed));
            }
#endif
            if (((ITransCode)referenceRecord).TransCode != String.Empty && resultRecord == null)
            {
                logger.Warn<BaseReferenceDto>(String.Format(
                    "В связанном справочнике {0} не найдена запись с кодом {1} ",
                    this.GetType().Name,
                    ((ITransCode) referenceRecord).TransCode));
            }

            return resultRecord;
        }

        protected virtual ICriteria _searchByCode(IEnumerable<string> codes)
        {
            return Criteria.New<ReferenceEntity>().ByCode(codes.ToArray());
        }

        /// <summary>
        /// Делает кеш релизов справочника невалидным, чтобы он 
        /// обновился при следующем обновлении.
        /// </summary>
        protected void UnvalidateReleaseCache()
        {
            lock (_releaseCacheLock)
            {
                _releaseCache = null;
            }
        }

        // Исключает из запроса все удалённые записи
        private IQueryable<TRefData> ExcludeDeleted(IQueryable<TRefData> query)
        {
            return !WithDeleted ? query.Where(o => !o.IsDeleted) : query;
        }



        // Вызывается при выполнении запроса для обработки объекта критерия и применения его 
        // к запросу данных. Он также разворачивает объект набора критериев, для 
        // обработки его содержимого по отдельности.
        private IQueryable<TRefData> _ApplyCriteria(IQueryable<TRefData> query, ICriteria<TRefData> criteria)
        {
            if (criteria == null || criteria is NullCriteria<TRefData>) return query;

            var criteriaSet = criteria as CriteriaSet<TRefData>;

            query = criteriaSet != null
                ? criteriaSet.Aggregate(query, _ApplyCriteria)
                : ApplyCriteria(query, criteria);

            return query;
        }

        /// <summary>
        /// Метод, выполняющий обработку одного критерия и модифицирующий результирующий запрос.
        /// Он выполняется в QueryRecords или QueryRecord перед тем, как запрос будет передан в базу данных.
        /// Вызывается по одному разу для каждого объекта критерия, содержащегося в составном критерии.
        /// Базовая реализация определяет обработку основных критериев.
        /// Может быть переопределён в наследниках, для обеспечения поддержки 
        /// дополнительных критериев выборки, специфичных для конкретных справочников.
        /// </summary>
        /// <param name="query">Объект запроса, на который влияет критерий</param>
        /// <param name="criteria">Объект критерия запроса. Сюда не попадает объект составного критерия, а только составляющие его подкритерии</param>
        /// <returns>Объект запроса, получившийся в результате применения критерия к исходному запросу.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="criteria"/> is <see langword="null" />.</exception>
        /// <exception cref="NotSupportedCriteriaException">Критерий данного типа не поддерживается</exception>
        /// <exception cref="CriteriaException">Ошибка в применении критерия запроса, проблемный критерий указан в свойстве <see cref="CriteriaException.Criteria"/></exception>
        /// <remarks>При переопределении в наследнике следует вызывать базовую реализацию метода, если вы хотите обеспечить
        /// поддержки базовых критериев. Либо не вызывать, если вы хотите переопределить поведение базовых критериев. Также следует бросать исключение 
        /// <see cref="NotSupportedCriteriaException"/>, либо вызывать базовый метод, если был передан критерий, не предусмотренный обработкой вашего переопределения.</remarks>
        protected virtual IQueryable<TRefData> ApplyCriteria(IQueryable<TRefData> query, ICriteria<TRefData> criteria)
        {
            if (criteria == null) throw new ArgumentNullException("criteria");

            var byVersionId = criteria as BaseCriterias.ByVersionIdCriteria;
            if (byVersionId != null)
                return query.Where(o => o.VersionId == byVersionId.VersionId);

            var byRecordId = criteria as BaseCriterias.ByRecordIdCriteria;
            if (byRecordId != null)
                return query.Where(o => o.RecordId == byRecordId.RecordId);

            var byCode = criteria as BaseCriterias.ByCodeCriteria;
            if (byCode != null)
                return query.Where(o => o.Code == byCode.Code);

            if (criteria is BaseCriterias.InActualCriteria)
                return query.Where(o => o.ReleaseDateEnd == ReferenceBookConstants.ActualReleaseEndDate);

            var inRelease = criteria as BaseCriterias.InReleaseCriteria;
            if (inRelease != null)
            {
                if (!GetReleases().Contains(inRelease.ReleaseDate))
                    throw new CriteriaException("Specified release date not found in reference book", inRelease);

                return query.Where(o => o.ReleaseDateBegin <= inRelease.ReleaseDate &&
                                        o.ReleaseDateEnd >= inRelease.ReleaseDate);
            }

            var codesCriteria = criteria as BaseCriterias.ByCodesCriteria;
            if (codesCriteria != null)
            {
                return query.Where(o => codesCriteria.Codes.Contains(o.Code));
            }

            var idsCriteria = criteria as BaseCriterias.ByRecordIdsCriteria;
            if (idsCriteria != null)
            {
                return query.Where(o => idsCriteria.RecordIds.Contains(o.RecordId));
            }

            var versionIdsCriteria = criteria as BaseCriterias.ByVersionIdsCriteria;
            if (versionIdsCriteria != null)
            {
                return query.Where(o => versionIdsCriteria.VersionIds.Contains(o.VersionId));
            }

            if (criteria is BaseCriterias.WithDeletedCriteria)
            {
                WithDeleted = true;
                return query;
            }

            throw new NotSupportedCriteriaException(criteria);
        }

        // todo добавить в класс два виртуальных метода, для предобработки запроса и для постобработки,
        // чтобы можно было вносить изменения в запрос перед его выполнением

        /// <summary>
        /// Выполняет обновление записей справочника согласно содержимому пакета обновления
        /// Добавляет новые версии для записей, на которые влияет обновление
        /// Этот метод может выполнить обновление только с текущей актуальной версии справочника 
        /// на любую более новую.
        /// </summary>
        /// <param name="package">Пакет обновления, который требуется применить к справочнику</param>
        /// <exception cref="ReferenceException">Проблема при выполнении обновления справочника. Подробности в
        /// описании и внутреннем исключении</exception>
        /// <exception cref="ArgumentNullException"><paramref name="package"/> is <see langword="null" />.</exception>
        public void Update(UpdatePackage<TRefData> package)
        {
            if (package == null) throw new ArgumentNullException("package");

            // проверить, что версия справочника обновляется с текущей актуальной
            var actualRelease = GetActualRelease();
            if (actualRelease != package.ReleaseDateFrom)
                throw new ReferenceException(
                    string.Format(
                        "Update package is not compatible with current reference book, because it can be " +
                        "update only from current actual version \"{0}\" to another new",
                        actualRelease.HasValue ? actualRelease.Value.ToString("yyyy.MM.dd") : "null"));

            // проверить, что версия, на которую обновляемся новее текущей
            if (package.ReleaseDateTo <= (package.ReleaseDateFrom ?? DateTime.MinValue))
                throw new ReferenceException("Update package ToReleaseDate can't be less or equals to FromReleaseDate");

            // начать транзакцию в хранилище справочников
            var db = GetConnection();
            try
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    // разбить все записи обновления на части
                    foreach (var packageChunk in package.Records.ToChunks(100))
                    {
                        var dbSet = db.GetSet<TRefData>();

                        // получить из актуального справочника все записи с теми же кодами, что и у данных обновления, сохранить в кеш
                        var updatedCodes = packageChunk.Select(o => o.Record.Code).ToArray();
                        var existedRecords =
                            dbSet.Where(o => o.ReleaseDateEnd == ReferenceBookConstants.ActualReleaseEndDate)
                                .Where(o => updatedCodes.Contains(o.Code)).ToArray();

                        // взять одну запись обновления
                        foreach (var packageItem in packageChunk)
                        {
                            // найти в кеше запись с тем же кодом
                            var previousVersion =
                                existedRecords.FirstOrDefault(o => o.Code == packageItem.Record.Code);

                            // применить к записи справочника операцию, указанную в обновлении
                            ApplyOperationToRecord(packageItem, package, previousVersion, dbSet, db);
                        }
                        // сохранить изменения в контексте
                        try
                        {
                            db.SaveChanges();
                        }
                        catch (DbUpdateException exception)
                        {
                            throw new ReferenceException("Update reference book failed", exception);
                        }
                    }
                    transaction.Commit();
                }
            }
            finally
            {
                if (DisposeContextAfterQuery) db.Dispose();
            }
            // набор релизов справочника изменился, сбросить кеш релизов
            UnvalidateReleaseCache();
        }

        // Применяет элемент обновления записи к записи согласно указанной в нём операции обновления
        private void ApplyOperationToRecord(UpdatePackageItem<TRefData> packageItem,
            UpdatePackage<TRefData> package, TRefData previousVersion,
            DbSet<TRefData> dbSet, DbConnectionBase db)
        {
            switch (packageItem.Operation)
            {
                // если операция обновления записи == "добавлена", то
                case UpdateOperation.Addition:
                    // если в кеше есть запись с тем же кодом
                    if (previousVersion != null)
                        throw new ReferenceException(string.Format(
                            "Record with code \"{0}\" already exist and can't be added twice",
                            packageItem.Record.Code));

                    // добавить версию новой записи в справочник
                    AddNewVersion(packageItem, package, null, dbSet, db);
                    break;

                // если операция обновления записи == "изменена" или "удалена", то
                case UpdateOperation.Modification:
                case UpdateOperation.Removal:
                    // если она не существует или флаг "удалена" установлен
                    if (previousVersion == null)
                        throw new ReferenceException(
                            string.Format("Record with code \"{0}\" not exist and can't be modified",
                                packageItem.Record.Code));
                    if (previousVersion.IsDeleted)
                        throw new ReferenceException(
                            string.Format(
                                "Record with code \"{0}\" is marked as deleted and can't be modified",
                                packageItem.Record.Code));

                    // снять актуальность с предыдущей версии записи из кеша
                    previousVersion.ReleaseDateEnd = package.ReleaseDateFrom ?? DateTime.MinValue;
                    db.AttachAs(previousVersion, EntityState.Modified);

                    // добавить новую версию существующей записи
                    AddNewVersion(packageItem, package, previousVersion.RecordId, dbSet, db);
                    break;

                case UpdateOperation.Auto:
                    // автоматически определить операцию на основе текущего состояния записи
                    if (previousVersion == null)
                    {
                        // предыдущей версии записи нет, по этому нужно её добавить
                        ApplyOperationToRecord(new UpdatePackageItem<TRefData>(packageItem.Record, UpdateOperation.Addition),
                            package, null, dbSet, db);
                    }
                    else
                    {
                        // предыдущая версия запись есть, нужно добавить новую версию
                        ApplyOperationToRecord(new UpdatePackageItem<TRefData>(packageItem.Record,
                            packageItem.Record.IsDeleted ? UpdateOperation.Removal : UpdateOperation.Modification),
                            package, previousVersion, dbSet, db);
                    }
                    break;
            }
        }

        // Добавляет новую версию записи, отражающую в ней соответственную операцию обновления
        private void AddNewVersion(UpdatePackageItem<TRefData> packageItem,
            UpdatePackage<TRefData> package, Guid? previousRecordId,
            DbSet<TRefData> dbSet, DbConnectionBase db)
        {
            var record = packageItem.Record;
            record.VersionId = IdentifierProvider.NewIdentifier();
            record.RecordId = previousRecordId ?? IdentifierProvider.NewIdentifier();
            record.ReleaseDateBegin = package.ReleaseDateTo;
            record.ReleaseDateEnd = ReferenceBookConstants.ActualReleaseEndDate;
            record.IsDeleted = packageItem.Operation == UpdateOperation.Removal;

            // обработать связанные записи других справочников
            AssignRelatedReferenceRecords(record, db);
            dbSet.Add(record);
        }

        // Обрабатывает связи текущей записи с записями других справочников, согласно данным в
        // описаниях связей, объявленных в справочнике.
        private void AssignRelatedReferenceRecords(TRefData record, DbConnectionBase db)
        {
            // для каждого поля кода из связаного справочника и соответственного ему поля со связанной сущностью
            foreach (var definitionInfo in _relationDefinitionInfos)
            {
                // получить значение кода
                var code = definitionInfo.CodeSelector(record);

                // получить объект связанного справочника
                var referenceBook = definitionInfo.RelatedReferenceBookFactory();
                if (referenceBook == null)
                    throw new ReferenceException("Related reference book factory return null object");

                // получить из связанного справочника запись с кодом той версии, которая была наиболее актуальной
                //  на момент релиза текущей записи
                var relatedRecords = referenceBook.QueryRecords(Criteria.New<ReferenceEntity>().ByCode(code));
                var relatedRecord = relatedRecords.Where(o => o.ReleaseDateBegin <= record.ReleaseDateBegin)
                    .OrderByDescending(o => o.ReleaseDateBegin)
                    .FirstOrDefault();
                // если такой записи нет, вернуть исключение и прервать работу
                if (relatedRecord == null)
                    throw new ReferenceException("Can not find related reference record for code " + code + " and release " +
                                                 record.ReleaseDateBegin.ToString("yyyy.MM.dd") + " in " +
                                                 referenceBook.GetType().Name);
                try
                {
                    var ownedRelatedRecord = db.Set(definitionInfo.Property.PropertyType).Find(relatedRecord.VersionId);
                    // присвоить связанную запись в поле связи текущей записи
                    definitionInfo.Property.SetValue(record, ownedRelatedRecord);
                }
                catch (Exception exception)
                {
                    throw new ReferenceException(
                        "Can not assign related property " + definitionInfo.Property.Name + " of record", exception);
                }
            }
        }

        /// <summary>
        /// Объявляет связь между записями текущего справочника и записями других связанных
        /// записей. Используется при актуализации ссылок при обновлении записей справочника
        /// </summary>
        /// <typeparam name="TRelatedRefData">Тип связанной записи справочника</typeparam>
        /// <param name="relationExpression">Лямбда, указывающая свойство-связь с объектом записи другого справочника</param>
        /// <param name="codeSelector">Делегат, возвращающий значение поля кода связанной записи</param>
        /// <param name="relatedReferenceBookFactory">Делегат, возвращающий объект связанного справочника</param>
        /// <exception cref="ArgumentNullException"><paramref name="relationExpression"/> or 
        /// <paramref name="codeSelector"/> or <paramref name="relatedReferenceBookFactory"/> is <see langword="null" />.</exception>
        protected void DefineRelation<TRelatedRefData>(Expression<Func<TRefData, TRelatedRefData>> relationExpression,
            Func<TRefData, string> codeSelector, Func<IReferenceBook<TRelatedRefData>> relatedReferenceBookFactory)
            where TRelatedRefData : ReferenceEntity
        {
            if (relationExpression == null)
                throw new ArgumentNullException("relationExpression");
            if (codeSelector == null)
                throw new ArgumentNullException("codeSelector");
            if (relatedReferenceBookFactory == null)
                throw new ArgumentNullException("relatedReferenceBookFactory");

            var propertyInfo = ExpressionExtensions.GetPropertyFromExpression(relationExpression);

            _relationDefinitionInfos.Add(new RelationDefinitionInfo
            {
                Property = propertyInfo,
                CodeSelector = codeSelector,
                RelatedReferenceBookFactory = relatedReferenceBookFactory
            });
        }

        // Хранит данные описания связанной записи
        private struct RelationDefinitionInfo
        {
            public PropertyInfo Property;
            public Func<TRefData, string> CodeSelector;
            public Func<IReferenceBook> RelatedReferenceBookFactory;
        }
    }
}
