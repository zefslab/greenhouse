﻿using System;
using System.Collections.Generic;
using System.Linq;
using Urish.Core.ReferenceModel.Api;

namespace Urish.Core.ReferenceModel
{
    /// <summary>
    /// Класс менеджера обновлений справочников. Отвечает за 
    /// весь цикл обновления, выполняет обновление каждого справочника и
    /// обслуживает обновления, полученные из провайдеров обновлений.
    /// </summary>
    public class UpdateManager : IUpdateManager
    {
        private readonly Dictionary<Type ,object> _updateProviders = 
            new Dictionary<Type, object>();

        /// <summary>
        /// Регистрирует переданный провайдер обновлений в менеджере. На каждый тип
        /// справочника может быть зарегистрирован только один провайдер.
        /// Регистрация второго с тем же типом заменяет регистрацию первого.
        /// </summary>
        /// <typeparam name="T">Тип записи справочника, связанный с провайдером</typeparam>
        /// <param name="provider">Объект регистрируемого провайдера</param>
        /// <exception cref="ArgumentNullException"><paramref name="provider"/> is <see langword="null" />.</exception>
        public void AddUpdateProvider<T>(IUpdateProvider<T> provider)
            where T : ReferenceEntity
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            _updateProviders[typeof(T)] = provider;
        }

        /// <summary>
        /// Отменяет регистрацию переданного провайдера в менеджере.
        /// </summary>
        /// <typeparam name="T">Тип записи справочника, связанный с провайдером</typeparam>
        /// <param name="provider">Объект отменяемого провайдера</param>
        public void RemoveUpdateProvider<T>(IUpdateProvider<T> provider)
            where T : ReferenceEntity
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            _updateProviders.Remove(typeof(T));
        }

        /// <summary>
        /// Возвращает провайдер обновлений, связанный с типом
        /// записи справочника
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected IUpdateProvider<T> GetProvider<T>()
            where T : ReferenceEntity
        {
            object provider;
            _updateProviders.TryGetValue(typeof (T), out provider);
            return provider as IUpdateProvider<T>;
        }

        /// <summary>
        /// Возвращает список всех провайдеров, зарегистрированных в менеджере
        /// </summary>
        /// <typeparam name="T">Тип записи справочника, связанный с провайдером</typeparam>
        /// <returns>Список объектов провайдеров, или пустой список</returns>
        public IReadOnlyCollection<object> GetUpdateProviders()
        {
            return _updateProviders.Values.ToList().AsReadOnly();
        }

        /// <summary>
        /// Выполняет обновление указанного справочника на основе данных доступных 
        /// обновлений в соответственном провайдере. Справочник обновляется по 
        /// наиболее актуальной из доступных релизов в порядке возростания даты релиза.
        /// Релизы, которые уже имеются в справочнике, повторно не применяются, также как
        /// и пропущенные релизы.
        /// </summary>
        /// <typeparam name="T">Тип записи справочника, связанный с провайдером</typeparam>
        /// <param name="referenceBook">Объект обновляемого справочника</param>
        /// <exception cref="ReferenceException">Ошибка при обновлении справочника</exception>
        /// <exception cref="ArgumentNullException"><paramref name="referenceBook"/> is <see langword="null" />.</exception>
        public void UpdateReferenceBook<T>(IReferenceBook<T> referenceBook)
            where T : ReferenceEntity
        {
            if (referenceBook == null)
                throw new ArgumentNullException("referenceBook");

            // найти провайдер обновлений, соответствующий данному справочнику
            var updateProvider = GetProvider<T>();
            if(updateProvider == null)
                throw new ReferenceException("Updating of current reference book is not supported");
            // запросить у провайдеров обновлений данные обновлений для этого справочника
            var availableUpdates = updateProvider.GetAvailableUpdates();

            // определить, какие из обновлений можно применить к справочнику
            // определить текущую актуальную версию справочника
            var actualRelease = referenceBook.GetActualRelease();
            // определить релизы обновлений, которые новее текущего релиза справочника
            var updatesToApply = availableUpdates.Where(
                o => o.ReleaseDate > (actualRelease ?? DateTime.MinValue) && o.ReferenceDataType == typeof (T))
                .ToArray();

            // для каждого обновления
            foreach (var updateInfo in updatesToApply.OrderBy(o => o.ReleaseDate))
            {
                // получить у провайдеров пакеты обновлений для выбранных к применению обновлений
                var updatePackage = updateProvider.GetUpdate(updateInfo);
                // передать справочнику пакеты полученных от провайдеров обновлений
                referenceBook.Update(updatePackage);
            }
        }
    }
}
