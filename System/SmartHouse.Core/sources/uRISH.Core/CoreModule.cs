﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Reflection;
using SmartHouse.Core.Api;
using SmartHouse.Core.Api.Dao;
using SmartHouse.Core.Api.Entity;
using SmartHouse.Core.Dao;
using SmartHouse.Core.Dao.Migrations;

namespace SmartHouse.Core.Data.Entity
{
    /// <summary>
    /// Модуль ядра
    /// </summary>
    public class CoreModule : BaseModule, IBaseModule
    {
        private Configuration _configuration;

        DbMigrationsConfiguration IBaseModule.MigrationConfiguration
        {
            get { return _configuration ?? (_configuration = new Configuration()); }
        }

        public DbContext DbConnection
        {
            get { return new DbConnection(); }
        }

        /// <summary>
        /// Предоставляет сборки в которых есть классы реализующие зависимости
        /// </summary>
        /// <returns></returns>
        public IList<Assembly> GetAssembliesForUnityContainer()
        {
            return new List<Assembly>
            {
                typeof (BaseDao<,,>).Assembly,
                typeof (ModuleDao).Assembly,
            };
        }

    }
}
