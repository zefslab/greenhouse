﻿using System;
using System.ComponentModel.Composition;
using System.Data.Entity.Migrations;

namespace SmartHouse.Core.Dao.Migrations
{
    public class EFMigrations : IModuleEventHandler
    {
        public void Enabled(ModuleEntry entry)
        {
        }

        public void Enabling(ModuleEntry entry)
        {
            if(entry.Info.Id != new Guid("D79E637B-B396-4975-BA63-44878CA13363"))
                return;
            var configuration = new Configuration();
            var migrator = new DbMigrator(configuration);
            migrator.Update();
        }

        public void Disabled(ModuleEntry entry)
        {
        }

        public void Disabling(ModuleEntry entry)
        {
        }
    }
}
