using System.Data.Entity.Migrations;
using SmartHouse.Core.Api.Dao;

namespace SmartHouse.Core.Dao.Migrations
{
    /// <summary>
    /// ������������ �������� Code-First
    /// 
    /// ���������� ���� ������ ��������� ����������.
    /// ������� �� ���� ����������� �������� ������ �� ������ ��������, ��������� �� � ������ #region
    /// ��� ��������������  � ����������� ������ context.SaveChanges(); ����� ���������� ������, ����� �����
    /// ���� ����� ����������� ������ ����������.
    /// </summary>
    public sealed class Configuration : DbMigrationsConfiguration<DbConnection>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;

            
        }

        protected override void Seed(DbConnection context)
        {

        }
    }

   
}

