﻿using System;
using System.ComponentModel.Composition;
using SmartHouse.Core.Api.Dao;
using SmartHouse.Core.Api.Entity;
using SmartHouse.Core.Api.Utilities.Unity.Attrributes;

namespace SmartHouse.Core.Dao
{
    [Export(typeof(IModuleDao))]
    [Repository(forInterface: typeof(IModuleDao))]
    public class ModuleDao : BaseDao<BaseModule, DbConnection, Guid>, IModuleDao
    {
    }
}
