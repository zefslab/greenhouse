﻿using System;
using System.Data.Entity;

namespace SmartHouse.Core.Api.Dao
{
    public static class DbContextExtensions
    {
        /// <summary>
        /// Присоединяет сущность к контексту данных в состоянии "изменена"
        /// TODO Не вредно указывать не только что делает метод, но еще и для чего он это делает, п
        /// отому как то, что делает метод можно прочитать из него самого, если он грамотно 
        /// и самодокументированного написан, а вот для чего это нужно, понять никак не возможно.
        /// Это самое ценное в комментарии.
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="context">Контекст данных</param>
        /// <param name="entity">Объект присоединяемой сущности</param>
        /// <returns></returns>
        public static T AttachAs<T>(this DbContext context, T entity, EntityState state) 
            where T : class
        {
            if (context == null) throw new ArgumentNullException("context");
            if (entity == null) throw new ArgumentNullException("entity");

            context.Entry(entity).State = state;
            return entity;
        }
    }
}
