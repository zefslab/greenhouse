﻿using System.Data.Entity;

namespace SmartHouse.Core.Api.Dao
{
    /// <summary>
    /// Базовый класс выделен для того, чтобы типизированный класс BaseDao мог использовать метод DbSet
    /// При переходе к использованию интерфейса IDbConnection можно будет выпилить этот базовый класс
    /// </summary>

    public class DbConnectionBase : DbContext
    {
        public virtual DbSet<TEntity> GetSet<TEntity>() where TEntity : class
        {
            return Set<TEntity>();
        }
     
    }
}
