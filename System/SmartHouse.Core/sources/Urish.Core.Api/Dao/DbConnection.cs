﻿using System.ComponentModel.Composition;
using System.Data.Entity;
using SmartHouse.Core.Api.Dao.Mapping;

namespace SmartHouse.Core.Api.Dao
{
    /// <summary>
    /// Класс взаимодействия служб Entity Framework с кодом описывающим модель бизнес данных посредством DbContext.
    /// Этот класс вынесен на уровень DAO с целью сделать независимым доменную модель от способа взаимодействия с БД
    /// На уровне модуля этот класс расширяется DbSet сущностями своей бизнес модели
    /// </summary>
    [Export]
    public class DbConnection : DbConnectionBase
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ModuleMap());
        }

        public new virtual DbSet<TEntity> GetSet<TEntity>() where TEntity : class
        {
            return Set<TEntity>();
        }

        //Следующие свойства используются главным образом в методе Seed и возможно миграциях для создания информационного наполнения
        //public DbSet<Module> Modules { get; set; }
    }
}
