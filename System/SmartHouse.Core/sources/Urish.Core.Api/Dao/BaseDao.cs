﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Microsoft.Practices.Unity;
using SmartHouse.Core.Api.Entity;
using SmartHouse.Core.Api.Utilities.Extensions;
using Urish.Diagnostic.Services.API;

namespace SmartHouse.Core.Api.Dao
{
    /// <summary>
    /// Базовый класс для служб доступа к данным (репозиториям), обеспечивающий абстракцию
    /// всех основных операций над данными: создание, удаление, изменение, чтение.
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности модели данных</typeparam>
    /// <typeparam name="TConnection">Тип класса контекста доступа к данным</typeparam>
    /// <typeparam name="TId">Тип идентификатора сущности</typeparam>
    public abstract class BaseDao<TEntity, TConnection, TId> : IDao<TEntity, TId>
        where TEntity : BaseEntity<TId>, new()
        where TConnection : DbConnectionBase
        where TId : struct
    {
        //[Import]
        //[Dependency]
        //public IPrincipal User { get; set; }

        [Import]
        [Dependency]
        public TConnection Connection { get; set; } //TODO как быстрое решение используется типизированное свойство. Потом переделать на интерфейс

        [Import]
        [Dependency]
        public IIdentifierProvider<TId> IdentifierProvider { get; set; }

        [Import]
        [Dependency]
        public ILoggingService logger { get; set; }

        protected IList<TEntity> Cache;

        /// <summary>Произвольный набор параметров, позволяющий уточнить характер выполнения
        /// того или иного метода
        /// </summary>

        /// <summary>
        /// Производит очитку кэша
        /// </summary>
        public void CacheClear()
        {
            Cache.Clear();
            Cache = null;
        }

        /// <summary>Переопределяется при необходимости
        /// подгрузки свойств из внешних контекстов
        /// </summary>
        public virtual TEntity Finalise(TEntity entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            return entity;
        }

        /// <summary>Позволяет прогружать отдельные свойства с типами внешних
        /// по отношению сущностей, что в целом ускоряет процесс финализации
        /// </summary>
        /// <param name="id">идентификатор свойства, наприер PersonId в Patient</param>
        /// <returns></returns>
        public virtual TEntity Finalise(TId id)
        {
            return Finalise(Get(id));
        }

        public virtual IList<TEntity> Finalise(IQueryable<TEntity> query)
        {
            if (query == null) throw new ArgumentNullException("query");
            return query.Select(x => Finalise(x)).ToList();
        }

        public virtual TEntity Add(TEntity entity, bool needSaveChanges = true)
        {
            if (entity == null) throw new ArgumentNullException("entity");

            entity.Id = IdentifierProvider.NewIdentifier();
            entity.CreatedDate = entity.ModifiedDate = DateTime.UtcNow;
            //entity.CreatedById = entity.ModifiedById = 1;// Convert.ToInt32(User.Identity.Name); TODO Допилить
            var entities = Connection.Set<TEntity>();

            var result = entities.Add(entity);

            if (needSaveChanges)
            {
                SaveChanges();
            }

            if (Cache != null)
            {
                Cache.Add(result);
            }

            return result;
        }

        /// <summary> TODO подумать как лучше реализовать
        /// </summary>
        public virtual IEnumerable<TEntity> Add(IEnumerable<TEntity> entities, bool needSaveChanges = true)
        {
            if (entities == null)
                return null;

            var result = new List<TEntity>();

            var identifiers = IdentifierProvider.NewIdentifiers(entities.Count()).GetEnumerator();

            foreach (var entity in entities)
            {
                identifiers.MoveNext();
                entity.Id = identifiers.Current;
                entity.CreatedDate = entity.ModifiedDate = DateTime.UtcNow;
                //entity.CreatedById = entity.ModifiedById = 1; // Convert.ToInt32(User.Identity.Name); TODO Допилить
                result.Add(entity);
            }

            Connection.GetSet<TEntity>().AddRange(result);

            if (needSaveChanges)
            {
                SaveChanges();
            }

            if (Cache != null)
            {
                result.ForEach(x => Cache.Add(x));
            }

            return result;
        }

        public virtual TEntity Update(TEntity entity, bool needSaveChanges = true)
        {
            if (entity == null) throw new ArgumentNullException("entity");

            if (EqualityComparer<TId>.Default.Equals(entity.Id, default(TId)))
            {
                // TODO требуется логирование
                Console.WriteLine("!!! В метод Update() попал объект, котрый еще не существует в контексте !!!");
                Console.ReadKey();
            }

            entity.ModifiedDate = DateTime.UtcNow;
            //entity.ModifiedById = 1;// Convert.ToInt32(User.Identity.Name); TODO Допилить

            //http://stackoverflow.com/questions/12724092/dbcontext-entry-performance-issue
            var entry = ((IObjectContextAdapter)Connection).ObjectContext.ObjectStateManager.GetObjectStateEntry(entity);

            entry.ChangeState(EntityState.Modified);

            entry.RejectPropertyChanges("CreatedDate");

            var result = (TEntity)entry.Entity;

            if (needSaveChanges)
            {
                SaveChanges();
            }
            return result;

        }

        /// <summary> Единоразовое добавление в контекст всех изменений в коллекции
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="needSaveChanges"></param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> Update(IEnumerable<TEntity> entities, bool needSaveChanges = true)
        {
            var result = new List<TEntity>();

            if (entities == null)
            {
                return result;
            }

            foreach (var entity in entities)
            {
                if (EqualityComparer<TId>.Default.Equals(entity.Id, default(TId)))
                {
                    // TODO требуется логирование
                    Console.WriteLine("!!! В метод Update() попал объект, котрый еще не существует в контексте !!!");
                    Console.ReadKey();
                }

                entity.ModifiedDate = DateTime.UtcNow;

                //http://stackoverflow.com/questions/12724092/dbcontext-entry-performance-issue
                var entry = ((IObjectContextAdapter)Connection).ObjectContext.ObjectStateManager.GetObjectStateEntry(entity);

                entry.ChangeState(EntityState.Modified);//Без этого обновление в БД не происходит

                entry.RejectPropertyChanges("CreatedDate");

                result.Add((TEntity)entry.Entity);
            }
            //Не для всех операций тредуется комит в БД. В групповых операциях экономичней сделать 
            //единый комит на множество изменений в контексте данных
            if (needSaveChanges)
            {
                SaveChanges();
            }

            return result;
        }

        /// <summary>
        /// TODO переписать метод в котором не будет производится фактическое удаление данных
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Delete(TEntity entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            Connection.GetSet<TEntity>().Remove(entity);
            //var type = entity.GetType().BaseType;
            SaveChanges();
        }

        public virtual void Delete(TId id)
        {
            Delete(Get(id));
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return GetAll(new DaoParametrs { IsWithDeleted = false, IsNeedCached = false });
        }

        public virtual IQueryable<TEntity> GetAll(DaoParametrs parameters)
        {
            if (parameters == null) throw new ArgumentNullException("parameters");
            IQueryable<TEntity> query;

            if (parameters.IsNeedCached)//Требуется кеширование
            {
                if (Cache == null)//Если ранеше данные в кеш не наполнялись
                {
                    Cache = Connection.GetSet<TEntity>().AsQueryable().ToList(); //Наполнить кеш
                }

                query = Cache.AsQueryable();
            }
            else
            {
                query = Connection.GetSet<TEntity>().AsQueryable();
            }

            if (!parameters.IsWithDeleted)
            {
                query = query.WhereNotDeleted<TEntity, TId>();
            }

            return query;
        }

        public virtual TEntity Get(TId id)
        {
            return Connection.GetSet<TEntity>().Find(id);
        }

        public virtual void Dispose()
        {
            Connection.Dispose();
        }
        /// <summary>
        /// Сохраняет изменения, накопившиеся в контексте в БД
        /// </summary>
        /// <param name="lockObject">объект на котором производится блокировка действия</param>
        public virtual void SaveChanges(object lockObject = null)
        {
            try
            {
                if (lockObject != null)
                {
                    lock (lockObject)
                    {
                        Connection.SaveChanges();
                    }
                }
                else
                {
                    Connection.SaveChanges();
                }
            }
            catch (OptimisticConcurrencyException ex)
            {
                //TODO пока не удается разрешить автоматически конфликт.
                //В перспективе желательно подойти к этому вопросу более грамотно
                //Некотрые драйвера позволяют перебирать конфликты и
                //делать попытки автоматического разрешения
                logger.Error<TEntity>("Конфликт одновременного обновления данных", ex);
            }
        }

        /// <summary>
        /// Создание версии данных
        /// </summary>
        /// <param name="entity"></param>
        public void Versioning(TEntity entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Получение данных с версией указаной даты
        /// </summary>
        /// <param name="id"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public TEntity Get(long id, DateTime date)
        {
            throw new NotImplementedException();
        }
    }
}