﻿using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.Migrations.Sql;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Npgsql;

namespace SmartHouse.Core.Api.Dao.Migration
{
    public class PatchedNpgsqlMigrationSqlGenerator : NpgsqlMigrationSqlGenerator
    {
        public override IEnumerable<MigrationStatement> Generate(IEnumerable<MigrationOperation> migrationOperations, string providerManifestToken)
        {
            var operations = migrationOperations as MigrationOperation[] ?? migrationOperations.ToArray();
            foreach (var operation in operations.OfType<ForeignKeyOperation>())
            {
                var hash = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(operation.Name)).Take(6).ToArray();
                var stringHash = string.Join("", hash.Select(o => o.ToString("X2")));
                operation.Name = "FK_" + string.Join("",string.Join("_", operation.DependentColumns).Take(40)) + "___" + stringHash;
            }
            return base.Generate(operations, providerManifestToken);
        }
    }
}
