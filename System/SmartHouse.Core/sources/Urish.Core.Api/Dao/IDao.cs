﻿using System;
using System.Collections.Generic;
using System.Linq;
using SmartHouse.Core.Api.Entity;

namespace SmartHouse.Core.Api.Dao
{

    public interface IDao<TEntity, in TId> : IDisposable
        where TEntity : BaseEntity<TId>, new()
        where TId : struct
    {
        IQueryable<TEntity> GetAll();

        IQueryable<TEntity> GetAll(DaoParametrs parameters);

        TEntity Add(TEntity entity, bool needSaveChanges = true);

        IEnumerable<TEntity> Add(IEnumerable<TEntity> entities, bool needSaveChanges = true);

        TEntity Update(TEntity entity, bool needSaveChanges = true);

        IEnumerable<TEntity> Update(IEnumerable<TEntity> entities, bool needSaveChanges = true);

        void Delete(TEntity entity);

        void Delete(TId id);

        TEntity Get(TId id);

        void SaveChanges(object lockObject = null);

        IList<TEntity> Finalise(IQueryable<TEntity> query);

        void CacheClear();

        /// <summary>
        /// Переопределяется при необходимости подгрузки свойств из внешних контекстов
        /// </summary>
        TEntity Finalise(TEntity entity);

        /// <summary>Позволяет прогружать отдельные свойства с типами внешних
        /// по отношению сущностей, что в целом ускоряет процесс финализации
        /// </summary>
        /// <param name="id">идентификатор свойства, наприер PersonId в Patient</param>
        /// <returns></returns>        
        TEntity Finalise(TId id);
    }
}
