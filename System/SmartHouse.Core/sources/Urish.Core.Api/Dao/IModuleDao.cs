﻿using System;
using SmartHouse.Core.Api.Entity;

namespace SmartHouse.Core.Api.Dao
{
    public interface IModuleDao : IDao<BaseModule, Guid>
    {

    }
}
