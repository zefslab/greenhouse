﻿using System;
using SmartHouse.Core.Api.Entity;

namespace SmartHouse.Core.Api.Dao.Mapping
{
    public class ModuleMap : NamedEntityMap<BaseModule,Guid>
    {
        public ModuleMap()
        {
            Property(u => u.CurrentVersion).HasColumnName("current_version");
            Property(u => u.IsSystem).HasColumnName("is_system");
            ToTable("modules", "core");
        }
    }
}
