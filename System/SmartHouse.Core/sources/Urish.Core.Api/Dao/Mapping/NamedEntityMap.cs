﻿using SmartHouse.Core.Api.Entity;

namespace SmartHouse.Core.Api.Dao.Mapping
{
    public class NamedEntityMap<TEntity, TId> : BaseEntityMap<TEntity, TId>
        where TEntity : NamedEntity<TId>
        where TId : struct
    {
        public NamedEntityMap()
        {
            Property(u => u.Name).HasColumnName("name");
        }
    }
}
