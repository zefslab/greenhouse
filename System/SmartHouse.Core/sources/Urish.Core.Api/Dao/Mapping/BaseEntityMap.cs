﻿using System.Data.Entity.ModelConfiguration;
using SmartHouse.Core.Api.Entity;

namespace SmartHouse.Core.Api.Dao.Mapping
{
    public abstract class BaseEntityMap<TEntity, TId> : EntityTypeConfiguration<TEntity>
        where TEntity : BaseEntity<TId>
        where TId : struct
    {
        protected BaseEntityMap()
        {
            Property(u => u.Id).HasColumnName("id");
            Property(u => u.CreatedDate).HasColumnName("created_date");
            Property(u => u.ModifiedDate).HasColumnName("modified_date");
            Property(u => u.DeletedDate).HasColumnName("deleted_date");
            
            //Причина отключения - http://jira.komiac.ru:8081/browse/URA-57?focusedWorklogId=12056&page=com.atlassian.jira.plugin.system.issuetabpanels%3Aworklog-tabpanel#worklog-12056
            //Ignore(u => u.CreatedBy);
            //Ignore(u => u.ModifiedBy);
        }
    }
}
