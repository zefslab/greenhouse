﻿using System;
using SmartHouse.Core.Api.Entity;

namespace SmartHouse.Core.Api.Services
{
    /// <summary>
    /// Определяет методы реализующие версионирование данных импортируемых сущностей
    ///  </summary>
    public interface IVersioningService<in TEntity, out TVersion, in TId>
        where TEntity : BaseEntity<TId> //Сущность которую нужно заверсионировать
        where TVersion : TEntity, IVersionEntity<TId> //Сущность в которую производится сохранение версий
        where TId : struct
    {
        /// <summary> Создание версии данных
        /// </summary>
        /// <param name="entity">сущность, которую нужно версионировать</param>
        /// <returns>Версия сущности</returns>
        TVersion CreateVersion(TEntity entity);

        /// <summary> Получение данных с версией указаной даты
        /// </summary>
        /// <param name="id">идентификатор сущности</param>
        /// <param name="date">дата, на которую нужно вернуть версию данных</param>
        /// <returns>Версия сущности</returns>
        TVersion GetVersion(TId id, DateTime date);
    }
}
