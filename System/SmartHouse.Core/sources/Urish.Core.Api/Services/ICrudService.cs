﻿/// ****************************************************************************************
/// Описываются всемозможные интерфейсы, которые могут навешиваться на отдельные сервисы
/// с требованием их реализации
/// ****************************************************************************************

using System.Collections.Generic;
using System.Linq;
using SmartHouse.Core.Api.Dto;
using SmartHouse.Core.Api.Entity;
using SmartHouse.Core.Api.Filter;

namespace SmartHouse.Core.Api.Services
{
    public interface ICrudService <TEntity, TDto, TId> : IGetListSupport<TEntity, TId> 
                                                       , IUpdateSupport<TEntity, TDto, TId>
                                                       , ICreateSupport<TEntity, TDto, TId>
                                                       , IGetSingleSupport<TEntity, TId>
        where TEntity : BaseEntity<TId>, new ()
        where TDto : BaseImportDto<TId>
        where TId : struct
    {
       
    }

    public interface IGetListSupport<TEntity, TId> 
        where TEntity : BaseEntity<TId>
        where TId : struct
    {
        IEnumerable<TEntity> Get(BaseFilter filters, IList<SortingInfo> sorters=null);

        IList<TEntity> Finalise(IQueryable<TEntity> entities);
    }

    public interface IGetSingleSupport<TEntity, in TId> 
        where TEntity : BaseEntity<TId>, new()
        where TId : struct
    {
        TEntity Get(TId id);

        TEntity Finalise(TEntity entity);

        TEntity Finalise(TId entity);
    }
    
    /// <summary>
    /// В контекст добавляется новая инстанция но основе импортированных данных
    /// </summary>
    /// <typeparam name="D">структура dto содержащая импортированные данные</typeparam>
    public interface ICreateSupport<TEntity, TDto, TId> 
        where TEntity : BaseEntity<TId>, new()
        where TDto : BaseImportDto<TId>
        where TId : struct
    {
        TEntity Create(TDto dto);
    }

    /// <summary> Обновляется уже сущестующая инстанция на основе импортированных данных
    /// Каждая сущность реализует эту процедуру согласно собственной бизнес логике
    /// </summary>
    /// <typeparam name="TDto">структура dto содержащая импортированные данные</typeparam>
    public interface IUpdateSupport<TEntity, TDto, TId> 
        where TEntity : BaseEntity<TId>, new()
        where TDto : BaseImportDto<TId>
        where TId : struct
    {
        TEntity Update(TEntity entity, TDto dto);
    }

    public interface IDeleteSupport<in TId>
        where TId : struct
    {
        void Delete(TId id);
    }

    public interface IUpdateListSupport<TEntity, TDto, TId>
        where TEntity : BaseEntity<TId>
        where TDto : BaseImportDto<TId>
        where TId : struct
    {
        IList<TEntity> UpdateList(IList<TDto> values);
    }
}
