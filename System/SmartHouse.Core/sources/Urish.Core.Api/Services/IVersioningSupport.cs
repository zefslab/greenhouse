﻿

using SmartHouse.Core.Api.Entity;

namespace SmartHouse.Core.Api.Services
{
    /// <summary>Интерфейс определяет свойства и методы позволяющие сервису сущности реализовывать версионирование
    /// </summary>
    public interface IVersioningSupport<TEntity, TVersion, TId>
        where TEntity : BaseEntity<TId>
        where TVersion : TEntity, IVersionEntity<TId>
        where TId : struct
    {
        /// <summary> Сервис реализующий версионирование данных
        /// 
        /// </summary>
        IVersioningService<TEntity, TVersion, TId> VersioningService { get; set; }

        /// <summary>Версионирование включено
        /// 
        /// </summary>
        bool IsVersioningEnable { get; set; }
        
        //TODO ...

    }
}
