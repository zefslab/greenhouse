﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using SmartHouse.Core.Api.Dao;
using SmartHouse.Core.Api.Dto;
using SmartHouse.Core.Api.Entity;
using SmartHouse.Core.Api.Filter;
using SmartHouse.Core.Api.Utilities;
using Urish.Diagnostic.Services.API;

namespace SmartHouse.Core.Api.Services
{

    public class SortingInfo
    {
        public string Field { get; set; }
        public string Direction { get; set; }
    }

    /// <summary>
    /// Базовый сервис призван реализовать типовые CRUD операции для сущностей
    /// </summary>
    /// <typeparam name="TEntity">тип сущности для которой реализуются типовые операции</typeparam>
    public class CrudService<TEntity, TDto, TId> : ICrudService<TEntity, TDto, TId>
        where TEntity : BaseEntity<TId>, IImportedEntity, new()
        where TDto : BaseImportDto<TId>
        where TId : struct
    {
        /// <summary>
        /// Репозиторий главной сущности для которой будет реализован прикладной сервис
        /// Важно, чтобы сервисы сущностей не могли использовать репозитории других сущностей
        /// в обход бизнес логики реализованной в сервисах этих сущностей.
        /// TODO подумать как сделать недоступным это свойство внешним объектам 
        /// </summary>
        protected IDao<TEntity, TId> Repository;

        protected ILoggingService Logger;

        public CrudService(IDao<TEntity, TId> repository, ILoggingService logger)
        {
            if (repository == null) throw new ArgumentNullException(nameof(repository));
            if (Logger == null) throw new ArgumentNullException(nameof(logger));

            Repository = repository;
            Logger = logger;
        }

        public virtual TEntity Finalise(TEntity entity)
        {
            return Repository.Finalise(entity);
        }

        public virtual TEntity Finalise(TId entity)
        {
            return Repository.Finalise(entity);
        }

        public virtual IList<TEntity> Finalise(IQueryable<TEntity> entities)
        {
            return Repository.Finalise(entities);
        }

        /// <summary> Получает сущность по идентификатору
        /// Некоторые справочники переопределяют этот метод, т.к. требуется больше параметров
        /// для поиска записи. Например, медицинские организации, их подразделения. 
        /// </summary>
        /// <param name="id">код версии записи</param>
        public TEntity Get(TId id)
        {
            return Repository.Get(id);
        }

        /// <summary> Предоставляет отфильтрованный и отсортированный список сущностей
        /// </summary>
        /// <param name="filters">набор фильтрующих критериев</param>
        /// <param name="sorters">очень редко используемый параметр</param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> Get(BaseFilter filter, IList<SortingInfo> sorters=null)
        {
            var result = _getList();

            #region Наиболее часто используемый фильтр в методе Seek()
            if (filter != null)
            {
                if (filter.InformationSystemId != null)
                {
                    result = result.Where(x => x.InformationSystemId == filter.InformationSystemId);
                }

                if (filter.ExternalId != null)
                {
                    result = result.Where(x => x.ExternalId == filter.ExternalId);
                }
            }
            #endregion

            //дополнительная фильтрация с учетом частных фильтров отдельных сущностей
            result = _doFiltration(result, filter);

            if (sorters != null)
            {
                result = _doSorting(result, sorters);
            }

            return result;
        }

        /// <summary> Простейшая реализация получения всех записей сущностей из репозитория
        /// Может быть переопределена в сервисах-наследниках
        /// </summary>
        /// <returns></returns>
        protected virtual IQueryable<TEntity> _getList()
        {
            return Repository.GetAll();
        }

        /// <summary> Формируется запрос с учетом переданных в качестве параметра фильтров
        ///
        /// </summary>
        /// <param name="query">исходный запрос</param>
        /// <param name="filters">фильтр с критериями фильтрации</param>
        /// <returns>запрос с наложенными фильтрующими критериями</returns>
        protected virtual IQueryable<TEntity> _doFiltration(IQueryable<TEntity> query, object filters)
        {
            return query;
        }

        /// <summary> Формируется запрос с учетом переданных в качестве параметра условий сортировки
        /// 
        /// </summary>
        /// <param name="query">исходный запрос</param>
        /// <param name="sorters">условия сортировки</param>
        /// <returns>результирующий запрос с учетом порядка сортировки</returns>
        protected virtual IQueryable<TEntity> _doSorting(IQueryable<TEntity> query, IList<SortingInfo> sorters)
        {
            return query.OrderByDescending(x => x.CreatedDate);
        }

        public TEntity Update(TEntity entity, TDto dto)
        {
            if (entity != null)
            {
                /// В процессе предварительного сравнения данных из dto
                /// с данными в системе выявляется необходимость их обновления
                /// что повлечет за собой создание версии данных
                if (dto.Equals(Repository.Finalise(entity)))
                {
#if DEBUG
                    Mconsole.WriteLine(
                        new Mstring(
                            String.Format("Игнорировано обновление сущности : {0} в следствии ее идентичности",
                                dto.GetType().Name), ConsoleColor.DarkYellow));
#endif
                    Logger.Trace<TEntity>(String.Format("Игнорировано обновление сущности : {0} в следствии ее идентичности",
                                dto.GetType().Name));

                }
                else
                {
                    entity = _update(entity, dto);

                    entity = Repository.Update(Mapper.Map(dto, entity));
                    
                }

                //Версионирование записи данных обновленнной сущности
                //VersioningService.CreateVersion(entity);
            }
            else
            {
                //TODO если таких сообщение приходить не будет, то удалить конструкцию if проверяющую на null
                // как избыточную
#if DEBUG
                Mconsole.WriteLine(new Mstring("При обновлении сущность содержит значение null!!!", ConsoleColor.Red));
                Console.ReadKey();
#endif
            }
            return entity;
        }

        /// <summary> Обновляются специфические для конкретной сущности ссылочные свойства
        /// </summary>
        protected virtual TEntity _update(TEntity entity, TDto dto)
        {
            return entity;
        }

        /// <summary> Создает новую сущность
        /// </summary>
        public TEntity Create(TDto dto)
        {
            var entity = Mapper.Map(dto, new TEntity());

            entity  = _create(entity, dto);

            //по какой либо причине сущность может быть не создана
            //например для мед. записи выдачи медикаментов не нашлось соответствия в справочнике
            if (entity != null)
            {
                entity = Repository.Add(entity);
            }

            return entity;
        }

        /// <summary> Реализует в сервисе конкретной сущности создание ссылочных свойств
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        protected virtual TEntity _create(TEntity entity, TDto dto)
        {
            return entity;
        }

       

    }
}
