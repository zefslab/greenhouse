﻿namespace SmartHouse.Core.Api.Dpo
{
    public abstract class BaseDpo<TId> where TId : struct
    {
       public TId Id { get; set; }
    }
}
