﻿namespace SmartHouse.Core.Api.Dto
{
    /// <summary>
    /// Базовый класс для всех экспортируемых структур данных
    /// </summary>
    public abstract class BaseExportDto<TId>
        where TId : struct
    {
        /// <summary>
        /// Идентификатор сущности из  системы ЕРИСЗ
        ///  </summary>
        public TId Id { get; set; }

    }
}
