﻿using System;
using SmartHouse.Core.Api.Entity;

namespace SmartHouse.Core.Api.Dto
{
    /// <summary>
    /// Базовый класс для всех импортируемых структур данных
    /// </summary>
    public abstract class BaseImportDto<TId> //: IEquatable<BaseEntity<TId>> 
        where TId : struct
    {
        /// <summary>
        /// Заполняется после инициализации сущности, чтобы в процессе импорта
        /// ссылочных свойств этой сущности можно было обращаться к ней для инициализации
        /// свойств ссылающихся на родительскую сущность-контейнер
        /// </summary>
        public BaseEntity<TId> Entity { get; set; }

        /// <summary>
        /// Унаследованный  идентификатор из БД ГИБДД
        /// </summary>
        public long? ExternalId { get; set; }

        /// <summary>
        /// Указание на запись в таблице импорта с исходными данными
        /// </summary>
        public Guid ImportedDataId { get; set; }

        /// <summary>
        /// Идентификатор информационной системы из которой импортируются данные
        /// Он требуется при импорте данных для определения наличия данных в системе
        /// передаваемых из внешнего источника.
        /// Сочетание его с ExternalId конкретной мед. записи образует уникальный ключ.
        /// </summary>
        public Guid InformationSystemId { get; set; }

        /// <summary>Сравнивает данные хранящиеся в БД с теми, которые пришли
        /// через механизм импорта.
        /// Во всех dto классах следует переопределить этот метод, чтобы не обновлять 
        /// данные в БД идентичными, пришедшими из внешнего источника.
        /// Для dto классов у которых этот метод не переопределен считается, что импортируемые
        /// данные нужно всегда обновлять.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual bool Equals(BaseEntity<TId> obj)
        {
            return false;
        }
        
        /// <summary>
        /// Преопределяемый метод валидации сущности.
        /// Чаще всего он будет вызываться перед сохранением объекта в БД 
        /// </summary>
        /// <returns></returns>
        public virtual ProcessingResult<T> Validate<T>()
            where  T : BaseImportDto<Guid>
        {
            return new ProcessingResult<T>();
        }
    }
}
