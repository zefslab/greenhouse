﻿namespace SmartHouse.Core.Api.Dto
{
    /// <summary>
    /// Базовый класс для именованных  импортируемых структур данных
    /// </summary>
    public abstract class NamedImportDto<TId> : BaseImportDto<TId>
        where TId : struct
    {
        public string Name { get; set; }
    }
}
