﻿using System;
using SmartHouse.Core.Api.Constant;

namespace SmartHouse.Core.Api.Entity
{
    /// <summary>
    /// Базовый класс для сущностей модели данных в системе
    /// Вводит уникальный идентификатор записи, а также данные о дате создания,
    /// изменения и удаления сущности и субъектах этих операций
    /// </summary>
    /// <typeparam name="TId">Тип уникального идентификатора записи</typeparam>
    public abstract class BaseEntity<TId> where TId : struct
    {
        /// <summary>
        /// Идентификатор записи, уникальный для всех записей одного типа в пределах системы
        /// Назначается записи при её создании и более не изменяется
        /// </summary>
        public TId Id { get; set; }

        /// <summary>
        /// Дата создания записи в UTC времени. Устанавливается единожды при создании и остаётся неизменной
        /// в течение всего срока жизни сущности
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Инициатор создания записи
        /// http://jira.komiac.ru:8081/browse/URA-57?focusedWorklogId=12056&page=com.atlassian.jira.plugin.system.issuetabpanels%3Aworklog-tabpanel#worklog-12056
        /// </summary>
        //public IInitiator CreatedBy { get; set; }

        /// <summary>
        /// Дата последнего изменения записи в UTC времени. Отражает только существенные изменения
        /// в данных записи, а также в её связях и зависимых записях
        /// </summary>
        public DateTime ModifiedDate { get; set; }

        /// <summary>
        /// Инициатор последнего изменения записи
        /// http://jira.komiac.ru:8081/browse/URA-57?focusedWorklogId=12056&page=com.atlassian.jira.plugin.system.issuetabpanels%3Aworklog-tabpanel#worklog-12056
        /// </summary>
        //public IInitiator ModifiedBy { get; set; }

        /// <summary>
        /// Дата удаления сущности в UTC времени. Сущность не удаляется из системы, но лишь помечается как удалённая после
        /// определённой даты. Запись считается удалённой, если дата удаления задана и меньше либо равна дате на текущий момент
        /// </summary>
        public DateTime? DeletedDate { get; set; }

        /// <summary>
        /// Преопределяемый метод валидации сущности.
        /// Чаще всего он будет вызываться перед сохранением объекта в БД 
        /// </summary>
        /// <returns></returns>
        public virtual ProcessingResults Validate()
        {
            return ProcessingResults.NonResult;
        }
    }
}