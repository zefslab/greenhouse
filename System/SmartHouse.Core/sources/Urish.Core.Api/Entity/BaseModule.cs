﻿using System;

namespace SmartHouse.Core.Api.Entity
{
    /// <summary>
    /// Базовый класс для всех модулей ЕРИСЗ
    /// </summary>
    public class BaseModule : NamedEntity<Guid>
    {
        /// <summary>
        /// Текущая версия модуля
        /// </summary>
        public string CurrentVersion { get; set; }

        /// <summary>
        /// Системный модуль - true, прикладной модуль - false
        /// </summary>
        public bool IsSystem { get; set; }
    }

 
}
