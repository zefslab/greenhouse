﻿namespace SmartHouse.Core.Api.Entity
{
    /// <summary>
    /// Базовый класс для именованных записей
    /// </summary>
    /// <typeparam name="TId">Тип уникального идентификатора записи</typeparam>
    public abstract class NamedEntity<TId> : BaseEntity<TId> where TId : struct
    {
        /// <summary>
        /// Имя/наименование записи
        /// </summary>
        public string Name { get; set; }
    }
}