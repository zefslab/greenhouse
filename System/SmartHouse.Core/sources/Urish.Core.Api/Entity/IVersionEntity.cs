﻿namespace SmartHouse.Core.Api
{
    /// <summary>
    /// Наследуется версионируемыми сущностями
    /// </summary>
    public interface IVersionEntity<TId>
        where TId : struct
    {
        /// <summary>
        /// ссылка на базовую сущность от которой унаследована версионная сущность
        /// </summary>
        TId BaseId { get; set; }
    }
}
