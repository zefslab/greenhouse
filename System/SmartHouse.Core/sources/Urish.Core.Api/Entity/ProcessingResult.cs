﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Microsoft.Practices.Unity;
using SmartHouse.Core.Api.Constant;
using Urish.Diagnostic.Services.API;

namespace SmartHouse.Core.Api.Entity
{
    /// <summary> Обертка, позволяющая вместе с самим результатом обработки данных
    /// полученных в результате выполнения сложно-составного процесса
    /// имет дополнительные данные описывающие этот результат
    /// </summary>
    public class ProcessingResult<T>
    {
        /// <summary>
        /// Данные, как результат выполнения какой либо операции
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Суммирующий результат выполнения операции
        /// </summary>
        public ProcessingResults Result { get; set; }

        /// <summary>
        /// Перечень обнаруженных ошибок в процессе выполения операции
        /// </summary>
        public List<Error> Errors { get; set; }

        /// <summary>
        /// Произвольное описание которое может быть отправлено или залогировано
        /// </summary>
        public string Description { get; set; }

        [Import]
        [Dependency]
        public ILoggingService Logger { get; set; }

        [Import]
        [Dependency]
        public INotificationService Notifier { get; set; }

        public ProcessingResult()
        {
            Errors = new List<Error>();
            Result = ProcessingResults.NonResult;
            Description = String.Empty;
        }

        /// <summary>
        /// Общий метод логирования результатов выполнения какой либо операции
        /// </summary>
        public void LoggingResult()
        {
            _loggingResult();
        }

        /// <summary>
        /// Логируются все свойства (накопленне результаты) текущего, базового класса
        /// </summary>
        /// <returns></returns>
        protected virtual void _loggingResult()
        {
            const string FATAL_ERROR_MESSAGE ="ЕРИСЗ: фатальная ошибка возникла в приложении в процессе обработки данных!";

            const string CRITICAL_ERROR_MESSAGE = "ЕРИСЗ: критическая ошибка возникла в приложении в процессе обработки данных!";

            const string EMAIL = "zef@kuzdrav.ru";

            ///Позаимствовал код логирования из класса BaseSystematization и метода LoggingAndNotify()
            var nl = Environment.NewLine;
            Errors.ForEach(e =>
            {
                var note = String.Empty;

                switch (e.Level)
                {
                    case ErrorLevels.Fatal:
                        //Логировать в файл подробную информацию
                        note = Data + nl
                               + Description + nl
                               + e.FullDescription + nl;
                        Logger.Fatal<T>(note, e.Exception);

                        //Логировать на консоль краткую информацию информацию для оперативного монитроинга
                        note = Data + nl
                               + e.Message + nl;
                        Logger.Fatal<T>(note, e.Exception);

                        //Логировать в БД подробную, структурированную информацию
                        note = Data + nl
                               + Description + nl
                               + e.JsonMessage + nl;
                        Logger.Fatal<T>(note);

                        Notifier.SendEmailNotification(FATAL_ERROR_MESSAGE, note, new[] { EMAIL });
                        break;
                    case ErrorLevels.Critical:
                        note = Data + nl
                               + Description + nl
                               + e.Message + nl;

                        Logger.Error<T>(note);

                        Notifier.SendEmailNotification(CRITICAL_ERROR_MESSAGE, note, new[] { EMAIL });

                        break;
                    case ErrorLevels.Warnings:
                        note = Data.GetType() + nl
                               + e.Message + nl;

                        Logger.Warn<T>(note);

                        break;

                    case ErrorLevels.NotCritical:

                        note = e.Message + nl;

                        Logger.Debug<T>(note);

                        break;
                    case ErrorLevels.Message:

                        note = e.Message + nl;

                        Logger.Trace<T>(note);

                        break;
                }
            });
        }

        public static ProcessingResult<T> operator +(ProcessingResult<T> result1, ProcessingResult<T> result2)
        {
            
            var outResult = new ProcessingResult<T>();
            if (result1 != null && result2 != null)
            {
                //outResult.Description += result1.Description;
                //outResult.Description += result2.Description;

                //суммирование накопленных ошибок 
                //outResult.Errors = result1.Errors.Concat(result2.Errors).ToList();

                outResult.Result = SummingResults(result1.Result, result2.Result);
            }
            else if (result1 != null)
            {
                outResult = result1;
            }
            else
            {
                outResult = result2;
            }

            //TODO не понятно пока как объединять данные и нужно ли вообще
            //Думается, что особой нужды суммировать данные нет, т.к. интерес
            //при выполнеии этой операции представляет именно ошибки, описание и общий результат

            return outResult;
        }

        
        /// <summary>
        /// Определение итогового результата при суммировании двух результатов
        /// </summary>
        /// <param name="result1">Суммирующий результат первой операции</param>
        /// <param name="result2">Суммирующий результат второй операции</param>
        /// <returns></returns>
        private static ProcessingResults SummingResults(ProcessingResults result1, ProcessingResults result2)
        {
            if (result1 == ProcessingResults.Error || result2 == ProcessingResults.Error)
            {
                return ProcessingResults.Error;
            }
            if (result1 == ProcessingResults.NotCritical || result2 == ProcessingResults.NotCritical)
            {
                return ProcessingResults.NotCritical;
            }
            if (result1 == ProcessingResults.Successfull || result2 == ProcessingResults.Successfull)
            {
                return ProcessingResults.Successfull;
            }
            return ProcessingResults.NonResult;
        }
    }
}
