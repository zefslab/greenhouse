﻿using System;
using SmartHouse.Core.Api.Constant;

namespace SmartHouse.Core.Api.Entity
{
    /// <summary>
    /// Ошибки возникающие в каком либо процессе 
    /// </summary>
    public class Error
    {
        public ErrorCodes Code { get; set; }

        public ErrorLevels Level { get; set; }

        /// <summary>
        /// Краткое сообщение для логирования на консоль и в большие файлы логов
        /// Используется в большинстве случаев для мониторинга
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Наиболее полное описание ошибки.
        /// Используется для выяснения причин возникновения ошибки
        /// Поэтому здесь должно собираться максимум мнформации, в том числе и 
        /// идентификаторы обрабатывемых данных.
        /// </summary>
        public string FullDescription { get; set; }

        /// <summary>
        /// Структурированное описание ошибки для записи в БД
        /// с целью упрощения атоматизированной или полуавтоматизированнной обработки
        /// </summary>
        public string JsonMessage { get; set; }

        public Exception Exception { get; set; }

        public Error(string message)
        {
            Message = message;
            Level = ErrorLevels.NotCritical;
        }

        public Error(string message, ErrorLevels level) : this(message)
        {
            Level = level;
        }

        public Error(string message, ErrorLevels level, ErrorCodes code) : this(message, level)
        {
            Code = code;
        }
    }
}
