﻿using System;

namespace SmartHouse.Core.Api.Entity
{
    /// <summary>
    /// Требует реализации для импортируемых данных
    /// </summary>
    public interface IImportedEntity
    {
        /// <summary>
        /// Идентификатор унаследованный из внешних систем при импортировании данных
        /// </summary>
        long? ExternalId { get; set; }

        /// <summary>
        /// Информационная система 
        /// к которой принадлежит импортированная запись
        /// </summary>
        Guid InformationSystemId { get; set; }

        /// <summary>
        /// Идентификатор ссылающийся на запись с импортированными данными, 
        /// хранящимися в исходном виде
        /// </summary>
        Guid ImportedDataId { get; set; }

    }
}
