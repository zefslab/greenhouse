﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Reflection;

namespace SmartHouse.Core.Api
{
    /// <summary>
    /// Используется системными и прикладными модулями для обеспечения единообразного их функционирования
    /// </summary>
    public interface IBaseModule
    {
        /// <summary>
        /// Конфигурация настроек мигратора для модуля
        /// </summary>
        DbMigrationsConfiguration MigrationConfiguration { get; }

        /// <summary>
        /// Используется в Runtime для добавления в кнтейнер зависимостей
        /// </summary>
        DbContext DbConnection { get; }


        /// <summary>
        /// Каждый модуль должен предоставить информацию о сборках,
        /// в которых есть классы для определения зависимостей
        /// </summary>
        /// <returns></returns>
        IList<Assembly> GetAssembliesForUnityContainer();
    }
}
