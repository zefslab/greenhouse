﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using Microsoft.Practices.Unity;
using Moq;
using SmartHouse.Core.Api.Dao;
using SmartHouse.Core.Api.Utilities.Unity.Attrributes;

namespace SmartHouse.Core.Api.Utilities.Unity
{
    /// <summary>
    /// Собирает по всему проекту классы, в которых есть атрибут, описывающий внедрение зависимостей и регистрирует их в Unity контейнере
    /// </summary>
    public static class UnityConfigurer
    {
        public static IUnityContainer ConfigureUnityContainer(IUnityContainer container, IList<Assembly> assemblies)
        {
            if (container == null) throw new ArgumentNullException("container");
            if (assemblies == null) throw new ArgumentNullException("assemblies");

            //container.AddNewExtension<Interception>();
            container.RegisterInstance(container);
            //container.RegisterType<DbConnection>(new PerRequestLifetimeManager(), new InjectionFactory(c => new DbConnection()));
            container.RegisterType(typeof(IPrincipal), typeof(NullPrincipal));

            //Регистрируются зависимости уровня ядра
            _registerTypes(typeof(BaseDao<,,>).Assembly, container); //Репозитории
            //... Сервисы


            //Регистрируются зависимости уровня прикладного модуля
            foreach (var assembly in assemblies)
            {
                _registerTypes(assembly, container);
            }

            return container;
        }

        public static IUnityContainer UnityContainerBuilder(IList<Assembly> assemblies)
        {
            return ConfigureUnityContainer(new UnityContainer(), assemblies);
        }

        public class NullPrincipal : IPrincipal
        {
            public bool IsInRole(string role)
            {
                return false;
            }

            public IIdentity Identity { get; private set; }
        }


        /// <summary> Найденные в сборке классы на которых висит атрибут указывающий на внедрение зависимостей
        ///  регистрирует в переданном Unity контейнере
        /// ComponentAttribute - это родительский атрибут для всех атрибутов предназначенных для отметки классов использующих внедрение зависимостей
        /// </summary>
        private static void _registerTypes(Assembly assembly, IUnityContainer container)
        {
            var temp = assembly.GetTypes();

            var temp2 =  
                //Выбираются типы из указанной сборки
                temp.Select(t => new
                {
                    type = t,
                    componentAttributes =
                        t.GetCustomAttributes(typeof (ComponentAttribute), true)
                            //Выбираются атрибуты производные от указанного типа ComponentAttribute
                            .Cast<ComponentAttribute>() //Преобразуется к указанному типу ComponentAttribute
                            .ToList()
                })
                .Where(t => t.componentAttributes.Count() > 0)
                // у которых есть хоть один атрибут унаследованный от ComponentAttribute
                .ToList();

                temp2.ForEach(t => t.componentAttributes.ForEach(a =>
                {
                    if (a is ConfigurationAttribute) //Атрибут класса устанавливающего зависимость для различных конфигураций
                    {//Для регистрации конфигураций
                        container.RegisterType(t.type, t.type, new ContainerControlledLifetimeManager());
                    }
                    //else if(a is ConnectionAttribute)
                    //{//Для регистрации подключений к БД
                    //    container.RegisterType(t.type, t.type, new ContainerControlledLifetimeManager());
                    //}
                    else if (a is ParseBehaviorAttribute || a is ParseBehaviorMapAttribute)
                    {//Для регистрации в контейнере стратегий парсинга и их мапингов
                            ///Используется для регистрации именованных типов мапируемых на один
                            /// и тот же  базовый класс, чтобы контейнер их различал
                            container.RegisterType(a.ForInterface, t.type, t.type.Name);
                    }
                    else
                    {
                        //Для регистрации репозиториев и сервисов
                        container.RegisterType(a.ForInterface, t.type, new ContainerControlledLifetimeManager());

                        //TODO Не понятно что происходит в этом коде. Из-за ошибке пока отключен
                        //if (a is ServiceAttribute) //Атрибут класса устанавливающего зависимость для сервисов
                        //{
                        //    c.Configure<Interception>()
                        //        .SetInterceptorFor(a.ForInterface, new InterfaceInterceptor());
                        //}
                    }
                }));

            // регистрация фабрик компонентов, получаемых из контейнера
            container.RegisterType<Func<Type, string, object>>(
                new InjectionFactory(c => new Func<Type, string, object>((t, n) => c.Resolve(t, n))));
            container.RegisterType<Func<Type, IEnumerable<object>>>(
                new InjectionFactory(c => new Func<Type, string, IEnumerable<object>>((t, n) => c.ResolveAll(t))));
            
            var testAllConnections = container.Registrations.Where(conf => conf.RegisteredType.Name == "DbConnection");

            temp2.ForEach(x => x.componentAttributes.ForEach(a =>
            {
                if (a is ConfigurationAttribute)
                    //Сам Automapper не инстацирует зависимости, поэтому нужно предварительно 
                    //разрешить все зависимости прописанные в его конфигурациях
                    container.Resolve(x.type, null);
            }));

            foreach (var interfaceType in temp
                .Where(t => t.IsInterface && t.GetCustomAttribute<FakeImplementableAttribute>() != null))
            {
                // проверка, если для интерфейса уже есть его регистрация в контейнере, то не выполнять 
                // регистрацию фэйковой реализации
                if (!container.Registrations.Select(o => o.RegisteredType).Contains(interfaceType))
                {
                    container.RegisterInstance(interfaceType, CreateFaceForInterface(interfaceType),
                    new ContainerControlledLifetimeManager());
                }
            }
        }

        private static object CreateFaceForInterface(Type interfaceType)
        {
            var constructorInfo = typeof(Mock<>).MakeGenericType(interfaceType).GetConstructor(new Type[0]);
            if (constructorInfo == null) return null;
            var fake = (Mock)constructorInfo.Invoke(new object[0]);
            return fake.Object;
        }
    }
}
