﻿using ATTrade.Utils.Transaction;

namespace uRISH.Unity.Attributes
{
    using System;

    using Microsoft.Practices.Unity;
    using Microsoft.Practices.Unity.InterceptionExtension;

    [AttributeUsage(AttributeTargets.Method)]
    public class TransactionalAttribute : HandlerAttribute
    {
        public override ICallHandler CreateHandler(IUnityContainer container)
        {
            return new TransactionalCallHandler(container);
        }
    }
}
