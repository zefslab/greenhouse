﻿using System;

namespace SmartHouse.Core.Api.Utilities.Unity.Attrributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ConnectionAttribute : ComponentAttribute
    {
        public ConnectionAttribute() :
            base(null)
        {
        }
    }
}
