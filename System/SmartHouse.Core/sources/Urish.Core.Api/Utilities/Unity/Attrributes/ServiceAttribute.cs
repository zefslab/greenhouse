﻿using System;

namespace SmartHouse.Core.Api.Utilities.Unity.Attrributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class ServiceAttribute : ComponentAttribute
    {
        /// <summary> Наименование записи в контейнере под котрой регистрируется инстанцируемый тип.
        /// Это нужно для регистрации в контейнере множества различных типов под одним
        /// интерфейсом или базовым типом и в последствии инстанцирования коллекции 
        /// Enumerable из этих типов
        /// </summary>
        public string Name;
        
        public ServiceAttribute(Type forInterface) 
            : base(forInterface)
        {
        }

        public ServiceAttribute(Type forInterface, string name = null)
            : base(forInterface)
        {
            Name = name;
        }

    }
}