﻿using System;

namespace SmartHouse.Core.Api.Utilities.Unity.Attrributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ComponentAttribute : Attribute
    {
        public Type ForInterface { get; set; }

        public ComponentAttribute(Type forInterface)
        {
            ForInterface = forInterface;
        }
    }
}
