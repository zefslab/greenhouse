﻿using System;

namespace SmartHouse.Core.Api.Utilities.Unity.Attrributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ParseBehaviorAttribute : ComponentAttribute
    {
        public ParseBehaviorAttribute(Type forType) 
            : base(forType)
        {
        }
    }
}