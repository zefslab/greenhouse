﻿using System;

namespace SmartHouse.Core.Api.Utilities.Unity.Attrributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class RepositoryAttribute : ComponentAttribute
    {
        public RepositoryAttribute(Type forInterface) : base(forInterface)
        { 
        }
    }
}
