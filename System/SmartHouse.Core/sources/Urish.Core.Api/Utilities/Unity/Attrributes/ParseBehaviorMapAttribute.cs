﻿using System;

namespace SmartHouse.Core.Api.Utilities.Unity.Attrributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ParseBehaviorMapAttribute : ComponentAttribute
    {
        public ParseBehaviorMapAttribute(Type forType) 
            : base(forType)
        {
        }
    }
}