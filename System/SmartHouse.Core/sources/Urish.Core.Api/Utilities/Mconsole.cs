﻿using System;
using System.Threading;
using Microsoft.Practices.ObjectBuilder2;

namespace SmartHouse.Core.Api.Utilities
{
    /// <summary>
    /// Модернизированная консоль, в которой можно выводить текст на экран с форматированием
    /// При необходимости эта консоль может работать в потокобезопасном режиме, установкой соответ
    /// параметра needLock в true
    /// </summary>
    public static class Mconsole
    {
        //Фиктивное поле, используемое для блокировки вывода на монитор
        private static object _lockObject = "Abc";

        public static void WriteLine(Mstring str, bool needLock = false)
        {
            _wrap(needLock, () =>
            {
                var bColor = Console.BackgroundColor;

                var fColor = Console.ForegroundColor;

                Console.BackgroundColor = str.Bcolor;

                Console.ForegroundColor = str.Fcolor;

                Console.WriteLine(str.Str);

                Console.BackgroundColor = bColor;

                Console.ForegroundColor = fColor;
            });
        }

        public static void WriteLine(Mstring[] strings, bool needLock = false)
        {
            _wrap(needLock, () =>
            {
                strings.ForEach(s => Write(s));

                Console.WriteLine();
            });
        }

        public static void WriteLine(bool needLock = false)
        {
            _wrap(needLock, () =>
            {
                Console.WriteLine();
            });
        }

        public static void Write(Mstring str, bool needLock = false)
        {
            _wrap(needLock, () =>
            {
                var bColor = Console.BackgroundColor;

                var fColor = Console.ForegroundColor;

                Console.BackgroundColor = str.Bcolor;

                Console.ForegroundColor = str.Fcolor;

                Console.Write(str.Str);

                Console.BackgroundColor = bColor;

                Console.ForegroundColor = fColor;
            });
        }
        public static void Write(Mstring[] strings, bool needLock = false)
        {
            _wrap(needLock, () =>
            {
                strings.ForEach(s => Write(s));
            });
        }

        /// <summary>
        /// Потокобезопасное исполнение по мере необходимости
        /// </summary>
        /// <param name="needLock"></param>
        /// <param name="action"></param>
        private static void _wrap(bool needLock, Action action)
        {
            if (needLock)
                lock (_lockObject)
                {
                    Console.Write("Thread id: {0}  ", Thread.CurrentThread.ManagedThreadId);
                    action();
                }
            else
            {
                action();
            }
        }
    }
}
