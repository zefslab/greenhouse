﻿using System;

namespace SmartHouse.Core.Api.Utilities
{
    public static class ColorConsole
    {
        //Фиктивное поле, используемое для блокировки вывода на монитор
        private static object _lockObject = "Abc";
        public static void WriteLine(ConsoleColor color, string format, params object[] args)
        {
            lock (_lockObject)
            {
                var prevColor = Console.ForegroundColor;
                Console.ForegroundColor = color;
                Console.WriteLine(format, args);
                Console.ForegroundColor = prevColor;
            }
        }

        public static void Write(ConsoleColor color, string format, params object[] args)
        {
            lock (_lockObject)
            {
                var prevColor = Console.ForegroundColor;
                Console.ForegroundColor = color;
                Console.Write(format, args);
                Console.ForegroundColor = prevColor;
            }
        }

        public static void WriteLine(ConsoleColor color, ConsoleColor backColor, string format, params object[] args)
        {
            lock (_lockObject)
            {
                var prevColor = Console.ForegroundColor;
                var prevBack = Console.BackgroundColor;
                Console.ForegroundColor = color;
                Console.BackgroundColor = backColor;
                Console.WriteLine(format, args);
                Console.ForegroundColor = prevColor;
                Console.BackgroundColor = prevBack;
            }
        }

        public static void Write(ConsoleColor color, ConsoleColor backColor, string format, params object[] args)
        {
            lock (_lockObject)
            {
                var prevColor = Console.ForegroundColor;
                var prevBack = Console.BackgroundColor;
                Console.ForegroundColor = color;
                Console.BackgroundColor = backColor;
                Console.Write(format, args);
                Console.ForegroundColor = prevColor;
                Console.BackgroundColor = prevBack;
            }
        }
    }
}
