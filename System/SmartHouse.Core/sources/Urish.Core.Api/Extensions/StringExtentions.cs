﻿
using System;

namespace SmartHouse.Core.Api.Utilities.Extensions
{

    public static class StringExtentions
    {
        /// <summary> Используется для сравнения строк взятых из БД и приходящих в виде импортированных данных
        /// без учета регистра и пробелов в конце и начале строки
        /// </summary>
        /// <param name="s"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static bool EqualsFine(this string s, string other)
        {
            if (other == null) return false;

            return s.Trim().ToUpper().Equals(other.Trim().ToUpper());
        }

        public static string AsFormat(this string format, params object[] arg)
        {
            if (format == null)
                throw new NullReferenceException();
            return string.Format(format, arg);
        }
    }
}
