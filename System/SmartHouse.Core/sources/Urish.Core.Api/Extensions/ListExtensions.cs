﻿using System.Collections.Generic;

namespace SmartHouse.Core.Api.Utilities.Extensions
{
    /// <summary>
    /// Методы расширения различных классов
    /// </summary>
    public static class ListExtensions
    {
        

        public static IList<T> EmptyIfNull<T>(this IList<T> list)
        {
            if (list == null) return new List<T>();

            return list;
        }
    }
}
