﻿using System;
using System.Linq;

namespace SmartHouse.Core.Api.Utilities.Extensions
{
    public static class EnumExtensions
    {
        public static T[] GetEnumAttributes<T>(this Enum enumValue)
            where T : Attribute
        {
            var type = enumValue.GetType();
            var attributes = type.GetMember(Enum.GetName(type, enumValue))[0].GetCustomAttributes(typeof(T), false);
            return attributes.Cast<T>().ToArray();
        }

        public static T GetEnumAttribute<T>(this Enum enumValue)
            where T : Attribute
        {
            var attributes = enumValue.GetEnumAttributes<T>();
            return (attributes.Length > 0) ? attributes[0] : null;
        }
    }
}
