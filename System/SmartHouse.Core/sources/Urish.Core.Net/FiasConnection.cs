﻿using Urish.Core.Net.Fias;

namespace Urish.Core.Net
{
    public class FiasConnection : DownloadServiceSoapClient, IFiasConnection
    {
    }

    public interface IFiasConnection : DownloadServiceSoap
    {
    }
}
