﻿using System;
using System.ServiceModel;
using System.Xml;
using Urish.Core.Net.Rosminzdrav.Iemc.Pix;
using Urish.Core.Net.WcfExtra.Gost;

namespace Urish.Core.Net
{
    public class RosminzdravIemcPixConnection : PIXManagerSync_PortTypeClient, IRosminzdravIemcPixConnection
    {
        public RosminzdravIemcPixConnection()
        {
            GostSecurityAlgorithmSuite.SetGostAlgorithmSuiteObsolute(Endpoint);
        }

        public addedResponse added(addedRequest request, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((PIXManagerSync_PortType)this).added(request);
            }
        }

        public IAsyncResult Beginadded(addedRequest request, AsyncCallback callback, object asyncState, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((PIXManagerSync_PortType)this).Beginadded(request, callback, asyncState);
            }
        }

        public addedResponse revised(revisedRequest request, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((PIXManagerSync_PortType)this).revised(request);
            }
        }

        public IAsyncResult Beginrevised(revisedRequest request, AsyncCallback callback, object asyncState, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((PIXManagerSync_PortType)this).Beginrevised(request, callback, asyncState);
            }
        }

        public addedResponse duplicatesResolved(duplicatesResolvedRequest request, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((PIXManagerSync_PortType)this).duplicatesResolved(request);
            }
        }

        public IAsyncResult BeginduplicatesResolved(duplicatesResolvedRequest request, AsyncCallback callback, object asyncState, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((PIXManagerSync_PortType)this).BeginduplicatesResolved(request, callback, asyncState);
            }
        }

        public findCandidatesResponse findCandidates(findCandidatesRequest request, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((PIXManagerSync_PortType)this).findCandidates(request);
            }
        }

        public IAsyncResult BeginfindCandidates(findCandidatesRequest request, AsyncCallback callback, object asyncState, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((PIXManagerSync_PortType)this).BeginfindCandidates(request, callback, asyncState);
            }
        }

        public findCandidatesResponse activateQuery(activateQueryRequest request, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((PIXManagerSync_PortType)this).activateQuery(request);
            }
        }

        public IAsyncResult BeginactivateQuery(activateQueryRequest request, AsyncCallback callback, object asyncState, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((PIXManagerSync_PortType)this).BeginactivateQuery(request, callback, asyncState);
            }
        }
    }

    public interface IRosminzdravIemcPixConnection : PIXManagerSync_PortType
    {
        addedResponse added(addedRequest request, string messageId);
        IAsyncResult Beginadded(addedRequest request, AsyncCallback callback, object asyncState, string messageId);
        addedResponse revised(revisedRequest request, string messageId);
        IAsyncResult Beginrevised(revisedRequest request, AsyncCallback callback, object asyncState, string messageId);
        addedResponse duplicatesResolved(duplicatesResolvedRequest request, string messageId);
        IAsyncResult BeginduplicatesResolved(duplicatesResolvedRequest request, AsyncCallback callback, object asyncState, string messageId);
        findCandidatesResponse findCandidates(findCandidatesRequest request, string messageId);
        IAsyncResult BeginfindCandidates(findCandidatesRequest request, AsyncCallback callback, object asyncState, string messageId);
        findCandidatesResponse activateQuery(activateQueryRequest request, string messageId);
        IAsyncResult BeginactivateQuery(activateQueryRequest request, AsyncCallback callback, object asyncState, string messageId);
    }
}
