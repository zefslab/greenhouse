﻿using System;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Urish.Core.Net.WcfExtra
{
    public class TextMessageEncoderIgnoreSignature : MessageEncoder
    {
        MessageEncoder _innerEncoder;
        private static readonly string _wssName = "Security";
        private static readonly string _wssNameSpace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";

        public TextMessageEncoderIgnoreSignature(MessageEncoder messageEncoder)
        {
            if (messageEncoder == null) throw new ArgumentNullException("messageEncoder");
            _innerEncoder = messageEncoder;
        }

        public override Message ReadMessage(Stream stream, int maxSizeOfHeaders, string contentType)
        {
            var message = _innerEncoder.ReadMessage(stream, maxSizeOfHeaders, contentType);
            message.Properties.Encoder = this;
            RemoveSignatureHeader(message);
            return message;
        }

        public override Message ReadMessage(ArraySegment<byte> buffer, BufferManager bufferManager, string contentType)
        {
            var message = _innerEncoder.ReadMessage(buffer, bufferManager);
            message.Properties.Encoder = this;
            RemoveSignatureHeader(message);
            return message;
        }

        private void RemoveSignatureHeader(Message message)
        {
            var index = message.Headers.FindHeader(_wssName, _wssNameSpace);
            if (index == -1) return;
            message.Headers.RemoveAt(index);
        }

        public override void WriteMessage(Message message, Stream stream)
        {
            _innerEncoder.WriteMessage(message, stream);
            stream.Flush();
        }

        public override ArraySegment<byte> WriteMessage(Message message, int maxMessageSize,
            BufferManager bufferManager, int messageOffset)
        {
            return _innerEncoder.WriteMessage(message, maxMessageSize, bufferManager, messageOffset);
        }

        public override string ContentType
        {
            get
            {
                if (MessageVersion.Envelope == EnvelopeVersion.Soap11)
                    return "text/xml";
                if (MessageVersion.Envelope == EnvelopeVersion.Soap12)
                    return "application/soap+xml;charset=UTF-8";
                return "";
            }
        }

        public override string MediaType
        {
            get { return ContentType; }
        }

        public override MessageVersion MessageVersion
        {
            get { return _innerEncoder.MessageVersion; }
        }
    }
}
