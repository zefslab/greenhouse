﻿using System;
using System.ServiceModel.Channels;
using System.Xml;

namespace Urish.Core.Net.WcfExtra
{
    public sealed class ExtendedMessageEncodingBindingElement
        : MessageEncodingBindingElement //BindingElement
    {
        //We will use an inner binding element to store information required for the inner encoder
        private MessageEncodingBindingElement _innerBindingElement;
        private readonly string _encoderType;

        //By default, use the default text encoder as the inner encoder
        public ExtendedMessageEncodingBindingElement(string encoderType)
            : this(new TextMessageEncodingBindingElement(), encoderType)
        {
        }

        public ExtendedMessageEncodingBindingElement()
            : this(
                new TextMessageEncodingBindingElement(),
                MessageEncodingBindingElementExtension.ConfigPropertyMessageEncoderTypeDefaultValue)
        {
        }

        public ExtendedMessageEncodingBindingElement(
            MessageEncodingBindingElement messageEncoderBindingElement, string encoderType)
        {
            _encoderType = encoderType;
            _innerBindingElement = messageEncoderBindingElement;
        }

        public MessageEncodingBindingElement InnerMessageEncodingBindingElement
        {
            get { return _innerBindingElement; }
            set { _innerBindingElement = value; }
        }

        //Main entry point into the encoder binding element. Called by WCF to get the factory that will create the
        //message encoder
        public override MessageEncoderFactory CreateMessageEncoderFactory()
        {
            return
                new ExtendedMessageEncoderFactory(
                    _innerBindingElement.CreateMessageEncoderFactory(), _encoderType);
        }

        public override MessageVersion MessageVersion
        {
            get { return _innerBindingElement.MessageVersion; }
            set { _innerBindingElement.MessageVersion = value; }
        }


        public override BindingElement Clone()
        {
            return new ExtendedMessageEncodingBindingElement(_innerBindingElement, _encoderType);
        }

        public override T GetProperty<T>(BindingContext context)
        {
            return typeof(T) == typeof(XmlDictionaryReaderQuotas)
                ? _innerBindingElement.GetProperty<T>(context)
                : base.GetProperty<T>(context);
        }

        public override IChannelFactory<TChannel> BuildChannelFactory<TChannel>(
            BindingContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            context.BindingParameters.Add(this);
            return context.BuildInnerChannelFactory<TChannel>();
        }

        public override IChannelListener<TChannel> BuildChannelListener<TChannel>(
            BindingContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            context.BindingParameters.Add(this);
            return context.BuildInnerChannelListener<TChannel>();
        }

        public override bool CanBuildChannelListener<TChannel>(BindingContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            context.BindingParameters.Add(this);
            return context.CanBuildInnerChannelListener<TChannel>();
        }
    }

    internal class ExtendedMessageEncoderFactory : MessageEncoderFactory
    {
        private readonly MessageEncoder _encoder;

        //The GZip encoder wraps an inner encoder
        //We require a factory to be passed in that will create this inner encoder
        public ExtendedMessageEncoderFactory(MessageEncoderFactory messageEncoderFactory,
            string encoderType)
        {
            if (messageEncoderFactory == null)
                throw new ArgumentNullException("messageEncoderFactory",
                    "A valid message encoder factory must be passed to the GZipEncoder");
            var messageEncoderType = Type.GetType(encoderType);
            if (messageEncoderType != null)
                _encoder =
                    (MessageEncoder)
                        Activator.CreateInstance(messageEncoderType, messageEncoderFactory.Encoder);
        }

        //The service framework uses this property to obtain an encoder from this encoder factory
        public override MessageEncoder Encoder
        {
            get { return _encoder; }
        }

        public override MessageVersion MessageVersion
        {
            get { return _encoder.MessageVersion; }
        }
    }
}
