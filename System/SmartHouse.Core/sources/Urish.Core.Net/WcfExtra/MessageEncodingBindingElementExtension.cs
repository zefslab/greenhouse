﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;

namespace Urish.Core.Net.WcfExtra
{
    public class MessageEncodingBindingElementExtension : BindingElementExtensionElement
    {
        private const string ConfigPropertyInnerMessageEncoding = "innerMessageEncoding";
        private const string ConfigPropertyMessageEncoderType = "messageEncoderType";
        private const string ConfigPropertyInnerMessageEncodingDefaultValue = "textMessageEncoding";
        public const string ConfigPropertyMessageEncoderTypeDefaultValue = "Rosminzdrav.Er.Q2.Client.WCF.GZipMessageEncoder, Rosminzdrav.Er.Q2";
        private const string ConfigPropertyMaxArrayLength = "MaxArrayLength";
        private const string CategoryReaderQuotas = "ReaderQuotas";
        private const string ConfigPropertyReaderQuotas = "readerQuotas";
        private const string BinaryMessageEncoding = "binaryMessageEncoding";
        private const string MtomMessageEncoding = "mtomMessageEncoding";
        private const string ConfigPropertyMaxDepth = "maxDepth";
        private const string ConfigPropertyMaxStringContentLength = "maxStringContentLength";
        private const string ConfigPropertyMaxBytesPerRead = "maxBytesPerRead";
        private const string ConfigPropertyMaxNameTableCharCount = "maxNameTableCharCount";
        private const string ConfigPropertyMessageVersion = "messageVersion";

        private XmlDictionaryReaderQuotasElement XmlDictionaryReaderQuotasElement
        {
            get { return ((XmlDictionaryReaderQuotasElement)base[ConfigPropertyReaderQuotas]); }
        }

        [ConfigurationProperty(ConfigPropertyMessageVersion, IsRequired = true)]
        public string MessageVersion
        {
            get { return (string)base[ConfigPropertyMessageVersion]; }
            set { base[ConfigPropertyMessageVersion] = value; }
        }

        //The only property we need to configure for our binding element is the type of
        //inner encoder to use. Here, we support text and binary.
        [ConfigurationProperty(ConfigPropertyInnerMessageEncoding, DefaultValue = ConfigPropertyInnerMessageEncodingDefaultValue)]
        public string InnerMessageEncoding
        {
            get { return (string)base[ConfigPropertyInnerMessageEncoding]; }
            set { base[ConfigPropertyInnerMessageEncoding] = value; }
        }

        [ConfigurationProperty(ConfigPropertyMessageEncoderType, DefaultValue = ConfigPropertyMessageEncoderTypeDefaultValue)]
        public string MessageEncoderType
        {
            get
            {
                return (string)base[ConfigPropertyMessageEncoderType];
            }
            set
            {
                base[ConfigPropertyMessageEncoderType] = value;
            }
        }

        /// <summary>
        /// Gets or sets the length of the max array.
        /// </summary>
        /// <value>The length of the max array.</value>
        [ConfigurationProperty(ConfigPropertyMaxArrayLength, DefaultValue = 999999999, IsRequired = false)]
        [Category(CategoryReaderQuotas)]
        [Description(ConfigPropertyMaxArrayLength)]
        public int MaxArrayLength
        {
            get { return XmlDictionaryReaderQuotasElement.MaxArrayLength; }
            set { XmlDictionaryReaderQuotasElement.MaxArrayLength = value; }
        }


        [ConfigurationProperty(ConfigPropertyMaxDepth, DefaultValue = 999999999, IsRequired = false)]
        [Category(CategoryReaderQuotas)]
        [Description(ConfigPropertyMaxDepth)]
        public int MaxDepth
        {
            get
            {
                return XmlDictionaryReaderQuotasElement.MaxDepth;
            }
            set
            {
                XmlDictionaryReaderQuotasElement.MaxDepth = value;
            }
        }


        [ConfigurationProperty(ConfigPropertyMaxStringContentLength, DefaultValue = 999999999, IsRequired = false)]
        [Category(CategoryReaderQuotas)]
        [Description(ConfigPropertyMaxStringContentLength)]
        public int MaxStringContentLength
        {
            get
            {
                return XmlDictionaryReaderQuotasElement.MaxStringContentLength;
            }
            set
            {
                XmlDictionaryReaderQuotasElement.MaxStringContentLength = value;
            }
        }


        [ConfigurationProperty(ConfigPropertyMaxBytesPerRead, DefaultValue = 999999999, IsRequired = false)]
        [Category(CategoryReaderQuotas)]
        [Description(ConfigPropertyMaxBytesPerRead)]
        public int MaxBytesPerRead
        {
            get
            {
                return XmlDictionaryReaderQuotasElement.MaxBytesPerRead;
            }
            set
            {
                XmlDictionaryReaderQuotasElement.MaxBytesPerRead = value;
            }
        }


        [ConfigurationProperty(ConfigPropertyMaxNameTableCharCount, DefaultValue = 999999999, IsRequired = false)]
        [Category(CategoryReaderQuotas)]
        [Description(ConfigPropertyMaxNameTableCharCount)]
        public int MaxNameTableCharCount
        {
            get
            {
                return XmlDictionaryReaderQuotasElement.MaxNameTableCharCount;
            }
            set
            {
                XmlDictionaryReaderQuotasElement.MaxNameTableCharCount = value;
            }
        }

        /// <summary>
        /// Gets the reader quotas.
        /// </summary>
        /// <value>The reader quotas.</value>
        [ConfigurationProperty(ConfigPropertyReaderQuotas)]
        public XmlDictionaryReaderQuotasElement ReaderQuotas
        {
            get { return (XmlDictionaryReaderQuotasElement)base[ConfigPropertyReaderQuotas]; }
        }

        //Called by the WCF to discover the type of binding element this config section enables
        public override Type BindingElementType
        {
            get { return typeof(ExtendedMessageEncodingBindingElement); }
        }

        //Called by the WCF to apply the configuration settings (the property above) to the binding element
        public override void ApplyConfiguration(BindingElement bindingElement)
        {
            var binding = (ExtendedMessageEncodingBindingElement)bindingElement;

            PropertyInformationCollection propertyInfo = ElementInformation.Properties;
            if (propertyInfo[ConfigPropertyInnerMessageEncoding].ValueOrigin != PropertyValueOrigin.Default)
            {
                switch (InnerMessageEncoding)
                {
                    case ConfigPropertyInnerMessageEncodingDefaultValue:
                        binding.InnerMessageEncodingBindingElement = new TextMessageEncodingBindingElement();

                        var textMessageEncodingBindingElement =
                            ((TextMessageEncodingBindingElement)
                                binding.InnerMessageEncodingBindingElement);
                        if (ReaderQuotas.MaxArrayLength > 0)
                        {
                            textMessageEncodingBindingElement.
                                ReaderQuotas.
                                MaxArrayLength = ReaderQuotas.MaxArrayLength;
                        }
                        if (ReaderQuotas.MaxBytesPerRead > 0)
                        {
                            textMessageEncodingBindingElement.
                                ReaderQuotas.
                                MaxBytesPerRead = ReaderQuotas.MaxBytesPerRead;
                        }
                        if (ReaderQuotas.MaxDepth > 0)
                        {
                            textMessageEncodingBindingElement.
                                ReaderQuotas.
                                MaxDepth = ReaderQuotas.MaxDepth;
                        }
                        if (ReaderQuotas.MaxNameTableCharCount > 0)
                        {
                            textMessageEncodingBindingElement.
                                ReaderQuotas.
                                MaxNameTableCharCount = ReaderQuotas.MaxNameTableCharCount;
                        }
                        if (ReaderQuotas.MaxStringContentLength > 0)
                        {
                            textMessageEncodingBindingElement.
                                ReaderQuotas.
                                MaxStringContentLength = ReaderQuotas.MaxStringContentLength;
                        }
                        break;
                    case BinaryMessageEncoding:
                        binding.InnerMessageEncodingBindingElement = new BinaryMessageEncodingBindingElement();
                        var binaryMessageEncodingBindingElement =
                            ((BinaryMessageEncodingBindingElement)
                                binding.InnerMessageEncodingBindingElement);
                        if (ReaderQuotas.MaxArrayLength > 0)
                        {
                            binaryMessageEncodingBindingElement.
                                ReaderQuotas.
                                MaxArrayLength = ReaderQuotas.MaxArrayLength;
                        }
                        if (ReaderQuotas.MaxBytesPerRead > 0)
                        {
                            binaryMessageEncodingBindingElement.
                                ReaderQuotas.
                                MaxBytesPerRead = ReaderQuotas.MaxBytesPerRead;
                        }
                        if (ReaderQuotas.MaxDepth > 0)
                        {
                            binaryMessageEncodingBindingElement.
                                ReaderQuotas.
                                MaxDepth = ReaderQuotas.MaxDepth;
                        }
                        if (ReaderQuotas.MaxNameTableCharCount > 0)
                        {
                            binaryMessageEncodingBindingElement.
                                ReaderQuotas.
                                MaxNameTableCharCount = ReaderQuotas.MaxNameTableCharCount;
                        }
                        if (ReaderQuotas.MaxStringContentLength > 0)
                        {
                            binaryMessageEncodingBindingElement.
                                ReaderQuotas.
                                MaxStringContentLength = ReaderQuotas.MaxStringContentLength;
                        }
                        break;
                    case MtomMessageEncoding:
                        binding.InnerMessageEncodingBindingElement = new MtomMessageEncodingBindingElement();
                        var mtomMessageEncodingBindingElement =
                            ((MtomMessageEncodingBindingElement)
                                binding.InnerMessageEncodingBindingElement);
                        if (ReaderQuotas.MaxArrayLength > 0)
                        {
                            mtomMessageEncodingBindingElement.
                                ReaderQuotas.
                                MaxArrayLength = ReaderQuotas.MaxArrayLength;
                        }
                        if (ReaderQuotas.MaxBytesPerRead > 0)
                        {
                            mtomMessageEncodingBindingElement.
                                ReaderQuotas.
                                MaxBytesPerRead = ReaderQuotas.MaxBytesPerRead;
                        }
                        if (ReaderQuotas.MaxDepth > 0)
                        {
                            mtomMessageEncodingBindingElement.
                                ReaderQuotas.
                                MaxDepth = ReaderQuotas.MaxDepth;
                        }
                        if (ReaderQuotas.MaxNameTableCharCount > 0)
                        {
                            mtomMessageEncodingBindingElement.
                                ReaderQuotas.
                                MaxNameTableCharCount = ReaderQuotas.MaxNameTableCharCount;
                        }
                        if (ReaderQuotas.MaxStringContentLength > 0)
                        {
                            mtomMessageEncodingBindingElement.
                                ReaderQuotas.
                                MaxStringContentLength = ReaderQuotas.MaxStringContentLength;
                        }
                        break;
                }
            }
        }

        //Called by the WCF to create the binding element
        protected override BindingElement CreateBindingElement()
        {
            var bindingElement = new ExtendedMessageEncodingBindingElement(MessageEncoderType);
            ApplyConfiguration(bindingElement);
            return bindingElement;
        }
    }

}
