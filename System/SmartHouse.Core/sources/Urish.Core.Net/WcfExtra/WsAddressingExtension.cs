﻿using System;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Urish.Core.Net.WcfExtra
{
    public class WsAddressingInspector : IClientMessageInspector
    {
        private readonly string _replyTo;

        public WsAddressingInspector(string replyTo)
        {
            _replyTo = replyTo;
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            request.Headers.ReplyTo = new EndpointAddress(_replyTo);
            return null;
        }

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
        }
    }

    public class WsAddressingEndpointBehavior : IEndpointBehavior
    {
        private readonly string _replyTo;

        public WsAddressingEndpointBehavior(string replyTo)
        {
            _replyTo = replyTo;
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            var inspector = new WsAddressingInspector(_replyTo);
            clientRuntime.MessageInspectors.Add(inspector);
        }
    }

    public class WsAddressingBehaviorExtensionElement : BehaviorExtensionElement
    {
        [ConfigurationProperty("replyTo", IsRequired = true)]
        public string ReplyTo
        {
            get { return (string)base["replyTo"]; }
            set { base["replyTo"] = value; }
        }

        protected override object CreateBehavior()
        {
            return new WsAddressingEndpointBehavior(ReplyTo);
        }

        public override Type BehaviorType
        {
            get { return typeof(WsAddressingEndpointBehavior); }
        }
    }
}
