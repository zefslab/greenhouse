﻿using System.ServiceModel.Configuration;

namespace Urish.Core.Net.WcfExtra.Gost
{
    public class GostClientCredentialsElement : ClientCredentialsElement
    {
        protected override object CreateBehavior()
        {
            var credentials = new GostClientCredentials();
            ApplyConfiguration(credentials);
            return credentials;
        }
    }
}
