﻿using System;
using System.Collections.ObjectModel;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using GostCryptography.Cryptography;

namespace Urish.Core.Net.WcfExtra.Gost
{
    public class GostSecurityTokenProvider : X509SecurityTokenProvider
    {
        public GostSecurityTokenProvider(X509Certificate2 certificate) : base(certificate)
        {
        }

        protected override SecurityToken GetTokenCore(TimeSpan timeout)
        {
            return new GostX509SecurityToken(Certificate);
        }
    }

    public class GostX509SecurityToken : X509SecurityToken
    {
        public GostX509SecurityToken(X509Certificate2 certificate) : base(certificate)
        {
        }

        public GostX509SecurityToken(X509Certificate2 certificate, string id) : base(certificate, id)
        {
        }

        public override ReadOnlyCollection<SecurityKey> SecurityKeys
        {
            get { return Array.AsReadOnly(new SecurityKey[1] { new GostX509AsymmetricSecurityKey(Certificate) }); }
        }
    }

    public class GostX509AsymmetricSecurityKey : X509AsymmetricSecurityKey
    {
        private readonly X509Certificate2 _certificate;

        public GostX509AsymmetricSecurityKey(X509Certificate2 certificate) : base(certificate)
        {
            _certificate = certificate;
        }

        public override AsymmetricAlgorithm GetAsymmetricAlgorithm(string algorithm, bool privateKey)
        {
            switch (algorithm)
            {
                case GostSecurityAlgorithmSuite.Gostr34102001Gostr3411:
                case GostSecurityAlgorithmSuite.TransportGost2001:
                    return new Gost3410AsymmetricAlgorithm(_certificate.GetPrivateKeyInfo());
                default:
                    throw new InvalidOperationException("Unsupported Encryption Algorithm");
            }
        }

        public override byte[] DecryptKey(string algorithm, byte[] keyData)
        {
            return base.DecryptKey(algorithm, keyData);
        }

        public override byte[] EncryptKey(string algorithm, byte[] keyData)
        {
            return base.EncryptKey(algorithm, keyData);
        }

        public override bool IsSupportedAlgorithm(string algorithm)
        {
            var algorithmFromConfig = GostCryptoConfig.CreateFromName(algorithm);


            switch (algorithm)
            {
                //case GostSecurityAlgorithmSuite.HmacGostr3411:
                case GostSecurityAlgorithmSuite.Gostr34102001Gostr3411:
                case GostSecurityAlgorithmSuite.Gostr34102001Gostr3411Obsolute:
                case GostSecurityAlgorithmSuite.KwGost:
                case GostSecurityAlgorithmSuite.Gost28147:
                    return true;
                default :
                    return false;
            }
        }

        public override HashAlgorithm GetHashAlgorithmForSignature(string algorithm)
        {
            switch (algorithm)
            {
                case GostSecurityAlgorithmSuite.HmacGostr3411:
                    return new Gost3411Hmac(new Gost28147SymmetricAlgorithm());
                case GostSecurityAlgorithmSuite.Gostr34102001Gostr3411:
                case GostSecurityAlgorithmSuite.Gostr34102001Gostr3411Obsolute:
                    return new Gost3411HashAlgorithm();
            }
            throw new InvalidOperationException(string.Format("Unsupported Hash Algorithm \"{0}\"", algorithm));
        }

        public override bool HasPrivateKey()
        {
            return base.HasPrivateKey();
        }

        public override int KeySize
        {
            get { return base.KeySize; }
        }

        public override bool IsAsymmetricAlgorithm(string algorithm)
        {
            return base.IsAsymmetricAlgorithm(algorithm);
        }

        public override bool IsSymmetricAlgorithm(string algorithm)
        {
            return base.IsSymmetricAlgorithm(algorithm);
        }

        public override AsymmetricSignatureDeformatter GetSignatureDeformatter(string algorithm)
        {
            return new GostSignatureDeformatter(_certificate.GetPublicKeyAlgorithm());
        }

        public override AsymmetricSignatureFormatter GetSignatureFormatter(string algorithm)
        {
            return new GostSignatureFormatter(_certificate.GetPrivateKeyAlgorithm());
        }
    }
}
