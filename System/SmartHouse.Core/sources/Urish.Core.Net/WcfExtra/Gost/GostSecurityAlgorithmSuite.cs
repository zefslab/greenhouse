﻿using System;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Security;

namespace Urish.Core.Net.WcfExtra.Gost
{
    public class GostSecurityAlgorithmSuite : SecurityAlgorithmSuite
    {
        private readonly bool _obsolute;
        private const string Namespace = "urn:ietf:params:xml:ns:cpxmlsec:algorithms:";
        private const string OldNamespace = "http://www.w3.org/2001/04/xmldsig-more#";
        public const string Gost28147 = Namespace + "gost28147";
        public const string Gostr3411 = Namespace + "gostr3411";
        public const string KwGost = Namespace + "kw-gost";
        public const string TransportGost2001 = Namespace + "transport-gost2001";
        public const string HmacGostr3411 = Namespace + "hmac-gostr3411";
        public const string Gostr34102001Gostr3411 = Namespace + "gostr34102001-gostr3411";
        public const string Gostr3411Obsolute = OldNamespace + "gostr3411";
        public const string Gostr34102001Gostr3411Obsolute = OldNamespace + "gostr34102001-gostr3411";

        public static GostSecurityAlgorithmSuite BasicGost = new GostSecurityAlgorithmSuite(false);
        public static GostSecurityAlgorithmSuite BasicGostObsolute = new GostSecurityAlgorithmSuite(true);


        private GostSecurityAlgorithmSuite(bool obsolute)
        {
            _obsolute = obsolute;
        }

        public override bool IsSymmetricKeyLengthSupported(int length)
        {
            return length == 256;
        }

        public override bool IsAsymmetricKeyLengthSupported(int length)
        {
            return length == 512;
        }

        public override string DefaultCanonicalizationAlgorithm
        {
            get { return Default.DefaultCanonicalizationAlgorithm; }
        }

        public override string DefaultDigestAlgorithm
        {
            get
            {
                return _obsolute ? Gostr3411Obsolute : Gostr3411;
            }
        }

        public override string DefaultEncryptionAlgorithm
        {
            get { return Gost28147; }
        }

        public override int DefaultEncryptionKeyDerivationLength
        {
            get { return 256; }
        }

        public override string DefaultSymmetricKeyWrapAlgorithm
        {
            get { return KwGost; }
        }

        public override string DefaultAsymmetricKeyWrapAlgorithm
        {
            get { return TransportGost2001; }
        }

        public override string DefaultSymmetricSignatureAlgorithm
        {
            get { return HmacGostr3411; }
        }

        public override string DefaultAsymmetricSignatureAlgorithm
        {
            get { return _obsolute ? Gostr34102001Gostr3411Obsolute : Gostr34102001Gostr3411; }
        }

        public override int DefaultSignatureKeyDerivationLength
        {
            get { return 256; }
        }

        public override int DefaultSymmetricKeyLength
        {
            get { return 256; }
        }

        public static void SetGostAlgorithmSuite(ServiceEndpoint endpoint)
        {
            if (endpoint == null) throw new ArgumentNullException("endpoint");

            var customBinding = endpoint.Binding as CustomBinding;
            if (customBinding != null)
            {
                var securityBindingElement = customBinding.Elements.Find<SecurityBindingElement>();
                if (securityBindingElement != null)
                {
                    securityBindingElement.DefaultAlgorithmSuite = BasicGost;
                }
            }
        }

        public static void SetGostAlgorithmSuiteObsolute(ServiceEndpoint endpoint)
        {
            if (endpoint == null) throw new ArgumentNullException("endpoint");

            var customBinding = endpoint.Binding as CustomBinding;
            if (customBinding != null)
            {
                var securityBindingElement = customBinding.Elements.Find<SecurityBindingElement>();
                if (securityBindingElement != null)
                {
                    securityBindingElement.DefaultAlgorithmSuite = BasicGostObsolute;
                }
            }
        }
    }
}
