﻿using System.IdentityModel.Selectors;
using System.ServiceModel.Description;

namespace Urish.Core.Net.WcfExtra.Gost
{
    public class GostClientCredentials : ClientCredentials
    {
        public GostClientCredentials()
        {
        }
        public GostClientCredentials(GostClientCredentials other):base(other)
        {}
        protected override ClientCredentials CloneCore()
        {
            return new GostClientCredentials(this);
        }

        public override SecurityTokenManager CreateSecurityTokenManager()
        {
            return new GostClientCredentialsSecurityTokenManager(Clone() as GostClientCredentials);
        }
    }
}
