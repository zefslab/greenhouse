﻿using System.IdentityModel.Selectors;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;

namespace Urish.Core.Net.WcfExtra.Gost
{
    public class GostClientCredentialsSecurityTokenManager : ClientCredentialsSecurityTokenManager
    {
        public GostClientCredentialsSecurityTokenManager(GostClientCredentials clientCredentials) : base(clientCredentials)
        {
        }

        public override SecurityTokenProvider CreateSecurityTokenProvider(SecurityTokenRequirement tokenRequirement)
        {
            var provider = base.CreateSecurityTokenProvider(tokenRequirement);
            if (!(provider is X509SecurityTokenProvider)) return provider;
            var certificate = (provider as X509SecurityTokenProvider).Certificate;
            return certificate.SignatureAlgorithm.Value == "1.2.643.2.2.3"
                ? new GostSecurityTokenProvider(certificate)
                : provider;
        }
    }

    public class GostX509Certificate : X509Certificate2
    {
    }
}