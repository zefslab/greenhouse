﻿using System.ServiceModel;
using System.Threading.Tasks;
using System.Xml;
using Urish.Core.Net.Rosminzdrav.Iemc.Xds.Query;
using Urish.Core.Net.WcfExtra.Gost;

namespace Urish.Core.Net
{
    public class RosminzdravIemcXdsQueryConnection : DocumentRegistry_PortTypeClient, IRosminzdravIemcXdsQueryConnection
    {
        public RosminzdravIemcXdsQueryConnection()
        {
            GostSecurityAlgorithmSuite.SetGostAlgorithmSuiteObsolute(Endpoint);
        }

        public DocumentRegistry_RegistryStoredQueryResponse DocumentRegistry_RegistryStoredQuery(
            DocumentRegistry_RegistryStoredQueryRequest request, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((DocumentRegistry_PortType)this).DocumentRegistry_RegistryStoredQuery(request);
            }
        }

        public Task<DocumentRegistry_RegistryStoredQueryResponse> DocumentRegistry_RegistryStoredQueryAsync(DocumentRegistry_RegistryStoredQueryRequest request, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((DocumentRegistry_PortType)this).DocumentRegistry_RegistryStoredQueryAsync(request);
            }
        }
    }

    public interface IRosminzdravIemcXdsQueryConnection : DocumentRegistry_PortType
    {
        DocumentRegistry_RegistryStoredQueryResponse DocumentRegistry_RegistryStoredQuery(
            DocumentRegistry_RegistryStoredQueryRequest request, string messageId);

        Task<DocumentRegistry_RegistryStoredQueryResponse> DocumentRegistry_RegistryStoredQueryAsync(
            DocumentRegistry_RegistryStoredQueryRequest request, string messageId);
    }
}
