﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Urish.Core.Net.Rosminzdrav.Nsi;

namespace Urish.Core.Net
{
    public class RosminzdravNsiConnection : SOAPServPortTypeClient, IRosminzdravNsiConnection
    {
        public class Book
        {
            public string Code { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string Oid { get; set; }

            public IList<Error> Errors { get; set; }
            public IList<Version> Versions { get; set; }
            public IList<Record> Records { get; set; }
            public Book()
            {
                Errors = new List<Error>();
                Versions = new List<Version>();
                Records = new List<Record>();
            }
        }

        public class BookList
        {
            public IList<Error> Errors { get; set; }
            public IList<Book> Books { get; set; }

            public BookList()
            {
                Errors = new List<Error>();
                Books = new List<Book>();
            }
        }

        public class Version
        {
            public string Number;
            public DateTime Date;
        }

        public class Record : Dictionary<string, string>
        {
        }

        public class Error
        {
            public string Code { get; set; }
            public string Message { get; set; }
        }
    }

    public interface IRosminzdravNsiConnection : SOAPServPortType
    {
    }

    public static class RosminzdravNsiConnectionExtensions
    {
        public static RosminzdravNsiConnection.Book AsBook(this Map[] maps)
        {
            var book = new RosminzdravNsiConnection.Book();
            var errors = maps.Where(map => map.key == "errors").AsQueryable();
            foreach (var map in errors.SelectMany(map => map.children))
            {
                book.Errors.Add(new RosminzdravNsiConnection.Error
                {
                    Code = map.key,
                    Message = map.value
                });
            }

            var versions = maps.Where(map => map.children.Any(o => o.key == "S_VERSION")).AsQueryable();
            foreach (var map in versions)
            {
                book.Versions.Add(new RosminzdravNsiConnection.Version
                {
                    Date = Convert.ToDateTime(map.children.First(o => o.key == "V_DATE").value),
                    Number = map.children.First(o => o.key == "S_VERSION").value
                });
            }

            foreach (var map in maps.Except(errors).Except(versions))
            {
                var record = new RosminzdravNsiConnection.Record();
                Array.ForEach(map.children.ToArray(),
                    map1 => record.Add(map1.key, map1.value));
                book.Records.Add(record);
            }

            return book;
        }

        public static RosminzdravNsiConnection.BookList AsBookList(this Map[] maps)
        {
            var bookList = new RosminzdravNsiConnection.BookList();
            var errors = maps.Where(map => map.key == "errors").AsQueryable();
            foreach (var map in errors.SelectMany(map => map.children))
            {
                bookList.Errors.Add(new RosminzdravNsiConnection.Error
                {
                    Code = map.key,
                    Message = map.value
                });
            }

            foreach (var map in maps.Except(errors))
            {
                bookList.Books.Add(new RosminzdravNsiConnection.Book
                {
                    Code = map.children.First(o => o.key == "S_CODE").value,
                    Name = map.children.First(o => o.key == "S_NAME").value,
                    Description = map.children.First(o => o.key == "S_DESCRIPTION").value,
                    Oid = map.children.First(o => o.key == "OID").value
                });
            }

            return bookList;
        }

        public static RosminzdravNsiConnection.Book ThrowIfError(this RosminzdravNsiConnection.Book book)
        {
            ThrowIfError(book.Errors);
            return book;
        }

        public static RosminzdravNsiConnection.BookList ThrowIfError(this RosminzdravNsiConnection.BookList bookList)
        {
            ThrowIfError(bookList.Errors);
            return bookList;
        }

        private static void ThrowIfError(ICollection<RosminzdravNsiConnection.Error> errors)
        {
            if(errors.Count == 0) return;
            var message = errors.Aggregate(new StringBuilder("Ошибка в работе сервиса НСИ"),
                (builder, error) => builder.AppendFormat("\n{0}: {1}", error.Code, error.Message),
                builder => builder.ToString());
            throw new Exception(message);
        }
    }
}
