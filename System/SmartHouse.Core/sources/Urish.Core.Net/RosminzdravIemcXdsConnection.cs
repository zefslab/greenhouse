﻿using System;
using System.ServiceModel;
using System.Xml;
using Urish.Core.Net.Rosminzdrav.Iemc.Xds;
using Urish.Core.Net.WcfExtra.Gost;

namespace Urish.Core.Net
{
    public class RosminzdravIemcXdsConnection : DocumentRepository_PortTypeClient, IRosminzdravIemcXdsConnection
    {
        public RosminzdravIemcXdsConnection()
        {
            GostSecurityAlgorithmSuite.SetGostAlgorithmSuiteObsolute(Endpoint);
        }

        public DocumentRepository_RetrieveDocumentSetResponse DocumentRepository_RetrieveDocumentSet(
            DocumentRepository_RetrieveDocumentSetRequest request, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((DocumentRepository_PortType)this).DocumentRepository_RetrieveDocumentSet(request);
            }

        }

        public IAsyncResult BeginDocumentRepository_RetrieveDocumentSet(DocumentRepository_RetrieveDocumentSetRequest request,
            AsyncCallback callback, object asyncState, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((DocumentRepository_PortType)this).BeginDocumentRepository_RetrieveDocumentSet(request, callback, asyncState);
            }
        }

        public DocumentRepository_RetrieveDocumentSetResponse DocumentRepository_ProvideAndRegisterDocumentSetb(
            DocumentRepository_ProvideAndRegisterDocumentSetbRequest request, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((DocumentRepository_PortType)this).DocumentRepository_ProvideAndRegisterDocumentSetb(request);
            }
        }

        public IAsyncResult BeginDocumentRepository_ProvideAndRegisterDocumentSetb(
            DocumentRepository_ProvideAndRegisterDocumentSetbRequest request, AsyncCallback callback, object asyncState, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((DocumentRepository_PortType)this).BeginDocumentRepository_ProvideAndRegisterDocumentSetb(request, callback, asyncState);
            }
        }
    }

    public interface IRosminzdravIemcXdsConnection : DocumentRepository_PortType
    {
        DocumentRepository_RetrieveDocumentSetResponse DocumentRepository_RetrieveDocumentSet(
            DocumentRepository_RetrieveDocumentSetRequest request, string messageId);

        IAsyncResult BeginDocumentRepository_RetrieveDocumentSet(DocumentRepository_RetrieveDocumentSetRequest request,
            AsyncCallback callback, object asyncState, string messageId);

        DocumentRepository_RetrieveDocumentSetResponse DocumentRepository_ProvideAndRegisterDocumentSetb(
            DocumentRepository_ProvideAndRegisterDocumentSetbRequest request, string messageId);

        IAsyncResult BeginDocumentRepository_ProvideAndRegisterDocumentSetb(
            DocumentRepository_ProvideAndRegisterDocumentSetbRequest request, AsyncCallback callback, object asyncState, string messageId);
    }
}
