﻿using System;
using System.ServiceModel;
using System.Xml;
using Urish.Core.Net.Rosminzdrav.Iemc.v25;
using Urish.Core.Net.WcfExtra.Gost;

namespace Urish.Core.Net
{
    public class RosminzdravIemcV25Connection : V25Process_AS_PortTypeClient, IRosminzdravIemcV25Connection
    {
        public RosminzdravIemcV25Connection()
        {
            GostSecurityAlgorithmSuite.SetGostAlgorithmSuiteObsolute(Endpoint);
        }

        public V25Process_AS_ProcessResponse V25Process_AS_Process(V25Process_AS_ProcessRequest request, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((V25Process_AS_PortType)this).V25Process_AS_Process(request);
            }
        }

        public IAsyncResult BeginV25Process_AS_Process(V25Process_AS_ProcessRequest request, AsyncCallback callback, object asyncState, string messageId)
        {
            using (new OperationContextScope((IContextChannel)Channel))
            {
                OperationContext.Current.OutgoingMessageHeaders.MessageId = new UniqueId(messageId);
                return ((V25Process_AS_PortType)this).BeginV25Process_AS_Process(request, callback, asyncState);
            }
        }
    }

    public interface IRosminzdravIemcV25Connection : V25Process_AS_PortType
    {
        V25Process_AS_ProcessResponse V25Process_AS_Process(V25Process_AS_ProcessRequest request, string messageId);
        IAsyncResult BeginV25Process_AS_Process(V25Process_AS_ProcessRequest request, AsyncCallback callback, object asyncState, string messageId);
    }
}
