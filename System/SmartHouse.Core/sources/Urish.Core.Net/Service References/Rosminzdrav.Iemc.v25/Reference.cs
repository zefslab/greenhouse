﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.0
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Urish.Core.Net.Rosminzdrav.Iemc.v25 {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace = "http://v25.iemk.atc.ru", ConfigurationName = "Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_PortType")]
    public interface V25Process_AS_PortType {
        
        // CODEGEN: Generating message contract since the operation V25Process_AS_Process is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="http://v25.iemk.atc.ru/V25ProcessAsync", ReplyAction="http://v25.iemk.atc.ru/V25ResponseAsync")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_ProcessResponse V25Process_AS_Process(global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_ProcessRequest request);
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://v25.iemk.atc.ru/V25ProcessAsync", ReplyAction="http://v25.iemk.atc.ru/V25ResponseAsync")]
        System.IAsyncResult BeginV25Process_AS_Process(global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_ProcessRequest request, System.AsyncCallback callback, object asyncState);
        
        global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_ProcessResponse EndV25Process_AS_Process(System.IAsyncResult result);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://egisz.rosminzdrav.ru")]
    public partial class transportHeader : object, System.ComponentModel.INotifyPropertyChanged {
        
        private authInfo authInfoField;
        
        private userInfo userInfoField;
        
        private string idField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public authInfo authInfo {
            get {
                return this.authInfoField;
            }
            set {
                this.authInfoField = value;
                this.RaisePropertyChanged("authInfo");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public userInfo userInfo {
            get {
                return this.userInfoField;
            }
            set {
                this.userInfoField = value;
                this.RaisePropertyChanged("userInfo");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form=System.Xml.Schema.XmlSchemaForm.Qualified, Namespace="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xs" +
            "d", DataType="ID")]
        public string Id {
            get {
                return this.idField;
            }
            set {
                this.idField = value;
                this.RaisePropertyChanged("Id");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://egisz.rosminzdrav.ru")]
    public partial class authInfo : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string userSessionField;
        
        private string clientEntityIdField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string userSession {
            get {
                return this.userSessionField;
            }
            set {
                this.userSessionField = value;
                this.RaisePropertyChanged("userSession");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string clientEntityId {
            get {
                return this.clientEntityIdField;
            }
            set {
                this.clientEntityIdField = value;
                this.RaisePropertyChanged("clientEntityId");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://egisz.rosminzdrav.ru")]
    public partial class education : object, System.ComponentModel.INotifyPropertyChanged {
        
        private property typeField;
        
        private property specialityField;
        
        private property diplomTypeField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public property type {
            get {
                return this.typeField;
            }
            set {
                this.typeField = value;
                this.RaisePropertyChanged("type");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public property speciality {
            get {
                return this.specialityField;
            }
            set {
                this.specialityField = value;
                this.RaisePropertyChanged("speciality");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public property diplomType {
            get {
                return this.diplomTypeField;
            }
            set {
                this.diplomTypeField = value;
                this.RaisePropertyChanged("diplomType");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://egisz.rosminzdrav.ru")]
    public partial class property : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string codeField;
        
        private string nameField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string code {
            get {
                return this.codeField;
            }
            set {
                this.codeField = value;
                this.RaisePropertyChanged("code");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
                this.RaisePropertyChanged("name");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://egisz.rosminzdrav.ru")]
    public partial class organization : object, System.ComponentModel.INotifyPropertyChanged {
        
        private property idField;
        
        private property typeField;
        
        private property positionTypeField;
        
        private property positionField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public property id {
            get {
                return this.idField;
            }
            set {
                this.idField = value;
                this.RaisePropertyChanged("id");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public property type {
            get {
                return this.typeField;
            }
            set {
                this.typeField = value;
                this.RaisePropertyChanged("type");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public property positionType {
            get {
                return this.positionTypeField;
            }
            set {
                this.positionTypeField = value;
                this.RaisePropertyChanged("positionType");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public property position {
            get {
                return this.positionField;
            }
            set {
                this.positionField = value;
                this.RaisePropertyChanged("position");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://egisz.rosminzdrav.ru")]
    public partial class userInfo : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string userIdField;
        
        private string lastNameField;
        
        private string firstNameField;
        
        private string middleNameField;
        
        private property roleField;
        
        private organization[] medOrgField;
        
        private property regionField;
        
        private education[] educationField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string userId {
            get {
                return this.userIdField;
            }
            set {
                this.userIdField = value;
                this.RaisePropertyChanged("userId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string lastName {
            get {
                return this.lastNameField;
            }
            set {
                this.lastNameField = value;
                this.RaisePropertyChanged("lastName");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string firstName {
            get {
                return this.firstNameField;
            }
            set {
                this.firstNameField = value;
                this.RaisePropertyChanged("firstName");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string middleName {
            get {
                return this.middleNameField;
            }
            set {
                this.middleNameField = value;
                this.RaisePropertyChanged("middleName");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public property role {
            get {
                return this.roleField;
            }
            set {
                this.roleField = value;
                this.RaisePropertyChanged("role");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("medOrg", Order=5)]
        public organization[] medOrg {
            get {
                return this.medOrgField;
            }
            set {
                this.medOrgField = value;
                this.RaisePropertyChanged("medOrg");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=6)]
        public property region {
            get {
                return this.regionField;
            }
            set {
                this.regionField = value;
                this.RaisePropertyChanged("region");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("education", Order=7)]
        public education[] education {
            get {
                return this.educationField;
            }
            set {
                this.educationField = value;
                this.RaisePropertyChanged("education");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://v25.iemk.atc.ru")]
    public partial class V25Response : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string formatField;
        
        private byte[] dataField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string format {
            get {
                return this.formatField;
            }
            set {
                this.formatField = value;
                this.RaisePropertyChanged("format");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="base64Binary", Order=1)]
        public byte[] data {
            get {
                return this.dataField;
            }
            set {
                this.dataField = value;
                this.RaisePropertyChanged("data");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://v25.iemk.atc.ru")]
    public partial class V25Request : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string formatField;
        
        private byte[] dataField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string format {
            get {
                return this.formatField;
            }
            set {
                this.formatField = value;
                this.RaisePropertyChanged("format");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="base64Binary", Order=1)]
        public byte[] data {
            get {
                return this.dataField;
            }
            set {
                this.dataField = value;
                this.RaisePropertyChanged("data");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class V25Process_AS_ProcessRequest {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://egisz.rosminzdrav.ru")]
        public Urish.Core.Net.Rosminzdrav.Iemc.v25.transportHeader transportHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://v25.iemk.atc.ru", Order=0)]
        public Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Request v25Request;
        
        public V25Process_AS_ProcessRequest() {
        }
        
        public V25Process_AS_ProcessRequest(Urish.Core.Net.Rosminzdrav.Iemc.v25.transportHeader transportHeader, Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Request v25Request) {
            this.transportHeader = transportHeader;
            this.v25Request = v25Request;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class V25Process_AS_ProcessResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://v25.iemk.atc.ru", Order=0)]
        public Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Response v25Response;
        
        public V25Process_AS_ProcessResponse() {
        }
        
        public V25Process_AS_ProcessResponse(Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Response v25Response) {
            this.v25Response = v25Response;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface V25Process_AS_PortTypeChannel : global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_PortType, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class V25Process_AS_ProcessCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        public V25Process_AS_ProcessCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        public Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Response Result {
            get {
                base.RaiseExceptionIfNecessary();
                return ((Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Response)(this.results[0]));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class V25Process_AS_PortTypeClient : System.ServiceModel.ClientBase<global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_PortType>, global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_PortType {
        
        private BeginOperationDelegate onBeginV25Process_AS_ProcessDelegate;
        
        private EndOperationDelegate onEndV25Process_AS_ProcessDelegate;
        
        private System.Threading.SendOrPostCallback onV25Process_AS_ProcessCompletedDelegate;
        
        public V25Process_AS_PortTypeClient() {
        }
        
        public V25Process_AS_PortTypeClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public V25Process_AS_PortTypeClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public V25Process_AS_PortTypeClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public V25Process_AS_PortTypeClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public event System.EventHandler<V25Process_AS_ProcessCompletedEventArgs> V25Process_AS_ProcessCompleted;
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_ProcessResponse global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_PortType.V25Process_AS_Process(global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_ProcessRequest request) {
            return base.Channel.V25Process_AS_Process(request);
        }
        
        public Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Response V25Process_AS_Process(Urish.Core.Net.Rosminzdrav.Iemc.v25.transportHeader transportHeader, Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Request v25Request) {
            global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_ProcessRequest inValue = new global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_ProcessRequest();
            inValue.transportHeader = transportHeader;
            inValue.v25Request = v25Request;
            global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_ProcessResponse retVal = ((global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_PortType)(this)).V25Process_AS_Process(inValue);
            return retVal.v25Response;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.IAsyncResult global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_PortType.BeginV25Process_AS_Process(global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_ProcessRequest request, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginV25Process_AS_Process(request, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public System.IAsyncResult BeginV25Process_AS_Process(Urish.Core.Net.Rosminzdrav.Iemc.v25.transportHeader transportHeader, Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Request v25Request, System.AsyncCallback callback, object asyncState) {
            global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_ProcessRequest inValue = new global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_ProcessRequest();
            inValue.transportHeader = transportHeader;
            inValue.v25Request = v25Request;
            return ((global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_PortType)(this)).BeginV25Process_AS_Process(inValue, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_ProcessResponse global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_PortType.EndV25Process_AS_Process(System.IAsyncResult result) {
            return base.Channel.EndV25Process_AS_Process(result);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Response EndV25Process_AS_Process(System.IAsyncResult result) {
            global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_ProcessResponse retVal = ((global::Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Process_AS_PortType)(this)).EndV25Process_AS_Process(result);
            return retVal.v25Response;
        }
        
        private System.IAsyncResult OnBeginV25Process_AS_Process(object[] inValues, System.AsyncCallback callback, object asyncState) {
            Urish.Core.Net.Rosminzdrav.Iemc.v25.transportHeader transportHeader = ((Urish.Core.Net.Rosminzdrav.Iemc.v25.transportHeader)(inValues[0]));
            Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Request v25Request = ((Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Request)(inValues[1]));
            return this.BeginV25Process_AS_Process(transportHeader, v25Request, callback, asyncState);
        }
        
        private object[] OnEndV25Process_AS_Process(System.IAsyncResult result) {
            Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Response retVal = this.EndV25Process_AS_Process(result);
            return new object[] {
                    retVal};
        }
        
        private void OnV25Process_AS_ProcessCompleted(object state) {
            if ((this.V25Process_AS_ProcessCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.V25Process_AS_ProcessCompleted(this, new V25Process_AS_ProcessCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void V25Process_AS_ProcessAsync(Urish.Core.Net.Rosminzdrav.Iemc.v25.transportHeader transportHeader, Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Request v25Request) {
            this.V25Process_AS_ProcessAsync(transportHeader, v25Request, null);
        }
        
        public void V25Process_AS_ProcessAsync(Urish.Core.Net.Rosminzdrav.Iemc.v25.transportHeader transportHeader, Urish.Core.Net.Rosminzdrav.Iemc.v25.V25Request v25Request, object userState) {
            if ((this.onBeginV25Process_AS_ProcessDelegate == null)) {
                this.onBeginV25Process_AS_ProcessDelegate = new BeginOperationDelegate(this.OnBeginV25Process_AS_Process);
            }
            if ((this.onEndV25Process_AS_ProcessDelegate == null)) {
                this.onEndV25Process_AS_ProcessDelegate = new EndOperationDelegate(this.OnEndV25Process_AS_Process);
            }
            if ((this.onV25Process_AS_ProcessCompletedDelegate == null)) {
                this.onV25Process_AS_ProcessCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnV25Process_AS_ProcessCompleted);
            }
            base.InvokeAsync(this.onBeginV25Process_AS_ProcessDelegate, new object[] {
                        transportHeader,
                        v25Request}, this.onEndV25Process_AS_ProcessDelegate, this.onV25Process_AS_ProcessCompletedDelegate, userState);
        }
    }
}
