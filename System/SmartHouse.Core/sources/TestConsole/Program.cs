﻿using System;
using System.IO;
using System.Xml;
using GostCryptography.Cryptography;
using Urish.Core.Api.Extensions;
using Urish.Core.Net;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            GostCryptoConfig.ProviderType = ProviderTypes.VipNet;

            var example = new Example(new RosminzdravIemcPixConnection(), 
                new RosminzdravIemcXdsConnection(), 
                new RosminzdravIemcV25Connection(), 
                new RosminzdravIemcXdsQueryConnection());

            var docId = new Guid("923EB1BD-3555-4948-9B52-0698604E9AEB");
            var patientId = new Guid("5908904B-7804-4C6D-84AD-4CCAE3EA16A0");
            var snils = "123456";

            {
                var cda_l3 = File.ReadAllBytes("cda_l3.xml");
                var cdaXml = new XmlDocument();
                cdaXml.Load(new MemoryStream(cda_l3));
                cdaXml["ClinicalDocument"]["recordTarget"]["patientRole"]["id"].Attributes[
                    "extension"].Value = patientId.ToNumber().ToString();
                cdaXml["ClinicalDocument"]["recordTarget"]["patientRole"]["patient"]["id"]
                    .Attributes["extension"].Value = patientId.ToNumber().ToString();
                cdaXml["ClinicalDocument"]["id"].Attributes["extension"].Value =
                    docId.ToString("D") + "ss39";
                var xmlNamespaceManager = new XmlNamespaceManager(new NameTable());
                xmlNamespaceManager.AddNamespace("x", "urn:hl7-org:v3");
                xmlNamespaceManager.AddNamespace("ext", "urn:hl7-RU-EHR:v1");
                var snilsdocNumberNode = cdaXml.SelectSingleNode(
                    "/x:ClinicalDocument/x:recordTarget/x:patientRole/x:patient" +
                    "/ext:patient/ext:asOtherIDs[ext:documentType[@code=3]]/ext:documentNumber",
                    xmlNamespaceManager);
                snilsdocNumberNode.Attributes["number"].Value = snils;

                cdaXml.Save("cda_l3.xml");
                cda_l3 = File.ReadAllBytes("cda_l3.xml");
                example.SendCdaToIemk(cda_l3, patientId, docId, 3);
            }
        }
    }
}
