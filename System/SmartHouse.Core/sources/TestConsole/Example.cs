﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Urish.Core.Api.Extensions;
using Urish.Core.Api.Utilities.Extensions;
using Urish.Core.Net;
using Urish.Core.Net.Rosminzdrav.Iemc.Pix;
using Urish.Core.Net.Rosminzdrav.Iemc.Xds;
using Urish.Core.Net.Rosminzdrav.Iemc.Xds.Query;
using AD = Urish.Core.Net.Rosminzdrav.Iemc.Pix.AD;
using ADXP = Urish.Core.Net.Rosminzdrav.Iemc.Pix.ADXP;
using adxpadditionalLocator = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpadditionalLocator;
using adxpbuildingNumberSuffix = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpbuildingNumberSuffix;
using adxpcareOf = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpcareOf;
using adxpcensusTract = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpcensusTract;
using adxpcity = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpcity;
using adxpcountry = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpcountry;
using adxpcounty = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpcounty;
using adxpdelimiter = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpdelimiter;
using adxpdeliveryAddressLine = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpdeliveryAddressLine;
using adxpdeliveryInstallationArea = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpdeliveryInstallationArea;
using adxpdeliveryInstallationQualifier = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpdeliveryInstallationQualifier;
using adxpdeliveryInstallationType = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpdeliveryInstallationType;
using adxpdeliveryMode = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpdeliveryMode;
using adxpdeliveryModeIdentifier = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpdeliveryModeIdentifier;
using adxpdirection = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpdirection;
using adxphouseNumber = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxphouseNumber;
using adxphouseNumberNumeric = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxphouseNumberNumeric;
using adxppostalCode = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxppostalCode;
using adxppostBox = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxppostBox;
using adxpprecinct = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpprecinct;
using adxpstate = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpstate;
using adxpstreetAddressLine = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpstreetAddressLine;
using adxpstreetName = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpstreetName;
using adxpstreetNameBase = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpstreetNameBase;
using adxpstreetNameType = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpstreetNameType;
using adxpunitID = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpunitID;
using adxpunitType = Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpunitType;
using AssociationType1 = Urish.Core.Net.Rosminzdrav.Iemc.Xds.AssociationType1;
using authInfo = Urish.Core.Net.Rosminzdrav.Iemc.Pix.authInfo;
using BL = Urish.Core.Net.Rosminzdrav.Iemc.Pix.BL;
using CD = Urish.Core.Net.Rosminzdrav.Iemc.Pix.CD;
using CE = Urish.Core.Net.Rosminzdrav.Iemc.Pix.CE;
using ClassificationType = Urish.Core.Net.Rosminzdrav.Iemc.Xds.ClassificationType;
using CS = Urish.Core.Net.Rosminzdrav.Iemc.Pix.CS;
using EN = Urish.Core.Net.Rosminzdrav.Iemc.Pix.EN;
using enfamily = Urish.Core.Net.Rosminzdrav.Iemc.Pix.enfamily;
using engiven = Urish.Core.Net.Rosminzdrav.Iemc.Pix.engiven;
using EntityClassDevice = Urish.Core.Net.Rosminzdrav.Iemc.Pix.EntityClassDevice;
using ENXP = Urish.Core.Net.Rosminzdrav.Iemc.Pix.ENXP;
using ExternalIdentifierType = Urish.Core.Net.Rosminzdrav.Iemc.Xds.ExternalIdentifierType;
using ExtrinsicObjectType = Urish.Core.Net.Rosminzdrav.Iemc.Xds.ExtrinsicObjectType;
using IdentifiableType = Urish.Core.Net.Rosminzdrav.Iemc.Xds.IdentifiableType;
using II = Urish.Core.Net.Rosminzdrav.Iemc.Pix.II;
using INT = Urish.Core.Net.Rosminzdrav.Iemc.Pix.INT;
using InternationalStringType = Urish.Core.Net.Rosminzdrav.Iemc.Xds.InternationalStringType;
using ItemsChoiceType2 = Urish.Core.Net.Rosminzdrav.Iemc.Pix.ItemsChoiceType2;
using IVL_TS = Urish.Core.Net.Rosminzdrav.Iemc.Pix.IVL_TS;
using IVXB_TS = Urish.Core.Net.Rosminzdrav.Iemc.Pix.IVXB_TS;
using LocalizedStringType = Urish.Core.Net.Rosminzdrav.Iemc.Xds.LocalizedStringType;
using ON = Urish.Core.Net.Rosminzdrav.Iemc.Pix.ON;
using PN = Urish.Core.Net.Rosminzdrav.Iemc.Pix.PN;
using QTY = Urish.Core.Net.Rosminzdrav.Iemc.Pix.QTY;
using RegistryPackageType = Urish.Core.Net.Rosminzdrav.Iemc.Xds.RegistryPackageType;
using SlotType1 = Urish.Core.Net.Rosminzdrav.Iemc.Xds.SlotType1;
using ST = Urish.Core.Net.Rosminzdrav.Iemc.Pix.ST;
using TEL = Urish.Core.Net.Rosminzdrav.Iemc.Pix.TEL;
using transportHeader = Urish.Core.Net.Rosminzdrav.Iemc.Pix.transportHeader;
using TS = Urish.Core.Net.Rosminzdrav.Iemc.Pix.TS;
using ValueListType = Urish.Core.Net.Rosminzdrav.Iemc.Xds.ValueListType;

namespace TestConsole
{
    public class Example
    {
        private readonly IRosminzdravIemcPixConnection _iemcPixConnection;
        private readonly IRosminzdravIemcXdsConnection _iemcXdsConnection;
        private readonly IRosminzdravIemcV25Connection _iemcV25Connection;
        private readonly IRosminzdravIemcXdsQueryConnection _iemcXdsQueryConnection;

        public Example(
            IRosminzdravIemcPixConnection iemcPixConnection,
            IRosminzdravIemcXdsConnection iemcXdsConnection,
            IRosminzdravIemcV25Connection iemcV25Connection,
            IRosminzdravIemcXdsQueryConnection iemcXdsQueryConnection)
        {
            _iemcPixConnection = iemcPixConnection;
            _iemcXdsConnection = iemcXdsConnection;
            _iemcV25Connection = iemcV25Connection;
            _iemcXdsQueryConnection = iemcXdsQueryConnection;
        }

        /*public IPerson CreatePatient()
        {
            // здесь нужно создать пациента в бд и вернуть его ид
            var rnd = new Random();
            var gender = _genderCatalog.GetAll(new PageData()).Single(o => o.Name == "мужской");
            IPerson person = new Person
            {
                FirstName = "Вадим",
                SecondName = "Петров",
                ThirdName = "Сергеевич",
                GenderId = gender.Id,
                Gender = gender,
                BirthDate = new DateTime(1927, 07, 17)
            };
            person = _personRegister.Insert(person);

            var phoneNumberType = _phoneNumberTypeCatalog.GetAll(new PageData()).Single(o => o.Name == "домашний");
            IPhone personPhone = new Phone
            {
                Number = "+7-926-333-22-44",
                PersonId = person.Id,
                PhoneNumberTypeId = phoneNumberType.Id,
                PhoneNumberType = phoneNumberType
            };
            personPhone = _phoneDictionary.Insert(personPhone);
            person.Phones = new[] { personPhone };

            var addressType = _addressTypeCatalog.GetAll(new PageData()).Single(o => o.Name == "прописка");
            IAddress address = new Address
            {
                AddressType = addressType,
                AddressTypeId = addressType.Id,
                BildingNo = "19",
                AppartmentNo = "3",
                //StreetId = new Guid("39c5a58b-11a0-b232-f37f-f3bb093eb6a7"),  // ФИАС улица Ленина в Обнинске РФ
                PersonId = person.Id
            };
            address = _addressDirectory.Insert(address);
            person.Addresses = new[] { address };

            var documentType = _documentTypeCatalog.GetDocumentType("31", null, null);
            IDocument snils = new Document
            {
                DocumentTypeId = documentType.Id, // 31 - код СНИЛСА
                DocumentType = documentType,
                Number = "{0:000000000}{1:00}".AsFormat(rnd.Next(999999999), rnd.Next(99)),
                PersonId = person.Id
            };
            snils = _documentDirectory.Insert(snils);
            var documentType2 = _documentTypeCatalog.GetDocumentType("21", null, null);
            IDocument passport = new Document
            {
                DocumentTypeId = documentType2.Id, // 21 - паспорт РФ
                DocumentType = documentType2,
                Serial = "3209",//rnd.Next(9999).ToString("0000"), //"4601",
                Number = "809457",//rnd.Next(999999).ToString("000000"), //"123321",
                PersonId = person.Id
            };
            passport = _documentDirectory.Insert(passport);
            var documentType3 = _documentTypeCatalog.GetDocumentType("32", null, null);
            IDocument omc = new Document
            {
                DocumentTypeId = documentType3.Id, // 32 - полис ОМС
                DocumentType = documentType3,
                Serial = "35",
                Number = "4755890003",//rnd.Next(999999999).ToString("0000000000"), //"3334879650",
                PersonId = person.Id
            };
            omc = _documentDirectory.Insert(omc);
            person.Documents = new[] { snils, passport, omc };
            return person;
        }*/

        /*public void ChangePatient(Guid patientId)
        {
            var patient = _personRegister.GetById(patientId);
            patient.BirthDate = new DateTime(1928, 07, 17);
            patient = _personRegister.Update(patient);
        }*/

        private static MCCI_MT000100UV01Sender _GetSender()
        {
            return new MCCI_MT000100UV01Sender
            {
                typeCode = Urish.Core.Net.Rosminzdrav.Iemc.Pix.CommunicationFunctionType.SND,
                device = new MCCI_MT000100UV01Device
                {
                    classCode = EntityClassDevice.DEV,
                    determinerCode = "INSTANCE",
                    id = new[]
                    {
                        new II {root = "482376cd-19a4-413e-b054-534241f0ad3a"},
                    },
                    name = new[] { new EN() { Text = new[] { "Региональная ИЭМК Куздрава" } } },
                    asAgent = new MCCI_MT000100UV01Agent()
                    {
                        classCode = "ASSIGNED",
                        representedOrganization = new MCCI_MT000100UV01Organization()
                        {
                            classCode = "ORG",
                            determinerCode = "INSTANCE",
                            id = new[] { new II() { root = "1.2.643.5.1.13.3.25.42.17" } },
                            name = new[]{new EN() {
                                Text = new []{"Государственное бюджетное учреждение здравоохранения Кемеровской области &quot;Кемеровский областной медицинский информационно-аналитический центр&quot;"}}},
                        },
                    },
                },
            };
        }

        private static MCCI_MT000100UV01Receiver[] _GetReceiver()
        {
            return new[]
            {
                new MCCI_MT000100UV01Receiver
                {
                    typeCode = Urish.Core.Net.Rosminzdrav.Iemc.Pix.CommunicationFunctionType.RCV,
                    device = new MCCI_MT000100UV01Device
                    {
                        classCode = EntityClassDevice.DEV,
                        determinerCode = "INSTANCE",
                        id = new[]
                        {
                            new II {root = "d5a0f9c0-5db4-11e3-949a-0800200c9a66"},
                        },
                        asAgent = new MCCI_MT000100UV01Agent()
                        {
                            classCode = "ASSIGNED",
                            representedOrganization = new MCCI_MT000100UV01Organization()
                            {
                                classCode = "ORG",
                                determinerCode = "INSTANCE",
                                id = new[]
                                {
                                    new II
                                    {
                                        root = "1.2.643.5.1.13.2.7.3"
                                    }
                                },
                                name = new[] {new EN() {Text = new[] {"ИЭМК"}}}
                            },
                        },
                    },
                },
            };
        }




        //public void SendPatientToIemk(Guid patientId)
        //{
        //    /*var patient = _personRegister.GetById(patientId);
        //    patient.Phones = _phoneDictionary.GetByPerson(patient, new PageData());
        //    patient.Gender = _genderCatalog.GetById(patient.GenderId.Value);
        //    patient.Documents = _documentDirectory.GetByPerson(patient, new PageData());
        //    patient.Documents.ForEach(document =>
        //    {
        //        if (document.DocumentTypeId != null)
        //            document.DocumentType = _documentTypeCatalog.GetById(document.DocumentTypeId.Value);
        //    });*/

        //    // отправить данные пациента в ИЭМК
        //    var prpaIn201301Ru02 = new PRPA_IN201301RU02
        //    {
        //        ITSVersion = "XML_1.0",
        //        id = new II { root = "1.2.643.5.1.13.3.25.42.17", extension = "134035427" },
        //        creationTime = new TS { value = "20131111163224" },
        //        interactionId =
        //            new II { root = "1.2.643.5.1.13.2.7.3", extension = "PRPA_IN201301RU01" },
        //        processingCode = new CS { code = "P" },
        //        processingModeCode = new CS { code = "T" },
        //        acceptAckCode = new CS { code = "AL" },
        //        receiver = _GetReceiver(),
        //        sender = _GetSender(),
        //        controlActProcess = new PRPA_IN201301RU02MFMI_MT700701RU02ControlActProcess()
        //        {
        //            classCode = ActClassControlAct.CACT,
        //            moodCode = x_ActMoodIntentEvent.EVN,
        //            subject = new[]
        //            {
        //                new PRPA_IN201301RU02MFMI_MT700701RU02Subject1()
        //                {
        //                    typeCode = "SUBJ",
        //                    registrationEvent =
        //                        new PRPA_IN201301RU02MFMI_MT700701RU02RegistrationEvent
        //                        {
        //                            classCode = "REG",
        //                            moodCode = "EVN",
        //                            id = new[]
        //                            {
        //                                new II {nullFlavor = "NA"}
        //                            },
        //                            statusCode = new CS {code = "active"},
        //                            subject1 = new PRPA_IN201301RU02MFMI_MT700701RU02Subject2
        //                            {
        //                                typeCode = ParticipationTargetSubject.SBJ,
        //                                patient = new PRPA_MT201301RU02Patient
        //                                {
        //                                    id = new[]
        //                                    {
        //                                        new II
        //                                        {
        //                                            root = "1.2.643.5.1.13.3.25.42.17",
        //                                            extension = new Guid("CCCA3A87-11F6-420A-98E8-76A35485CCB6").ToNumber().ToString()
        //                                        }
        //                                    },
        //                                    statusCode = new CS {code = "active"},
        //                                    Item = new PRPA_MT201301RU02Person
        //                                    {
        //                                        name = new[]
        //                                        {
        //                                            new PN
        //                                            {
        //                                                Items = new ENXP[]
        //                                                {
        //                                                    new enfamily
        //                                                    {
        //                                                        Text = new[] {patient.SecondName}
        //                                                    },
        //                                                    new engiven
        //                                                    {
        //                                                        Text = new[] {patient.FirstName}
        //                                                    },
        //                                                    new engiven
        //                                                    {
        //                                                        Text = new[] {patient.ThirdName}
        //                                                    }
        //                                                }
        //                                            }
        //                                        },
        //                                        telecom =
        //                                            patient.Phones.Select(
        //                                                o => new TEL {value = "tel:" + o.Number})
        //                                                .ToArray(),
        //                                        administrativeGenderCode = new CE
        //                                        {
        //                                            code = patient.Gender.Code == "01" ? "1" : "2",
        //                                            codeSystem = "1.2.643.5.1.13.2.1.1.156"
        //                                        },
        //                                        birthTime =
        //                                            new TS
        //                                            {
        //                                                value =
        //                                                    patient.BirthDate.Value.ToString(
        //                                                        "yyyyMMdd")
        //                                            },
        //                                        addr = new[]
        //                                        {
        //                                            new AD
        //                                            {
        //                                                // задать значение из БД
        //                                                Items = new ADXP[]
        //                                                {
        //                                                    /*new adxpdeliveryAddressLine
        //                                                    {
        //                                                        Text = new[] {"12-41, Октябрьский пр., Кемерово, Кемеровская область, Российская Федерация, 650066"}
        //                                                    },
        //                                                    new adxpcity {Text = new[] {"Кемерово"}},
        //                                                    new adxpstate
        //                                                    {
        //                                                        Text =
        //                                                            new[] {"Кемеровская область"}
        //                                                    },
        //                                                    new Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpcountry
        //                                                    {
        //                                                        Text = new []{"Российская Федерация"}
        //                                                    },
        //                                                    new Komiac.EHR.Net.Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpstreetName
        //                                                    {
        //                                                        Text = new []{"Октябрьский пр."}
        //                                                    },
        //                                                    new Komiac.EHR.Net.Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxphouseNumber
        //                                                    { Text = new []{"41"}},
        //                                                    new Komiac.EHR.Net.Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpadditionalLocator
        //                                                    {Text = new []{"12"}},
        //                                                    new Komiac.EHR.Net.Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpunitID
        //                                                    { Text = new []{"4200000900000"}},
        //                                                    new Komiac.EHR.Net.Urish.Core.Net.Rosminzdrav.Iemc.Pix.adxpdirection
        //                                                    {Text = new []{"55.347791,86.124755"}},*/

        //                                                    new adxpadditionalLocator {Text = new []{"adxpadditionalLocator"}},
        //                                                    new adxpbuildingNumberSuffix() {Text = new []{"adxpbuildingNumberSuffix"}},
        //                                                    new adxpcareOf() {Text = new []{"adxpcareOf"}},
        //                                                    new adxpcensusTract() {Text = new []{"adxpcensusTract"}},
        //                                                    new adxpcity() {Text = new []{"adxpcity"}},
        //                                                    new adxpcountry() {Text = new []{"adxpcountry"}},
        //                                                    new adxpcounty() {Text = new []{"adxpcounty"}},
        //                                                    new adxpdelimiter() {Text = new []{"adxpdelimiter"}},
        //                                                    new adxpdeliveryAddressLine() {Text = new []{"adxpdeliveryAddressLine"}},
        //                                                    new adxpdeliveryInstallationArea() {Text = new []{"adxpdeliveryInstallationArea"}},
        //                                                    new adxpdeliveryInstallationQualifier() {Text = new []{"adxpdeliveryInstallationQualifier"}},
        //                                                    new adxpdeliveryInstallationType() {Text = new []{"adxpdeliveryInstallationType"}},
        //                                                    new adxpdeliveryMode() {Text = new []{"adxpdeliveryMode"}},
        //                                                    new adxpdeliveryModeIdentifier() {Text = new []{"adxpdeliveryModeIdentifier"}},
        //                                                    new adxpdirection() {Text = new []{"adxpdirection"}},
        //                                                    new adxphouseNumber() {Text = new []{"adxphouseNumber"}},
        //                                                    new adxphouseNumberNumeric() {Text = new []{"adxphouseNumberNumeric"}},
        //                                                    new adxppostBox() {Text = new []{"adxppostBox"}},
        //                                                    new adxppostalCode() {Text = new []{"adxppostalCode"}},
        //                                                    new adxpprecinct() {Text = new []{"adxpprecinct"}},
        //                                                    new adxpstate() {Text = new []{"adxpstate"}},
        //                                                    new adxpstreetAddressLine() {Text = new []{"adxpstreetAddressLine"}},
        //                                                    new adxpstreetName() {Text = new []{"adxpstreetName"}},
        //                                                    new adxpstreetNameBase() {Text = new []{"adxpstreetNameBase"}},
        //                                                    new adxpstreetNameType() {Text = new []{"adxpstreetNameType"}},
        //                                                    new adxpunitID() {Text = new []{"adxpunitID"}},
        //                                                    new adxpunitType() {Text = new []{"adxpunitType"}}
        //                                                }
        //                                            }
        //                                        },
        //                                        asMember = new[]
        //                                        {
        //                                            new PRPA_MT201301UV02Member
        //                                            {
        //                                                classCode = "MBR",
        //                                                group = new PRPA_MT201301UV02Group
        //                                                {
        //                                                    classCode = "PUB",
        //                                                    code = new CE
        //                                                    {
        //                                                        code = "4",
        //                                                        codeSystem =
        //                                                            "1.2.643.5.1.13.2.1.1.366",
        //                                                        displayName = "Работающий"
        //                                                    },
        //                                                },
        //                                            },
        //                                            new PRPA_MT201301UV02Member
        //                                            {
        //                                                classCode = "MBR",
        //                                                group = new PRPA_MT201301UV02Group
        //                                                {
        //                                                    classCode = "PUB",
        //                                                    code = new CE
        //                                                    {
        //                                                        code = "11",
        //                                                        codeSystem =
        //                                                            "1.2.643.5.1.13.2.1.1.358",
        //                                                        displayName =
        //                                                            "граждане РФ, пострадавшие от радиации"
        //                                                    }
        //                                                }
        //                                            }
        //                                        },
        //                                        asOtherIDs = new[]
        //                                        {
        //                                            new PRPA_MT201301RU02OtherIDs
        //                                            {
        //                                                classCode = "IDENT",
        //                                                documentType = new CD { code = "3", codeSystem = "1.2.643.5.1.13.2.7.1.62"},
        //                                                documentNumber = new PRPA_MT201301RU02documentNumberType
        //                                                {
        //                                                    number = patient.Documents
        //                                                            .Where(
        //                                                                o =>
        //                                                                    o.DocumentType.Code ==
        //                                                                    "31")
        //                                                            .Select(
        //                                                                o => Regex.Replace(
        //                                                                    string.Concat(o.Serial,
        //                                                                        o.Number),
        //                                                                    @"\D", "")).Single()
        //                                                },
        //                                                effectiveTime = new IVL_TS() { value = "20100214" },
        //                                                scopingOrganization = new COCT_MT150002UV01Organization
        //                                                {
        //                                                    classCode = "ORG", determinerCode = "INSTANCE",
        //                                                    id = new []{new II {nullFlavor = "NI"}}
        //                                                },
        //                                            },
        //                                            new PRPA_MT201301RU02OtherIDs
        //                                            {
        //                                                classCode = "IDENT",
        //                                                documentType = new CD {code = "1", codeSystem = "1.2.643.5.1.13.2.7.1.62" },
        //                                                documentNumber = new PRPA_MT201301RU02documentNumberType
        //                                                {
        //                                                    number = patient.Documents
        //                                                            .Where(
        //                                                                o =>
        //                                                                    o.DocumentType.Code ==
        //                                                                    "32")
        //                                                            .Select(
        //                                                                o => Regex.Replace(
        //                                                                    string.Concat(o.Serial,
        //                                                                        o.Number),
        //                                                                    @"\D", "")).Single()
        //                                                },
        //                                                effectiveTime = new IVL_TS
        //                                                {
        //                                                    value = "20100214"
        //                                                },
        //                                                scopingOrganization =
        //                                                    new COCT_MT150002UV01Organization
        //                                                    {
        //                                                        classCode = "ORG",
        //                                                        determinerCode = "INSTANCE",
        //                                                        id =
        //                                                            new[]
        //                                                            {
        //                                                                new II
        //                                                                {
        //                                                                    root = "1.2.643.5.1.13.2.1.1.635",
        //                                                                    extension = "39",
        //                                                                    assigningAuthorityName = "ЗАО &quot;МАКС-М&quot;"
        //                                                                }
        //                                                            },
        //                                                        name =
        //                                                            new[]
        //                                                            {
        //                                                                new ON
        //                                                                {
        //                                                                    Text =
        //                                                                        new[]
        //                                                                        {"ЗАО &quot;МАКС-М&quot;"}
        //                                                                }
        //                                                            }
        //                                                    }
        //                                            },
        //                                            new PRPA_MT201301RU02OtherIDs
        //                                            {
        //                                                classCode = "PAT",
        //                                                documentType = new CD() { code = "5", codeSystem = "1.2.643.5.1.13.2.7.1.62",
        //                                                    qualifier = new []{new Komiac.EHR.Net.Urish.Core.Net.Rosminzdrav.Iemc.Pix.CR
        //                                                    {
        //                                                        name = new Komiac.EHR.Net.Urish.Core.Net.Rosminzdrav.Iemc.Pix.CV
        //                                                        {
        //                                                            code = "14", codeSystem = "1.2.643.5.1.13.2.1.1.498",
        //                                                            codeSystemName = "Классификатор документов, удостоверяющих личность гражданина Российской Федерации",
        //                                                            displayName = "Паспорт гражданина РФ"
        //                                                        }
        //                                                    }}
        //                                                },
        //                                                documentNumber = new PRPA_MT201301RU02documentNumberType
        //                                                {
        //                                                    number = patient.Documents
        //                                                            .Where(
        //                                                                o =>
        //                                                                    o.DocumentType.Code ==
        //                                                                    "21")
        //                                                            .Select(
        //                                                                o => Regex.Replace(
        //                                                                    string.Concat(o.Serial,
        //                                                                        o.Number),
        //                                                                    @"\D", "")).Single()
        //                                                },
        //                                                effectiveTime =
        //                                                    new IVL_TS {value = "20020505"},
        //                                                scopingOrganization =
        //                                                    new COCT_MT150002UV01Organization
        //                                                    {
        //                                                        classCode = "ORG",
        //                                                        determinerCode = "INSTANCE",
        //                                                        id =
        //                                                            new[]
        //                                                            {
        //                                                                new II
        //                                                                {
        //                                                                    nullFlavor = "NA"
        //                                                                }
        //                                                            },
        //                                                        name =
        //                                                            new[]
        //                                                            {
        //                                                                new ON
        //                                                                {
        //                                                                    Text =
        //                                                                        new[]
        //                                                                        {
        //                                                                            "ОУФМС России по Кемеровской области в Центральном районе гор. Кемерово"
        //                                                                        }
        //                                                                }
        //                                                            }
        //                                                    }
        //                                            },
        //                                            new PRPA_MT201301RU02OtherIDs()
        //                                            {
        //                                                classCode = "PAT",
        //                                                documentType = new CD { code = "9", codeSystem = "1.2.643.5.1.13.2.7.1.62" },
        //                                                documentNumber = new PRPA_MT201301RU02documentNumberType()
        //                                                {
        //                                                    number = patient.Id.ToNumber().ToString()
        //                                                },
        //                                                scopingOrganization = new COCT_MT150002UV01Organization
        //                                                {
        //                                                    classCode = "ORG", determinerCode = "INSTANCE",
        //                                                    id = new []
        //                                                    {
        //                                                        new II
        //                                                        {
        //                                                            root = "1.2.643.5.1.13.3.25.42.17",
        //                                                            assigningAuthorityName = "Государственное бюджетное учреждение здравоохранения Кемеровской области &quot;Кемеровский областной медицинский информационно-аналитический центр&quot;"
        //                                                        }
        //                                                    },
        //                                                    name = new []{new ON { Text = new []{"Государственное бюджетное учреждение здравоохранения Кемеровской области &quot;Кемеровский областной медицинский информационно-аналитический центр&quot;"}}}
        //                                                }
        //                                            }
        //                                        },
        //                                        asUnidentified = new BL {value = false},
        //                                        extraMark =
        //                                            new[] {new CE {displayName = "Отсутствуют"}},
        //                                        birthPlace = new PRPA_MT201301UV02BirthPlace
        //                                        {
        //                                            classCode = "BIRTHPL",
        //                                            addr = new AD
        //                                            {
        //                                                Items = new ADXP[]
        //                                                {
        //                                                    new adxpcity
        //                                                    {
        //                                                        Text = new[] {"Красноярск"}
        //                                                    },
        //                                                    new adxpstate
        //                                                    {
        //                                                        Text =
        //                                                            new[] {"Российская Федерация"}
        //                                                    }
        //                                                }
        //                                            }
        //                                        }
        //                                    },
        //                                    providerOrganization =
        //                                        new Urish.Core.Net.Rosminzdrav.Iemc.Pix.
        //                                            COCT_MT150003UV03Organization
        //                                        {
        //                                            classCode = "ORG",
        //                                            determinerCode = "INSTANCE",
        //                                            id =
        //                                                new[]
        //                                                {
        //                                                    new II {root = "1.2.643.5.1.13.3.25.42.17"}
        //                                                },
        //                                            name = new[]
        //                                            {
        //                                                new ON
        //                                                {
        //                                                    Text = new[]
        //                                                    {
        //                                                        "Государственное бюджетное учреждение здравоохранения Кемеровской области \"Кемеровский областной медицинский информационно-аналитический центр\"^^^^^^^^^1.2.643.5.1.13.3.25.42.17"
        //                                                    }
        //                                                }
        //                                            },
        //                                            //"Институт кардиохирургии им. В. И. Бураковского^^^^^^^^^1.2.643.5.1.13.3.25.77.761"}}},
        //                                            contactParty = new[]
        //                                            {
        //                                                new Urish.Core.Net.Rosminzdrav.Iemc.Pix.
        //                                                    COCT_MT150003UV03ContactParty
        //                                                {
        //                                                    telecom =
        //                                                        new[]
        //                                                        {
        //                                                            new TEL
        //                                                            {
        //                                                                value =
        //                                                                    "tel:+7-384-2540-610"
        //                                                            }
        //                                                        }
        //                                                }
        //                                            }
        //                                        }
        //                                }
        //                            },
        //                            custodian = new MFMI_MT700701UV01Custodian
        //                            {
        //                                typeCode = "CST",
        //                                assignedEntity = new COCT_MT090003UV01AssignedEntity
        //                                {
        //                                    classCode = "ASSIGNED",
        //                                    id = new[] {new II {root = "1.2.643.5.1.13.3.25.42.17"}},
        //                                    Item = new COCT_MT090003UV01Organization
        //                                    {
        //                                        classCode = "ORG",
        //                                        determinerCode = "INSTANCE",
        //                                        name = new[]
        //                                        {
        //                                            new EN
        //                                            {
        //                                                Text = new[]
        //                                                {
        //                                                    "Государственное бюджетное учреждение здравоохранения Кемеровской области \"Кемеровский областной медицинский информационно-аналитический центр\"^^^^^^^^^1.2.643.5.1.13.3.25.42.17"
        //                                                }
        //                                            }
        //                                        }
        //                                        //"Институт кардиохирургии им. В. И. Бураковского^^^^^^^^^1.2.643.5.1.13.3.25.77.761"}}}
        //                                    }
        //                                }
        //                            }
        //                        }
        //                }
        //            }
        //        }
        //    };

        //    var addedResponse = _iemcPixConnection.added(new addedRequest
        //    {
        //        transportHeader = new transportHeader
        //        {
        //            authInfo = new authInfo { clientEntityId = "482376cd-19a4-413e-b054-534241f0ad3a" }
        //        },
        //        PRPA_IN201301RU02 = prpaIn201301Ru02
        //    });
        //}

        //public void ReceivePatientFromIemk(Guid patientId)
        //{
        //    var patient = _personRegister.GetById(patientId);
        //    patient.Phones = _phoneDictionary.GetByPerson(patient, new PageData());
        //    patient.Gender = _genderCatalog.GetById(patient.GenderId.Value);
        //    patient.Documents = _documentDirectory.GetByPerson(patient, new PageData());
        //    patient.Documents.ForEach(document =>
        //    {
        //        if (document.DocumentTypeId != null)
        //            document.DocumentType = _documentTypeCatalog.GetById(document.DocumentTypeId.Value);
        //    });
        //    var snils = Regex.Replace(patient.Documents.Single(o => o.DocumentType.Code == "31").Number, @"\D", "");
        //    // вернуть данные пациента из ИЭМК
        //    var prpaIn201305Ru02 = new PRPA_IN201305RU02
        //    {
        //        ITSVersion = "XML_1.0",
        //        id = new II { root = "1.2.643.5.1.13.3.25.42.17", extension = "123523745" },
        //        creationTime = new TS { value = "20070428150301" },
        //        interactionId =
        //            new II { root = "1.2.643.5.1.13.2.7.3", extension = "PRPA_IN201305RU01" },
        //        processingCode = new CS { code = "P" },
        //        processingModeCode = new CS { code = "T" },
        //        acceptAckCode = new CS { code = "AL" },
        //        receiver = _GetReceiver(),
        //        sender = _GetSender(),
        //        controlActProcess = new PRPA_IN201305RU02QUQI_MT021001RU02ControlActProcess
        //        {
        //            classCode = ActClassControlAct.CACT,
        //            moodCode = x_ActMoodIntentEvent.EVN,
        //            code = new CD { code = "PRPA_TE201305UV02", codeSystem = "2.16.643.1.113883.1.6" },
        //            queryByParameter = new PRPA_MT201306RU02QueryByParameter()
        //            {
        //                queryId =
        //                    new II { root = "1.2.643.5.1.13.3.25.42.17", extension = "1439562934" },
        //                statusCode = new CS { code = "new" },
        //                initialQuantity = new INT { value = "2" },
        //                parameterList = new PRPA_MT201306RU02ParameterList()
        //                {
        //                    livingSubjectAdministrativeGender = new[]
        //                    {
        //                        new PRPA_MT201306UV02LivingSubjectAdministrativeGender
        //                        {
        //                            value = new[] {new CE {code = "1"}},
        //                            semanticsText =
        //                                new ST {Text = new[] {"LivingSubject.administrativeGender"}}
        //                        }
        //                    },
        //                    livingSubjectBirthTime = new[]
        //                    {
        //                        new PRPA_MT201306UV02LivingSubjectBirthTime
        //                        {
        //                            value = new[]
        //                            {
        //                                new IVL_TS
        //                                {
        //                                    Items = new QTY[]
        //                                    {
        //                                        new IVXB_TS {value = "19270717"},
        //                                        new IVXB_TS {value = "19810804"}
        //                                    },
        //                                    ItemsElementName = new[]
        //                                    {
        //                                        ItemsChoiceType2.low,
        //                                        ItemsChoiceType2.high
        //                                    }
        //                                }
        //                            },
        //                            semanticsText =
        //                                new ST {Text = new[] {"LivingSubject.birthTime"}},
        //                        }
        //                    },
        //                    livingSubjectName = new[]
        //                    {
        //                        new PRPA_MT201306UV02LivingSubjectName
        //                        {
        //                            value = new[]
        //                            {
        //                                new EN
        //                                {
        //                                    Items = new ENXP[]
        //                                    {
        //                                        new enfamily {Text = new[] {patient.SecondName}},
        //                                        new engiven {Text = new[] {patient.FirstName}},
        //                                        new engiven {Text = new[] {patient.ThirdName}}
        //                                    }
        //                                }
        //                            },
        //                            semanticsText = new ST {Text = new[] {"LivingSubject.name"}}
        //                        }
        //                    },
        //                    otherIDsScopingOrganization = new[]
        //                    {
        //                        new PRPA_MT201306UV02OtherIDsScopingOrganization
        //                        {
        //                            value =
        //                                new[] {new II {root = "1.2.643.100.3", extension = snils}},
        //                            semanticsText =
        //                                new ST {Text = new[] {"OtherIDs.ScopingOrganization"}}
        //                        }
        //                    },
        //                    livingSubjectAsUnidentified = new[]
        //                    {
        //                        new PRPA_MT201306RU02LivingSubjectAsUnidentified
        //                        {
        //                            value = new[] {new BL {value = false}},
        //                            semanticsText =
        //                                new ST {Text = new[] {"LivingSubject.asUnidentified"}}
        //                        }
        //                    },
        //                    livingSubjectExtraMark = new[]
        //                    {
        //                        new PRPA_MT201306RU02LivingSubjectExtraMark
        //                        {
        //                            value = new[] {new CE()},
        //                            semanticsText =
        //                                new ST {Text = new[] {"LivingSubject.extraMark"}}
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    };
        //    var findCandidatesResponse = _iemcPixConnection.findCandidates(new findCandidatesRequest
        //    {
        //        transportHeader = new transportHeader
        //        {
        //            authInfo = new authInfo { clientEntityId = "482376cd-19a4-413e-b054-534241f0ad3a" }
        //        },
        //        PRPA_IN201305RU02 = prpaIn201305Ru02
        //    });
        //}

        //public void CommitPatientToIemk(Guid patientId)
        //{
        //    var patient = _personRegister.GetById(patientId);
        //    patient.Phones = _phoneDictionary.GetByPerson(patient, new PageData());
        //    patient.Gender = _genderCatalog.GetById(patient.GenderId.Value);
        //    patient.Documents = _documentDirectory.GetByPerson(patient, new PageData());
        //    patient.Documents.ForEach(document =>
        //    {
        //        if (document.DocumentTypeId != null)
        //            document.DocumentType = _documentTypeCatalog.GetById(document.DocumentTypeId.Value);
        //    });

        //    var prpaIn201302Ru02 = new PRPA_IN201302RU02()
        //    {
        //        ITSVersion = "XML_1.0",
        //        id = new II { root = "1.2.643.5.1.13.3.25.42.17", extension = "1349023742" },
        //        creationTime = new TS { value = "20111111191603" },
        //        interactionId =
        //            new II { root = "1.2.643.5.1.13.2.7.3", extension = "PRPA_IN201302RU01" },
        //        processingCode = new CS { code = "P" },
        //        processingModeCode = new CS { code = "T" },
        //        acceptAckCode = new CS { code = "AL" },
        //        receiver = _GetReceiver(),
        //        sender = _GetSender(),
        //        controlActProcess = new PRPA_IN201302RU02MFMI_MT700701RU02ControlActProcess
        //        {
        //            classCode = ActClassControlAct.CACT,
        //            moodCode = x_ActMoodIntentEvent.EVN,
        //            subject = new[]
        //            {
        //                new PRPA_IN201302RU02MFMI_MT700701RU02Subject1
        //                {
        //                    typeCode = "SUBJ",
        //                    registrationEvent =
        //                        new PRPA_IN201302RU02MFMI_MT700701RU02RegistrationEvent
        //                        {
        //                            classCode = "REG",
        //                            moodCode = "EVN",
        //                            id = new[]
        //                            {
        //                                new II {nullFlavor = "NA"}
        //                            },
        //                            statusCode = new CS {code = "active"},
        //                            subject1 = new PRPA_IN201302RU02MFMI_MT700701RU02Subject2
        //                            {
        //                                typeCode = ParticipationTargetSubject.SBJ,
        //                                patient = new PRPA_MT201302RU02Patient
        //                                {
        //                                    id = new[]
        //                                    {
        //                                        new PRPA_MT201302UV02Patientid
        //                                        {
        //                                            root = "1.2.643.5.1.13.3.25.42.17",
        //                                            extension = patient.Id.ToNumber().ToString()
        //                                        }
        //                                    },
        //                                    statusCode =
        //                                        new PRPA_MT201302UV02PatientstatusCode
        //                                        {
        //                                            code = "active"
        //                                        },
        //                                    Item = new PRPA_MT201302RU02PatientpatientPerson
        //                                    {
        //                                        name = new[]
        //                                        {
        //                                            new PN
        //                                            {
        //                                                Items = new ENXP[]
        //                                                {
        //                                                    new enfamily
        //                                                    {
        //                                                        Text = new[] {patient.SecondName}
        //                                                    },
        //                                                    new engiven
        //                                                    {
        //                                                        Text = new[] {patient.FirstName}
        //                                                    },
        //                                                    new engiven
        //                                                    {
        //                                                        Text = new[] {patient.ThirdName}
        //                                                    }
        //                                                }
        //                                            }
        //                                        },
        //                                        telecom =
        //                                            patient.Phones.Select(
        //                                                o => new TEL {value = "tel:" + o.Number})
        //                                                .ToArray(),
        //                                        administrativeGenderCode = new CE
        //                                        {
        //                                            code = patient.Gender.Code == "01" ? "1" : "2",
        //                                            codeSystem = "1.2.643.5.1.13.2.1.1.156"
        //                                        },
        //                                        birthTime =
        //                                            new TS
        //                                            {
        //                                                value =
        //                                                    patient.BirthDate.Value.ToString(
        //                                                        "yyyyMMdd")
        //                                            },
        //                                        addr = new[]
        //                                        {
        //                                            new AD
        //                                            {
        //                                                // задать значение из БД
        //                                                Items = new ADXP[]
        //                                                {
        //                                                    new adxpdeliveryAddressLine
        //                                                    {
        //                                                        Text = new[] {"Ленина ул., 19,,,3"},
        //                                                    },
        //                                                    new adxpcity {Text = new[] {"Обнинск"}},
        //                                                    new adxpstate
        //                                                    {
        //                                                        Text =
        //                                                            new[] {"Российская Федерация"}
        //                                                    }
        //                                                }
        //                                            }
        //                                        },
        //                                        asMember = new[]
        //                                        {
        //                                            new PRPA_MT201302UV02Member
        //                                            {
        //                                                classCode = "MBR",
        //                                                group = new PRPA_MT201302UV02Group
        //                                                {
        //                                                    classCode = "PUB",
        //                                                    code = new CE
        //                                                    {
        //                                                        code = "4",
        //                                                        codeSystem =
        //                                                            "1.2.643.5.1.13.2.1.1.366",
        //                                                        displayName = "Работающий"
        //                                                    },
        //                                                },
        //                                            },
        //                                            new PRPA_MT201302UV02Member
        //                                            {
        //                                                classCode = "MBR",
        //                                                group = new PRPA_MT201302UV02Group
        //                                                {
        //                                                    classCode = "PUB",
        //                                                    code = new CE
        //                                                    {
        //                                                        code = "3",
        //                                                        codeSystem =
        //                                                            "1.2.643.5.1.13.2.1.1.358",
        //                                                        displayName =
        //                                                            "участники Великой Отечественной войны"
        //                                                    }
        //                                                }
        //                                            }
        //                                        },
        //                                        asOtherIDs = new[]
        //                                        {
        //                                            new PRPA_MT201302RU02OtherIDs()
        //                                            {
        //                                                classCode = "IDENT",
        //                                                documentType =
        //                                                    new CD
        //                                                    {
        //                                                        code = "3",
        //                                                        codeSystem =
        //                                                            "1.2.643.5.1.13.2.7.1.62"
        //                                                    },
        //                                                documentNumber =
        //                                                    new PRPA_MT201302RU02documentNumberType()
        //                                                    {
        //                                                        number = patient.Documents
        //                                                            .Where(
        //                                                                o =>
        //                                                                    o.DocumentType.Code ==
        //                                                                    "31")
        //                                                            .Select(
        //                                                                o => Regex.Replace(
        //                                                                    string.Concat(o.Serial,
        //                                                                        o.Number),
        //                                                                    @"\D", "")).Single()
        //                                                    },
        //                                                scopingOrganization =
        //                                                    new COCT_MT150002UV01Organization
        //                                                    {
        //                                                        classCode = "ORG", determinerCode = "INSTANCE",
        //                                                        id = new []{new II {nullFlavor = "NI"}}
        //                                                    },
        //                                            },
        //                                            new PRPA_MT201302RU02OtherIDs
        //                                            {
        //                                                classCode = "IDENT",
        //                                                documentType =
        //                                                    new CD
        //                                                    {
        //                                                        code = "1",
        //                                                        codeSystem =
        //                                                            "1.2.643.5.1.13.2.7.1.62"
        //                                                    },
        //                                                documentNumber =
        //                                                    new PRPA_MT201302RU02documentNumberType()
        //                                                    {
        //                                                        number = patient.Documents
        //                                                            .Where(
        //                                                                o =>
        //                                                                    o.DocumentType.Code ==
        //                                                                    "32")
        //                                                            .Select(
        //                                                                o => Regex.Replace(
        //                                                                    string.Concat(o.Serial,
        //                                                                        o.Number),
        //                                                                    @"\D", "")).Single()
        //                                                    },
        //                                                effectiveTime = new IVL_TS
        //                                                {
        //                                                    value = "20100214"
        //                                                },
        //                                                scopingOrganization =
        //                                                    new COCT_MT150002UV01Organization
        //                                                    {
        //                                                        classCode = "ORG",
        //                                                        determinerCode = "INSTANCE",
        //                                                        id =
        //                                                            new[]
        //                                                            {
        //                                                                new II
        //                                                                {
        //                                                                    root =
        //                                                                        "1.2.643.5.1.13.2.1.1.635",
        //                                                                    extension = "39",
        //                                                                    assigningAuthorityName =
        //                                                                        "ЗАО &quot;МАКС-М&quot;"
        //                                                                }
        //                                                            },
        //                                                        name =
        //                                                            new[]
        //                                                            {
        //                                                                new ON
        //                                                                {
        //                                                                    Text =
        //                                                                        new[]
        //                                                                        {
        //                                                                            "ЗАО &quot;МАКС-М&quot;"
        //                                                                        }
        //                                                                }
        //                                                            }
        //                                                    }
        //                                            },
        //                                            new PRPA_MT201302RU02OtherIDs
        //                                            {
        //                                                classCode = "PAT",
        //                                                documentType = new CD()
        //                                                {
        //                                                    code = "5",
        //                                                    codeSystem = "1.2.643.5.1.13.2.7.1.62",
        //                                                    qualifier = new []{new Komiac.EHR.Net.Urish.Core.Net.Rosminzdrav.Iemc.Pix.CR
        //                                                    {
        //                                                        name = new Komiac.EHR.Net.Urish.Core.Net.Rosminzdrav.Iemc.Pix.CV
        //                                                        {
        //                                                            code = "14", codeSystem = "1.2.643.5.1.13.2.1.1.498",
        //                                                            codeSystemName = "Классификатор документов, удостоверяющих личность гражданина Российской Федерации",
        //                                                            displayName = "Паспорт гражданина РФ"
        //                                                        }
        //                                                    }}
        //                                                },
        //                                                documentNumber =
        //                                                    new PRPA_MT201302RU02documentNumberType()
        //                                                    {
        //                                                        number = patient.Documents
        //                                                            .Where(
        //                                                                o =>
        //                                                                    o.DocumentType.Code ==
        //                                                                    "21")
        //                                                            .Select(
        //                                                                o => Regex.Replace(
        //                                                                    string.Concat(o.Serial,
        //                                                                        o.Number),
        //                                                                    @"\D", "")).Single()
        //                                                    },
        //                                                effectiveTime =
        //                                                    new IVL_TS {value = "20020505"},
        //                                                scopingOrganization =
        //                                                    new COCT_MT150002UV01Organization
        //                                                    {
        //                                                        classCode = "ORG",
        //                                                        determinerCode = "INSTANCE",
        //                                                        id =
        //                                                            new[]
        //                                                            {
        //                                                                new II
        //                                                                {
        //                                                                    nullFlavor = "NA"
        //                                                                }
        //                                                            },
        //                                                        name =
        //                                                            new[]
        //                                                            {
        //                                                                new ON
        //                                                                {
        //                                                                    Text =
        //                                                                        new[]
        //                                                                        {
        //                                                                            "ОУФМС России по Кемеровской области в Центральном районе гор. Кемерово"
        //                                                                        }
        //                                                                }
        //                                                            }
        //                                                    }
        //                                            },
        //                                            new PRPA_MT201302RU02OtherIDs()
        //                                            {
        //                                                classCode = "PAT",
        //                                                documentType = new CD { code = "9", codeSystem = "1.2.643.5.1.13.2.7.1.62" },
        //                                                documentNumber = new PRPA_MT201302RU02documentNumberType()
        //                                                {
        //                                                    number = patient.Id.ToNumber().ToString()
        //                                                },
        //                                                scopingOrganization = new COCT_MT150002UV01Organization
        //                                                {
        //                                                    classCode = "ORG", determinerCode = "INSTANCE",
        //                                                    id = new []
        //                                                    {
        //                                                        new II
        //                                                        {
        //                                                            root = "1.2.643.5.1.13.3.25.42.17",
        //                                                            assigningAuthorityName = "Государственное бюджетное учреждение здравоохранения Кемеровской области &quot;Кемеровский областной медицинский информационно-аналитический центр&quot;"
        //                                                        }
        //                                                    },
        //                                                    name = new []{new ON { Text = new []{"Государственное бюджетное учреждение здравоохранения Кемеровской области &quot;Кемеровский областной медицинский информационно-аналитический центр&quot;"}}}
        //                                                }
        //                                            }
        //                                        },

        //                                        asUnidentified = new BL {value = false},
        //                                        extraMark =
        //                                            new[] {new CE {displayName = "Отсутствуют"}},
        //                                        birthPlace = new PRPA_MT201302UV02BirthPlace
        //                                        {
        //                                            classCode = "BIRTHPL",
        //                                            addr = new AD
        //                                            {
        //                                                Items = new ADXP[]
        //                                                {
        //                                                    new adxpcity
        //                                                    {
        //                                                        Text = new[] {"Красноярск"}
        //                                                    },
        //                                                    new adxpstate
        //                                                    {
        //                                                        Text =
        //                                                            new[] {"Российская Федерация"}
        //                                                    }
        //                                                }
        //                                            }
        //                                        }
        //                                    },
        //                                    providerOrganization =
        //                                        new Urish.Core.Net.Rosminzdrav.Iemc.Pix.
        //                                            COCT_MT150003UV03Organization
        //                                        {
        //                                            classCode = "ORG",
        //                                            determinerCode = "INSTANCE",
        //                                            id =
        //                                                new[]
        //                                                {
        //                                                    new II {root = "1.2.643.5.1.13.3.25.42.17"}
        //                                                },
        //                                            name = new[]
        //                                            {
        //                                                new ON
        //                                                {
        //                                                    Text = new[]
        //                                                    {
        //                                                        "Государственное бюджетное учреждение здравоохранения Кемеровской области \"Кемеровский областной медицинский информационно-аналитический центр\"^^^^^^^^^1.2.643.5.1.13.3.25.42.17"
        //                                                    }
        //                                                }
        //                                            },
        //                                            contactParty = new[]
        //                                            {
        //                                                new Urish.Core.Net.Rosminzdrav.Iemc.Pix.
        //                                                    COCT_MT150003UV03ContactParty
        //                                                {
        //                                                    telecom =
        //                                                        new[]
        //                                                        {
        //                                                            new TEL
        //                                                            {
        //                                                                value =
        //                                                                    "tel:+7-384-2540-610"
        //                                                            }
        //                                                        }
        //                                                }
        //                                            }
        //                                        }
        //                                }
        //                            },
        //                            custodian = new MFMI_MT700701UV01Custodian
        //                            {
        //                                typeCode = "CST",
        //                                assignedEntity = new COCT_MT090003UV01AssignedEntity
        //                                {
        //                                    classCode = "ASSIGNED",
        //                                    id = new[] {new II {root = "1.2.643.5.1.13.3.25.42.17"}},
        //                                    Item = new COCT_MT090003UV01Organization
        //                                    {
        //                                        classCode = "ORG",
        //                                        determinerCode = "INSTANCE",
        //                                        name = new[]
        //                                        {
        //                                            new EN
        //                                            {
        //                                                Text = new[]
        //                                                {
        //                                                    "Государственное бюджетное учреждение здравоохранения Кемеровской области \"Кемеровский областной медицинский информационно-аналитический центр\"^^^^^^^^^1.2.643.5.1.13.3.25.42.17"
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                }
        //            }
        //        }
        //    };
        //    var findCandidatesResponse = _iemcPixConnection.revised(new revisedRequest
        //    {
        //        transportHeader = new transportHeader
        //        {
        //            authInfo = new authInfo { clientEntityId = "482376cd-19a4-413e-b054-534241f0ad3a" }
        //        },
        //        PRPA_IN201302RU02 = prpaIn201302Ru02
        //    });
        //}

        private static Dictionary<int, string> cdalevels = new Dictionary
            <int, string>
        {
            {1, "application/hl7-cda-level-one+xml"},
            {2, "application/hl7-cda-level-two+xml"},
            {3, "application/hl7-cda-level-three+xml"}
        };

        public void SendCdaToIemk(byte[] cdaDoc, Guid patientId, Guid docId, int level)
        {
            var provideAndRegisterDocumentSetRequestType = new ProvideAndRegisterDocumentSetRequestType
            {
                SubmitObjectsRequest = new SubmitObjectsRequest
                {
                    RegistryObjectList = new IdentifiableType[]
                    {
                        new ExtrinsicObjectType
                        {
                            id = "Document01",
                            mimeType = "text/xml",
                            objectType = "urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",
                            Slot = new[]
                            {
                                new SlotType1
                                {
                                    name = "creationTime",
                                    ValueList = new ValueListType()
                                    {
                                        Value = new[] {"20131112185007"}
                                    }
                                },
                                new SlotType1
                                {
                                    name = "languageCode",
                                    ValueList = new ValueListType()
                                    {
                                        Value = new[] {"RU"}
                                    }
                                },
                                new SlotType1
                                {
                                    name = "serviceStartTime",
                                    ValueList = new ValueListType()
                                    {
                                        Value = new[] {"201310150800"}
                                    }
                                },
                                new SlotType1
                                {
                                    name = "serviceStopTime",
                                    ValueList = new ValueListType()
                                    {
                                        Value = new[] {"201310151200"}
                                    }
                                },
                                new SlotType1
                                {
                                    name = "sourcePatientId",
                                    ValueList = new ValueListType()
                                    {
                                        Value = new[]
                                        {
                                            "{0}^^^&1.2.643.5.1.13.3.25.42.17&ISO"
                                                .AsFormat(patientId.ToNumber().ToString())
                                        }
                                    }
                                },
                                new SlotType1
                                {
                                    name = "legalAuthenticator",
                                    ValueList = new ValueListType()
                                    {
                                        Value = new[] {"14834576709^Сидоров^Виктор^Сергеевич"}
                                    }
                                },
                                new SlotType1
                                {
                                    name = "medicalRecordNumber",
                                    ValueList = new ValueListType()
                                    {
                                        Value = new[] {"MK1400002"}
                                    }
                                },
                            },
                            Name = new InternationalStringType
                            {
                                LocalizedString =
                                    new[]
                                    {new LocalizedStringType() {value = "Выписной эпикриз стационара №18257"}}
                            },
                            Description = new InternationalStringType()
                            {
                                LocalizedString =
                                    new[]
                                    {
                                        new LocalizedStringType()
                                        {
                                            value = "Комментарий к эпикризу стационара  №18257"
                                        }
                                    }
                            },
                            Classification = new[]
                            {
                                new ClassificationType()
                                {
                                    id = "cl01",
                                    classificationScheme =
                                        "urn:uuid:93606bcf-9494-43ec-9b4e-a7748d1a838d",
                                    classifiedObject = "Document01",
                                    nodeRepresentation = "",
                                    Slot = new[]
                                    {
                                        new SlotType1()
                                        {
                                            name = "authorPerson",
                                            ValueList =
                                                new ValueListType()
                                                {
                                                    Value =
                                                        new[]
                                                        {"14834576709^Сидоров^Виктор^Сергеевич"}
                                                }
                                        },
                                        new SlotType1()
                                        {
                                            name = "authorInstitution",
                                            ValueList =
                                                new ValueListType()
                                                {
                                                    Value =
                                                        new[]
                                                        {
                                                            "Государственное бюджетное учреждение здравоохранения Кемеровской области \"Кемеровский областной медицинский информационно-аналитический центр\"^^^^^^^^^1.2.643.5.1.13.3.25.42.17"
                                                        }
                                                }
                                        },
                                        new SlotType1()
                                        {
                                            name = "authorRole",
                                            ValueList =
                                                new ValueListType()
                                                {
                                                    Value = new[] {"Врач-кардиолог"}
                                                }
                                        },
                                    }
                                },
                                new ClassificationType()
                                {
                                    id = "cl02",
                                    classificationScheme =
                                        "urn:uuid:41a5887f-8865-4c09-adf7-e362475b143a",
                                    classifiedObject = "Document01",
                                    nodeRepresentation = "1",
                                    Slot = new[]
                                    {
                                        new SlotType1()
                                        {
                                            name = "codingScheme",
                                            ValueList =
                                                new ValueListType()
                                                {
                                                    Value = new[] {"1.2.643.5.1.13.2.1.1.646"}
                                                }
                                        }
                                    },
                                    Name = new InternationalStringType()
                                    {
                                        LocalizedString =
                                            new[]
                                            {
                                                new LocalizedStringType()
                                                {
                                                    value = "Выписной эпикриз стационара"
                                                }
                                            }
                                    }
                                },
                                new ClassificationType()
                                {
                                    id = "cl03",
                                    classificationScheme =
                                        "urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f",
                                    classifiedObject = "Document01",
                                    nodeRepresentation = "N",
                                    Slot = new[]
                                    {
                                        new SlotType1()
                                        {
                                            name = "codingScheme",
                                            ValueList =
                                                new ValueListType()
                                                {
                                                    Value = new[] {"1.2.643.5.1.13.2.7.1.10"}
                                                }
                                        }
                                    },
                                    Name = new InternationalStringType()
                                    {
                                        LocalizedString =
                                            new[] {new LocalizedStringType() {value = "Нормальный"}}
                                    }
                                },
                                new ClassificationType()
                                {
                                    id = "cl04",
                                    classificationScheme =
                                        "urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f",
                                    classifiedObject = "Document01",
                                    nodeRepresentation = "V",
                                    Slot = new[]
                                    {
                                        new SlotType1()
                                        {
                                            name = "codingScheme",
                                            ValueList =
                                                new ValueListType()
                                                {
                                                    Value = new[] {"2.16.840.1.113883.5.25"}
                                                },
                                        }
                                    },
                                    Name = new InternationalStringType()
                                    {
                                        LocalizedString =
                                            new[]
                                            {new LocalizedStringType() {value = "Ограниченный"}}
                                    }
                                },
                                new ClassificationType()
                                {
                                    id = "cl05",
                                    classificationScheme =
                                        "urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d",
                                    classifiedObject = "Document01",
                                    nodeRepresentation = cdalevels[level],
                                    Slot = new[]
                                    {
                                        new SlotType1()
                                        {
                                            name = "codingScheme",
                                            ValueList =
                                                new ValueListType()
                                                {
                                                    Value = new[] {"1.2.643.5.1.13.2.7.1.40"}
                                                }
                                        }
                                    },
                                    Name = new InternationalStringType()
                                    {
                                        LocalizedString =
                                            new[]
                                            {
                                                new LocalizedStringType()
                                                {
                                                    value = cdalevels[level]
                                                }
                                            }
                                    }
                                },
                                new ClassificationType()
                                {
                                    id = "cl06",
                                    classificationScheme =
                                        "urn:uuid:cccf5598-8b07-4b77-a05e-ae952c785ead",
                                    classifiedObject = "Document01",
                                    nodeRepresentation = "14",
                                    Slot = new[]
                                    {
                                        new SlotType1()
                                        {
                                            name = "codingScheme",
                                            ValueList =
                                                new ValueListType()
                                                {
                                                    Value = new[] {"1.2.643.5.1.13.2.7.1.25"}
                                                }
                                        }
                                    },
                                    Name = new InternationalStringType()
                                    {
                                        LocalizedString =
                                            new[]
                                            {
                                                new LocalizedStringType()
                                                {
                                                    value = "Кардиологическое отделение"
                                                }
                                            }
                                    }
                                },
                                new ClassificationType()
                                {
                                    id = "cl07",
                                    classificationScheme =
                                        "urn:uuid:f0306f51-975f-434e-a61c-c59651d33983",
                                    classifiedObject = "Document01",
                                    nodeRepresentation = "POCD_MT000040_RU01",
                                    Slot = new[]
                                    {
                                        new SlotType1()
                                        {
                                            name = "codingScheme",
                                            ValueList =
                                                new ValueListType()
                                                {
                                                    Value = new[] {"2.16.840.1.113883.1.3"}
                                                }
                                        }
                                    },
                                    Name = new InternationalStringType()
                                    {
                                        LocalizedString =
                                            new[] {new LocalizedStringType() {value = "CDA R2"}}
                                    }
                                },
                                new ClassificationType()
                                {
                                    id = "cl18",
                                    classificationScheme = "urn:uuid:f33fb8ac-18af-42cc-ae0e-ed0b0bdb91e1",
                                    classifiedObject = "Document01",
                                    nodeRepresentation = "99",
                                    Slot = new []
                                    {
                                        new SlotType1()
                                        {
                                            name = "codingScheme",
                                            ValueList = new ValueListType()
                                            {
                                                Value = new []{"1.2.643.5.1.13.2.1.1.462"}
                                            }
                                        }
                                    },
                                    Name = new InternationalStringType()
                                    {
                                        LocalizedString = new []{new LocalizedStringType() {value = "медицинский информационно-аналитический"}}
                                    }
                                },
                            },
                            ExternalIdentifier = new[]
                            {
                                new ExternalIdentifierType
                                {
                                    id = "ei01",
                                    registryObject = "Document01",
                                    identificationScheme =
                                        "urn:uuid:58a6f841-87b3-4a3e-92fd-a8ffeff98427",
                                    value =
                                        "{0}^^^&1.2.643.5.1.13.3.25.42.17&ISO"
                                            .AsFormat(patientId.ToNumber().ToString()),
                                    Name = new InternationalStringType
                                    {
                                        LocalizedString =
                                            new[]
                                            {
                                                new LocalizedStringType()
                                                {
                                                    value = "XDSDocumentEntry.patientId"
                                                }
                                            }
                                    }
                                },
                                new ExternalIdentifierType
                                {
                                    id = "ei02",
                                    registryObject = "Document01",
                                    identificationScheme =
                                        "urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab",
                                    value = "{0}ss39".AsFormat(docId.ToString("D")),
                                    Name = new InternationalStringType
                                    {
                                        LocalizedString =
                                            new[]
                                            {
                                                new LocalizedStringType()
                                                {
                                                    value = "XDSDocumentEntry.uniqueId"
                                                }
                                            }
                                    }
                                }
                            }
                        },
                        new RegistryPackageType
                        {
                            id = "SubmissionSet01",
                            Slot = new[]
                            {
                                new SlotType1()
                                {
                                    name = "submissionTime",
                                    ValueList =
                                        new ValueListType() {Value = new[] {"20131112132550"}}
                                }
                            },
                            Name = new InternationalStringType()
                            {
                                LocalizedString =
                                    new[]
                                    {
                                        new LocalizedStringType()
                                        {
                                            value = "Набор документов - Выписной эпикриз стационара"
                                        }
                                    }
                            },
                            Description = new InternationalStringType()
                            {
                                LocalizedString =
                                    new[]
                                    {
                                        new LocalizedStringType
                                        {
                                            value =
                                                "Выписной эпикриз стационара -  комментарий к набору документов"
                                        }
                                    }
                            },
                            Classification = new[]
                            {
                                new ClassificationType()
                                {
                                    id = "cl08",
                                    classificationScheme =
                                        "urn:uuid:a7058bb9-b4e4-4307-ba5b-e3f0ab85e12d",
                                    classifiedObject = "SubmissionSet01",
                                    nodeRepresentation = "",
                                    Slot = new[]
                                    {
                                        new SlotType1()
                                        {
                                            name = "authorPerson",
                                            ValueList =
                                                new ValueListType()
                                                {
                                                    Value =
                                                        new[] {"11223344500^Петров^Петр^Васильевич"}
                                                }
                                        },
                                        new SlotType1()
                                        {
                                            name = "authorInstitution",
                                            ValueList =
                                                new ValueListType()
                                                {
                                                    Value =
                                                        new[]
                                                        {
                                                            "Государственное бюджетное учреждение здравоохранения Кемеровской области \"Кемеровский областной медицинский информационно-аналитический центр\"^^^^^^^^^1.2.643.5.1.13.3.25.42.17"
                                                        }
                                                }
                                        },
                                        new SlotType1()
                                        {
                                            name = "authorRole",
                                            ValueList =
                                                new ValueListType()
                                                {
                                                    Value = new[] {"Врач-кардиолог"}
                                                }
                                        },
                                        new SlotType1()
                                        {
                                            name = "authorSpecialty",
                                            ValueList =
                                                new ValueListType() {Value = new[] {"Кардиология"}}
                                        }
                                    }
                                }
                            },
                            ExternalIdentifier = new[]
                            {
                                new ExternalIdentifierType()
                                {
                                    id = "ei03",
                                    registryObject = "SubmissionSet01",
                                    identificationScheme =
                                        "urn:uuid:96fdda7c-d067-4183-912e-bf5ee74998a8",
                                    value = "{0}ss39".AsFormat(Guid.NewGuid().ToString("D")),
                                    Name = new InternationalStringType()
                                    {
                                        LocalizedString =
                                            new[]
                                            {
                                                new LocalizedStringType()
                                                {
                                                    value = "XDSSubmissionSet.uniqueId"
                                                }
                                            }
                                    }
                                },
                                new ExternalIdentifierType()
                                {
                                    id = "ei04",
                                    registryObject = "SubmissionSet01",
                                    identificationScheme =
                                        "urn:uuid:554ac39e-e3fe-47fe-b233-965d2a147832",
                                    value = "1.3.6.1.4.1.21367.2005.3.7",
                                    Name = new InternationalStringType()
                                    {
                                        LocalizedString =
                                            new[]
                                            {
                                                new LocalizedStringType()
                                                {
                                                    value = "XDSSubmissionSet.sourceId"
                                                }
                                            }
                                    }
                                },
                                new ExternalIdentifierType()
                                {
                                    id = "ei05",
                                    registryObject = "SubmissionSet01",
                                    identificationScheme =
                                        "urn:uuid:6b5aea1a-874d-4603-a4bc-96a0a7b38446",
                                    value =
                                        "{0}^^^&1.2.643.5.1.13.3.25.42.17&ISO"
                                            .AsFormat(patientId.ToNumber().ToString()),
                                    Name = new InternationalStringType()
                                    {
                                        LocalizedString =
                                            new[]
                                            {
                                                new LocalizedStringType()
                                                {
                                                    value = "XDSSubmissionSet.patientId"
                                                }
                                            }
                                    }
                                }
                            }
                        },
                        new ClassificationType
                        {
                            id = "cl10",
                            classifiedObject = "SubmissionSet01",
                            classificationNode = "urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd"
                        },
                        new AssociationType1()
                        {
                            id = "as01",
                            associationType =
                                "urn:oasis:names:tc:ebxml-regrep:AssociationType:HasMember",
                            sourceObject = "SubmissionSet01",
                            targetObject = "Document01",
                            Slot = new[]
                            {
                                new SlotType1()
                                {
                                    name = "SubmissionSetStatus",
                                    ValueList = new ValueListType() {Value = new[] {"Original"}}
                                }
                            }
                        }
                    }
                },
                Document = new[]
                {
                    new ProvideAndRegisterDocumentSetRequestTypeDocument()
                    {
                        id = "Document01",
                        Value = cdaDoc
                    }
                }
            };

            var response = _iemcXdsConnection.DocumentRepository_ProvideAndRegisterDocumentSetb(
                new DocumentRepository_ProvideAndRegisterDocumentSetbRequest()
                {
                    ProvideAndRegisterDocumentSetRequest = provideAndRegisterDocumentSetRequestType,
                    transportHeader = new Urish.Core.Net.Rosminzdrav.Iemc.Xds.transportHeader()
                    {
                        authInfo = new Urish.Core.Net.Rosminzdrav.Iemc.Xds.authInfo()
                        //{ clientEntityId = "482376cd-19a4-413e-b054-534241f0ad3a" } // test
                        { clientEntityId = "97wvgjzf-rmfb-6gvi-slxg-azq1qnis5rk9" } // production
                    }
                });
        }

        //public string FindCDAFromIemk(Guid patientId)
        //{
        //    var patient = _personRegister.GetById(patientId);
        //    patient.Phones = _phoneDictionary.GetByPerson(patient, new PageData());
        //    patient.Gender = _genderCatalog.GetById(patient.GenderId.Value);
        //    patient.Documents = _documentDirectory.GetByPerson(patient, new PageData());
        //    patient.Documents.ForEach(document =>
        //    {
        //        if (document.DocumentTypeId != null)
        //            document.DocumentType = _documentTypeCatalog.GetById(document.DocumentTypeId.Value);
        //    });

        //    var adhocQueryRequest = new AdhocQueryRequest
        //    {
        //        ResponseOption = new ResponseOptionType
        //        {
        //            returnComposedObjects = true,
        //            returnType = ResponseOptionTypeReturnType.LeafClass
        //        },
        //        AdhocQuery = new Urish.Core.Net.Rosminzdrav.Iemc.Xds.Query.AdhocQueryType
        //        {
        //            id = "urn:uuid:14d4debf-8f97-4251-9a74-a90016b0af0d",
        //            Slot = new[]
        //            {
        //                new Urish.Core.Net.Rosminzdrav.Iemc.Xds.Query.SlotType1
        //                {
        //                    name = "$XDSDocumentEntryPatientId",
        //                    ValueList = new Urish.Core.Net.Rosminzdrav.Iemc.Xds.Query.ValueListType
        //                    {
        //                        Value = new [] { "'{0}^^^&1.2.643.5.1.13.3.25.42.17&ISO'".AsFormat(patientId.ToNumber().ToString()) }
        //                    }
        //                },
        //                new Urish.Core.Net.Rosminzdrav.Iemc.Xds.Query.SlotType1
        //                {
        //                    name = "$XDSDocumentEntryStatus",
        //                    ValueList = new Urish.Core.Net.Rosminzdrav.Iemc.Xds.Query.ValueListType
        //                    {
        //                        Value = new []{ "('urn:oasis:names:tc:ebxml-regrep:StatusType:Approved')" }
        //                    }
        //                },
        //                new Urish.Core.Net.Rosminzdrav.Iemc.Xds.Query.SlotType1
        //                {
        //                    name = "$XDSDocumentEntryCreationTimeFrom",
        //                    ValueList = new Urish.Core.Net.Rosminzdrav.Iemc.Xds.Query.ValueListType
        //                    {
        //                        Value = new []{ "20131112100023" }
        //                    }
        //                },
        //                new Urish.Core.Net.Rosminzdrav.Iemc.Xds.Query.SlotType1
        //                {
        //                    name = "$XDSDocumentEntryCreationTimeTo",
        //                    ValueList = new Urish.Core.Net.Rosminzdrav.Iemc.Xds.Query.ValueListType
        //                    {
        //                        Value = new []{ "20131112194023" }
        //                    }
        //                },
        //            }
        //        }
        //    };

        //    var response = _iemcXdsQueryConnection.DocumentRegistry_RegistryStoredQuery(new DocumentRegistry_RegistryStoredQueryRequest
        //    {
        //        transportHeader = new Urish.Core.Net.Rosminzdrav.Iemc.Xds.Query.transportHeader()
        //        {
        //            authInfo = new Urish.Core.Net.Rosminzdrav.Iemc.Xds.Query.authInfo()
        //            {
        //                clientEntityId = "482376cd-19a4-413e-b054-534241f0ad3a"
        //            }
        //        },
        //        AdhocQueryRequest = adhocQueryRequest
        //    });
        //    return "";
        //    //return (response.AdhocQueryResponse.RegistryObjectList[0]
        //    //    as Urish.Core.Net.Rosminzdrav.Iemc.Xds.Query.ExtrinsicObjectType)
        //    //    .ExternalIdentifier[0].value;
        //}

        //public void ReceiveCDAFromIemk(Guid patientId, string docId)
        //{
        //    var patient = _personRegister.GetById(patientId);
        //    patient.Phones = _phoneDictionary.GetByPerson(patient, new PageData());
        //    patient.Gender = _genderCatalog.GetById(patient.GenderId.Value);
        //    patient.Documents = _documentDirectory.GetByPerson(patient, new PageData());
        //    patient.Documents.ForEach(document =>
        //    {
        //        if (document.DocumentTypeId != null)
        //            document.DocumentType =
        //                _documentTypeCatalog.GetById(document.DocumentTypeId.Value);
        //    });

        //    var response = _iemcXdsConnection.DocumentRepository_RetrieveDocumentSet(new DocumentRepository_RetrieveDocumentSetRequest
        //    {
        //        transportHeader = new Urish.Core.Net.Rosminzdrav.Iemc.Xds.transportHeader()
        //        {
        //            authInfo = new Urish.Core.Net.Rosminzdrav.Iemc.Xds.authInfo() { clientEntityId = "482376cd-19a4-413e-b054-534241f0ad3a" }
        //        },
        //        RetrieveDocumentSetRequest = new[]
        //        {
        //            new DocumentRequestType
        //            {
        //                DocumentUniqueId = docId
        //            }
        //        }
        //    });
        //}

        #region
        //public byte[] CreateCDADoc(byte[] data)
        //{
        //    var doc = new ClinicalDocument();
        //    doc.TypeId.Root = "2.16.840.1.113883.1.3";
        //    doc.TypeId.Extension = "POCD_MT000040_RU01";
        //    doc.TemplateId.Add(
        //        new HL7SDK.Cda.II { Root = "1.2.643.5.1.13.2.7.5.1.1.1" });
        //    doc.Id.Root = "1.2.643.5.1.13.2.7.5.1.1.4";
        //    doc.Id.Extension = Guid.NewGuid().ToString("N");
        //    doc.Code.Code = "1_1";
        //    doc.Code.CodeSystem = "1.2.643.5.1.13.2.1.1.1";
        //    doc.Code.CodeSystemName = "Система электронных медицинских документов";
        //    doc.Code.DisplayName = "Эпикриз лечения из стационара";
        //    doc.Title.Text = "Эпикриз лечения из стационара";
        //    doc.EffectiveTime.Value = "20130617114723";
        //    doc.ConfidentialityCode.Code = "V";
        //    doc.ConfidentialityCode.CodeSystem = "2.16.840.1.113883.5.25";
        //    doc.ConfidentialityCode.CodeSystemName = "Конфиденциальность";
        //    doc.ConfidentialityCode.DisplayName = "Закрыто";
        //    doc.ConfidentialityCode.Translation.Add(
        //        new HL7SDK.Cda.CD
        //        {
        //            Code = "R",
        //            CodeSystem = "1.2.643.5.1.13.2.7.1.9",
        //            CodeSystemName = "Конфиденциальность опекуна",
        //            DisplayName = "Ограниченно"
        //        });
        //    doc.ConfidentialityCode.Translation.Add(
        //        new HL7SDK.Cda.CD
        //        {
        //            Code = "N",
        //            CodeSystem = "1.2.643.5.1.13.2.7.1.10",
        //            CodeSystemName = "Конфиденциальность врача",
        //            DisplayName = "Открыто"
        //        });
        //    doc.LanguageCode.Code = "RU";
        //    doc.SetId.Root = "1.2.643.5.1.13.2.7.6.1.2.3.987654321.1.1";
        //    doc.SetId.Extension = "2013/321";
        //    doc.VersionNumber.Value = 1;
        //    doc.RecordTarget.Add(
        //        new RecordTarget().With(o =>
        //        {
        //            o.TypeCode = "RCT";
        //            o.RealmCode.Add(
        //                new HL7SDK.Cda.CS { Code = "PAT"});
        //            o.PatientRole.ClassCode = "PAT";
        //            o.PatientRole.Id.Add(new HL7SDK.Cda.II
        //            {
        //                Root = "1.2.643.5.1.13.2.7.5.2.2.90006399.1",
        //                Extension = "48905059995"
        //            });
        //            o.PatientRole.Addr.Add(new HL7SDK.Cda.AD().With(a =>
        //            {
        //                a.Use = "H";
        //                a.Items.Add(new HL7SDK.Cda.adxpcountry() {Text = "Российская Федерация"});
        //                a.Items.Add(new HL7SDK.Cda.adxpstate() {Text = "Новосибирская область"});
        //                a.Items.Add(new HL7SDK.Cda.adxpcity() {Text = "Новосибирск"});
        //                a.Items.Add(new HL7SDK.Cda.adxpstreetName() {Text = "Лескова ул."});
        //                a.Items.Add(new HL7SDK.Cda.adxphouseNumber() {Text = "69"});
        //                a.Items.Add(new HL7SDK.Cda.adxpadditionalLocator() {Text = "9"});
        //                a.Items.Add(new HL7SDK.Cda.adxppostalCode() {Text = "630008"});
        //                a.Items.Add(new HL7SDK.Cda.adxpunitID() { Text = "123456789012" });
        //                a.Items.Add(new HL7SDK.Cda.adxpdirection() {Text = "55.022155,82.950660"});
        //            }));
        //            o.PatientRole.Addr.Add(new HL7SDK.Cda.AD().With(a =>
        //            {
        //                a.Use = "HP";
        //                a.Items.Add(new HL7SDK.Cda.adxpcountry() { Text = "Российская Федерация" });
        //                a.Items.Add(new HL7SDK.Cda.adxpstate() { Text = "Новосибирская область" });
        //                a.Items.Add(new HL7SDK.Cda.adxpcounty() { Text = "Калининский район"});
        //                a.Items.Add(new HL7SDK.Cda.adxpcity() { Text = "Пашино" });
        //                a.Items.Add(new HL7SDK.Cda.adxpstreetName() { Text = "Новоуральская ул." });
        //                a.Items.Add(new HL7SDK.Cda.adxphouseNumber() { Text = "11" });
        //                a.Items.Add(new HL7SDK.Cda.adxpadditionalLocator() { Text = "25" });
        //                a.Items.Add(new HL7SDK.Cda.adxppostalCode() { Text = "630900" });
        //            }));
        //            o.PatientRole.Telecom.Add(new HL7SDK.Cda.TEL() { Value = "tel:+79876543210" });
        //            o.PatientRole.Telecom.Add(new HL7SDK.Cda.TEL() {Value = "tel:+79031234567", Use = "МС"});
        //            o.PatientRole.Telecom.Add(new HL7SDK.Cda.TEL() { Value = "mailto:viktor.korneev@mail.ru" });
        //            o.PatientRole.Telecom.Add(new HL7SDK.Cda.TEL() { Value = "fax:+78442171300" });
        //            o.PatientRole.Patient.With(p =>
        //            {
        //                p.ClassCode = "PSN";
        //                p.DeterminerCode = "INSTANCE";
        //                p.Id.Root = "1.2.643.5.1.13.1.7.5.2.2.90006399.1";
        //                p.Id.Extension = "3216";
        //                p.Name.Add(new HL7SDK.Cda.PN().With(n =>
        //                {
        //                    n.Items.Add(new HL7SDK.Cda.enfamily() { Text = "Корнеев"});
        //                    n.Items.Add(new HL7SDK.Cda.engiven() { Text = "Виктор"});
        //                    n.Items.Add(new HL7SDK.Cda.engiven() { Text = "Павлович"});
        //                }));
        //                p.AdministrativeGenderCode.Code = "1";
        //                p.AdministrativeGenderCode.CodeSystem = "1.2.643.5.1.13.2.1.1.156";
        //                p.AdministrativeGenderCode.CodeSystemName = "Классификатор типов пола";
        //                p.AdministrativeGenderCode.DisplayName = "Мужской";
        //                p.BirthTime.Value = "16890505";
        //                p.Guardian.Add(new Guardian().With(g =>
        //                {
        //                    g.ClassCode = "GUARD";
        //                    g.Id.Add(new HL7SDK.Cda.II() 
        //                        { Root = "1.2.643.5.1.13.2.7.5.2.2.90006399.1",
        //                        Extension = "36598"});
        //                    g.Code.Code = "1";
        //                    g.Code.CodeSystem = "1.2.643.5.1.13.2.7.1.15";
        //                    g.Code.CodeSystemName = "Отношение к пациенту";
        //                    g.Code.DisplayName = "Родитель";
        //                    g.AsPerson.Name.Add(new HL7SDK.Cda.PN().With(n => 
        //                        {
        //                            n.Items.Add(new HL7SDK.Cda.enfamily() {Text = "Хунта"});
        //                            n.Items.Add(new HL7SDK.Cda.engiven() {Text = "Кристобаль"});
        //                            n.Items.Add(new HL7SDK.Cda.engiven() {Text = "Хозевич"});
        //                        }));
        //                }));
        //                p.Birthplace.Place.Addr.With(a =>
        //                {
        //                    a.Use = "PHYS";
        //                    a.Items.Add(new HL7SDK.Cda.adxpcountry() { Text = "Российская Федерация"});
        //                    a.Items.Add(new HL7SDK.Cda.adxpstate() {Text = "Волгоградская область"});
        //                    a.Items.Add(new HL7SDK.Cda.adxpcity() {Text = "Урюпинск"});
        //                });
        //            });
        //        }));
        //    doc.Author.Add(new Author()
        //        {
        //            TypeCode = "AUT",
        //            Time = new HL7SDK.Cda.TS() { Value = "20070217114723"},
        //            AssignedAuthor = new AssignedAuthor().With(au =>
        //            {
        //                au.ClassCode = "ASSIGNED";
        //                au.Id.Add(new HL7SDK.Cda.II() { Root = "1.2.643.5.1.13.2.7.5.2.2.90006399.1", Extension = "2341"});
        //                au.Code.Code = "3086";
        //                au.Code.CodeSystem = "1.2.643.5.1.13.2.1.1.89";
        //                au.Code.CodeSystemName = "Классификатор медицинских должностей";
        //                au.Code.DisplayName = "Врач-хирург";
        //                au.Telecom.Add(new HL7SDK.Cda.TEL { Value = "tel:+78442171311" });
        //                au.Telecom.Add(new HL7SDK.Cda.TEL { Value = "mailto:a.privalov@oblhosp.volgograd.ru"});
        //                au.Telecom.Add(new HL7SDK.Cda.TEL { Value = "fax:+78442171300"});
        //                au.AsPerson.Name.Add(new HL7SDK.Cda.PN().With(n => 
        //                    {
        //                        n.Items.Add(new HL7SDK.Cda.enfamily() { Text = "Привалов" });
        //                        n.Items.Add(new HL7SDK.Cda.engiven() { Text = "Александр"});
        //                        n.Items.Add(new HL7SDK.Cda.engiven() {Text = "Иванович"});
        //                    }));
        //                au.RepresentedOrganization.With(org =>
        //                {
        //                    org.ClassCode = "ORG";
        //                    org.DeterminerCode = "INSTANCE";
        //                    org.Id.Add(new HL7SDK.Cda.II()
        //                    {
        //                        Root = "1.2.643.5.1.13.2.1.1.178",
        //                        Extension = "90006399"
        //                    });
        //                    org.Name.Add(new HL7SDK.Cda.ON() { Text = "Волгоградская областная больница"});
        //                    org.Telecom.Add(new HL7SDK.Cda.TEL() { Value = "tel:+78442171300"});
        //                    org.Telecom.Add(new HL7SDK.Cda.TEL() { Value = "mailto:info@oblhosp.volgograd.ru"});
        //                    org.Telecom.Add(new HL7SDK.Cda.TEL() { Value = "fax:+78442171300"});
        //                    org.Addr.Add(new HL7SDK.Cda.AD().With(a =>
        //                    {
        //                        a.Use = "PHYS";
        //                        a.Items.Add(new HL7SDK.Cda.adxpcountry() { Text = "Российская Федерация" });
        //                        a.Items.Add(new HL7SDK.Cda.adxpstate() { Text = "Волгоградская область" });
        //                        a.Items.Add(new HL7SDK.Cda.adxpcity() { Text = "Волгоград" });
        //                        a.Items.Add(new HL7SDK.Cda.adxpstreetName() { Text = "ул. Ангарская" });
        //                        a.Items.Add(new HL7SDK.Cda.adxphouseNumber() { Text = "22" });
        //                        a.Items.Add(new HL7SDK.Cda.adxppostalCode() { Text = "400081" });
        //                    }));
        //                });
        //            })
        //        });
        //    doc.Custodian.TypeCode = "CST";
        //    doc.Custodian.AssignedCustodian.RepresentedCustodianOrganization.With(org =>
        //    {
        //        org.Id.Add(new HL7SDK.Cda.II() { Root = "1.2.643.5.1.13.2.1.1.178", Extension = "90006399" });
        //        org.Name.Text = "Волгоградская областная больница";
        //    });
        //    doc.Participant.Add(new Participant1().With(p =>
        //    {
        //        p.AssociatedEntity.ClassCode = "PRS";
        //        p.AssociatedEntity.Id.Add(new HL7SDK.Cda.II { NullFlavor = "NI" });
        //        p.AssociatedEntity.Code.Code = "11";
        //        p.AssociatedEntity.Code.CodeSystem = "1.2.643.5.1.13.2.7.1.15";
        //        p.AssociatedEntity.Code.CodeSystemName = "Отношение к пациенту";
        //        p.AssociatedEntity.Code.DisplayName = "Другие родственники";
        //        p.AssociatedEntity.Telecom.Add(new HL7SDK.Cda.TEL() { Value = "tel:+74954223456"});
        //        p.AssociatedEntity.AssociatedPerson.Name.Add(new HL7SDK.Cda.PN().With(n =>
        //        {
        //            n.Items.Add(new HL7SDK.Cda.enfamily() { Text = "Савенкова"});
        //            n.Items.Add(new HL7SDK.Cda.engiven() { Text = "Арина"});
        //            n.Items.Add(new HL7SDK.Cda.engiven() { Text = "Сергеевна"});
        //        }));
        //    }));
        //    doc.InFulfillmentOf.Add(new InFulfillmentOf().With(f =>
        //    {
        //        f.Order.ClassCode = "ACT";
        //        f.Order.MoodCode = "RQO";
        //        f.Order.Id.Add(new HL7SDK.Cda.II()
        //        {
        //            Root = "1.2.643.5.1.13.2.7.6.1.2.987654321.1.1",
        //            Extension = "101"
        //        });
        //        f.Order.Code.Code = "5656";
        //        f.Order.Code.CodeSystem = "1.2.643.5.1.13.2.1.1.1";
        //        f.Order.Code.CodeSystemName = "Система электронных медицинских документов";
        //        f.Order.Code.DisplayName = "Направление на госпитализацию";
        //        f.Order.PriorityCode.Code = "4";
        //        f.Order.PriorityCode.CodeSystem = "1.2.643.5.1.13.2.1.1.115";
        //        f.Order.PriorityCode.CodeSystemName = "Классификатор признаков экстренности госпитализации";
        //        f.Order.PriorityCode.DisplayName = "В плановом порядке";
        //    }));
        //    doc.ComponentOf.EncompassingEncounter.With(e =>
        //    {
        //        e.Id.Add(new HL7SDK.Cda.II() { Root = "1.2.643.5.1.13.2.7.6.2.2.90006399.1", Extension = "2013/531"});
        //        e.Code.Code = "STAT";
        //        e.Code.CodeSystem = "1.2.643.5.1.13.2.7.1.1";
        //        e.Code.CodeSystemName = "Вид случая заболевания";
        //        e.Code.DisplayName = "Госпитализация";
        //        e.EffectiveTime.Init(null, 
        //            new HL7SDK.Cda.IVXB_TS() {Value = "20130531081300"},
        //            new HL7SDK.Cda.IVXB_TS() {Value = "20130615232212"});
        //        e.DischargeDispositionCode.Code = "2";
        //        e.DischargeDispositionCode.CodeSystem = "1.2.643.5.1.13.2.1.1.123";
        //        e.DischargeDispositionCode.CodeSystemName = "Классификатор исходов госпитализации";
        //        e.DischargeDispositionCode.DisplayName = "Переведен в другое ЛПУ";
        //        e.EncounterParticipant.Add(new EncounterParticipant().With(ep =>
        //        {
        //            ep.AssignedEntity.Id.Add(new HL7SDK.Cda.II() { Root = "1.2.643.5.1.13.2.7.5.2.2.90006399.1", Extension = "6543"});
        //            ep.AssignedEntity.Code.Code = "2";
        //            ep.AssignedEntity.Code.CodeSystem = "1.2.643.5.1.13.2.7.1.30";
        //            ep.AssignedEntity.Code.CodeSystemName = "Роль в оказании медицинской помощи";
        //            ep.AssignedEntity.Code.DisplayName = "Врач";
        //            ep.AssignedEntity.AssignedPerson.Name.Add(new HL7SDK.Cda.PN().With(p =>
        //            {
        //                p.Items.Add(new HL7SDK.Cda.enfamily() { Text = "Выбегалло"});
        //                p.Items.Add(new HL7SDK.Cda.engiven() { Text = "Амвросий"});
        //                p.Items.Add(new HL7SDK.Cda.engiven() {Text = "Амбруазович"});
        //            }));
        //            ep.AssignedEntity.RepresentedOrganization.Id.Add(new HL7SDK.Cda.II()
        //                { Root = "1.2.643.5.1.13.2.1.1.178", Extension = "1234567"});
        //            ep.AssignedEntity.RepresentedOrganization.ClassCode = "ORG";
        //            ep.AssignedEntity.RepresentedOrganization.Name.Add(new HL7SDK.Cda.ON() { Text = "Очень хорошая ЦРБ"});
        //            ep.AssignedEntity.RepresentedOrganization.StandardIndustryClassCode.With(c =>
        //            {
        //                c.Code = "111052";
        //                c.CodeSystem = "1.2.643.5.1.13.2.1.1.77";
        //                c.CodeSystemName = "Тип медицинской организации";
        //                c.DisplayName = "Центральная районная больница";
        //            });
        //        }));
        //    });
        //    doc.Component.AsNonXMLBody.Text.Representation = BinaryDataEncoding.B64;
        //    doc.Component.AsNonXMLBody.Text.MediaType = "application/pdf";
        //    doc.Component.AsNonXMLBody.Text.Text = Convert.ToBase64String(data);
        //    return new UTF8Encoding().GetBytes(
        //        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + doc.Xml);
        //}

        #endregion
    }
}