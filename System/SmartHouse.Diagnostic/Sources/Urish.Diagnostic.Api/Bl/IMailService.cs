﻿using System.IO;

namespace SmartHouse.Diagnostic.Api.Bl
{
    public interface IMailService
    {
        void StartOrResume();

        void SendMailToQueue(string[] emails, string subject, StringWriter writer);

        void SendMailToQueue(string[] emails, string subject, string body);

        void SendMailToQueue(string[] emails, string subject, string body, byte[] attachment, string fileName);
    }

}
