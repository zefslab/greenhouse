﻿using System.Collections.Generic;

namespace SmartHouse.Diagnostic.Api.Bl
{
    public interface INotificationService
    {
        void SendEmailNotification(string subject, string body, IEnumerable<string> emails);

        void SendSmsNotification(string body, IEnumerable<string> sms);
    }
}