﻿using System.Collections.Generic;

namespace SmartHouse.Diagnostic.Api.Bl
{
    public interface ISmsCenterService
    {
        void SendSMS(IEnumerable<string> phones, string message);
    }
}