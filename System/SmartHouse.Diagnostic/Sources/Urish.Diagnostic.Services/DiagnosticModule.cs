﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Reflection;
using SmartHouse.Core.Api;
using SmartHouse.Core.Api.Entity;
using Urish.Diagnostic.Services.BL;

namespace Urish.Diagnostic
{
    public class DiagnosticModule: BaseModule, IBaseModule
    {
        DbMigrationsConfiguration IBaseModule.MigrationConfiguration
        {
            get { return null; } //Module don`t use Dao layer
        }
        public DbContext DbConnection
        {
            get { return null; } //Module don`t use Dao layer
        }

        /// <summary>
        /// Предоставляет сборки в которых есть классы реализующие зависимости
        /// </summary>
        /// <returns></returns>
        IList<Assembly> IBaseModule.GetAssembliesForUnityContainer()
        {
            return new List<Assembly>
            {
                typeof(LoggingService).Assembly,
               
            };
        }



    }
}
