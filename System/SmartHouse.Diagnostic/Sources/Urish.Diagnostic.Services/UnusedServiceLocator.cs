﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;

namespace SmartHouse.Core
{
    // note Не удалять!!!
    // Класс создан для того, чтобы библиотека Microsoft.Practices.ServiceLocation была использована,
    // и не удалялась из ссылок инструментами Resharper, т.к. она нужна 
    // для корректной загрузки сборок модулей в MEF, зависящих от Unity
    internal class UnusedServiceLocator : ServiceLocatorImplBase
    {
        
        protected override object DoGetInstance(Type serviceType, string key)
        {
            return null;
        }

        protected override IEnumerable<object> DoGetAllInstances(Type serviceType)
        {
            return null;
        }
    }
}
