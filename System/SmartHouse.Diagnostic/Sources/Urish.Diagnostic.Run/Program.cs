﻿using Urish.Core.Api.Utilities.Unity;

namespace Urish.Diagnostic.Run
{
    using System;
    using Microsoft.Practices.Unity;
    using Services.API;

    class Program
    {
        static void Main(string[] args)
        {
            var container = UnityConfigurer.UnityContainerBuilder(new[] {typeof (ILoggingService).Assembly});

            var logger = container.Resolve<ILoggingService>();

            logger.Info<Program>("Application started!");

            try
            {
                throw  new Exception("Test exception");
            }
            catch (Exception ex)
            {
                logger.Error<Program>("Huston we have a problem!", ex);

                logger.Warn<Program>("Some warn message!!!");

                logger.Fatal<ILoggingService>("Some fatal message!!!");
            }

            logger.Info<Program>("Application stoped!");

            Console.ReadKey();
        }
    }
}