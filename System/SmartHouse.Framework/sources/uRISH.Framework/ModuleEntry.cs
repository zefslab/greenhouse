﻿using System.ComponentModel.Composition;
using System.Xml.Serialization;

namespace uRISH.Framework
{
    /// <summary>
    /// Входная точка компонента модульного ядра, загруженного в runtime
    /// </summary>
    [XmlRoot("module", Namespace = ModuleNamespace)]
    public class ModuleEntry
    {
        public const string ModuleNamespace = "urish://module/config";
        /// <summary>
        /// Получает класс с описанием компонента
        /// </summary>
        [XmlElement("info", Namespace = ModuleNamespace)]
        public ModuleInfo Info { get; set; }
        /// <summary>
        /// Получает путь в системе по директории, где находятся ресурсы компонента
        /// </summary>
        public string RootDirectory { get; set; }

        public ModuleState State { get; set; }

        // public ModuleConfiguration Configuration { get; set; }
    }

    public enum ModuleState
    {
        Disabled = 0,
        Enabled = 1,
        EnableRequested = 2,
        DisableRequested = 3
    }
}
