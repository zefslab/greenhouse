﻿using System.ComponentModel.Composition.Hosting;

namespace Urish.Framework
{
    /// <summary>
    /// Основной класс ядра модульного фреймворка
    /// </summary>
    public partial class ApplicationKernel
    {
        /// <summary>
        /// IoC контейнер MEF
        /// </summary>
        private CompositionContainer _container;
        /// <summary>
        /// Класс, представляющий обёртку над контейнером для доступа к его содержимому из других компонентов
        /// </summary>
        private ComponentProvider _componentProvider;
        /// <summary>
        /// Корневой MEF каталог, предоставляющий компоненты для контейнера
        /// </summary>
        private AggregateCatalog _mainCatalog;

        /// <summary>
        /// Управляет модулями, подключенными к ядру, предоставляет к ним доступ для других компонентов
        /// </summary>
        private ModuleManager _moduleManager;

        private ApplicationKernel()
        { }

        /// <summary>
        /// Класс, представляющий обёртку над контейнером для доступа к его содержимому из других компонентов
        /// </summary>
        public IComponentProvider ComponentProvider
        {
            get
            {
                return _componentProvider;
            }
        }

        /// <summary>
        /// Управляет модулями, подключенными к ядру, предоставляет к ним доступ для других компонентов
        /// </summary>
        public ModuleManager ModuleManager
        {
            get { return _moduleManager; }
        }
    }
}
