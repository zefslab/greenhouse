﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using Komiac.Linq;
using Urish.Framework.Composition;
using Urish.Framework.Model;

namespace Urish.Framework
{
    /// <summary>
    /// Менеджер модулей ядра системы. Позволяет получить информацию о всех подключенных 
    /// к ядру модулей, а также управлять состоянием модулей, включать их и отключать.
    /// </summary>
    public class ModuleManager : IModuleManager
    {
        private readonly AggregateCatalog _mainCatalog;
        private readonly IComponentProvider _componentProvider;
        private readonly Dictionary<ModuleEntry, ModuleState> _desiredStates = new Dictionary<ModuleEntry, ModuleState>();

        public ModuleManager(AggregateCatalog mainCatalog, IComponentProvider componentProvider)
        {
            if (mainCatalog == null) throw new ArgumentNullException("mainCatalog");
            if (componentProvider == null) throw new ArgumentNullException("componentProvider");
            _mainCatalog = mainCatalog;
            _componentProvider = componentProvider;
        }

        /// <summary>
        /// 
        /// Возвращает список объектов <see cref="ModuleEntry"/>, по одному на каждый 
        /// подключенный модуль к ядру
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ModuleEntry> GetModules()
        {
            return GetModuleEntries().ToList().AsReadOnly();
        }

        /// <summary>
        /// Устанавливает состояние переданных модулей в <see cref="ModuleState.Enabled"/>. 
        /// Состояние устанавливается отложенно и применяется к модулям после вызова.
        /// <see cref="ApplyChanges"/>
        /// </summary>
        /// <param name="entries"></param>
        public void EnableModules(IEnumerable<ModuleEntry> entries)
        {
            if (entries == null) throw new ArgumentNullException("entries");

            foreach (var entry in entries)
            {
                if(!GetModuleEntries().Contains(entry))
                { throw new Exception(string.Format(
                    "Объект ModuleEntry с id {0} не найден, либо был получен не из текущего менеджера", entry.Info.Id)); }
                SetNewDesireState(entry, ModuleState.Enabled);
            }
        }

        /// Устанавливает состояние переданных модулей в <see cref="ModuleState.Disabled"/>.
        /// Состояние устанавливается отложенно и применяется к модулям после вызова.
        /// <see cref="ApplyChanges"/>
        public void DisableModules(IEnumerable<ModuleEntry> entries)
        {
            if (entries == null) throw new ArgumentNullException("entries");
            foreach (var entry in entries)
            {
                if (!GetModuleEntries().Contains(entry))
                { throw new Exception(string.Format("Объект ModuleEntry с id {0} не найден, либо был получен не из текущего менеджера", entry.Info.Id)); }
                SetNewDesireState(entry, ModuleState.Disabled);
            }
        }

        /// <summary>
        /// Применяет изменения в состояниях модулей и инициирует события изменения 
        /// состояний модулей, которые обрабатываются объектами, наследуемыми от 
        /// <see cref="IModuleEventHandler"/>.
        /// </summary>
        public void ApplyChanges()
        {
            if(_desiredStates.Count == 0)
                return;

            // получить обработчики изменения состояний модулей
            var handlers = _componentProvider.GetAll<IModuleEventHandler>().ToList();
            // сгрупировать модули по состояниям
            var entriesByState = _desiredStates.GroupBy(o => o.Value, o => o.Key).ToDictionary(o=>o.Key, o=>o.ToArray());
            // упорядочить обработчиков в заданном порядке выполнения
            handlers = handlers.OrderBy(o => o.GetMetadataOrDefault().Order).ToList();
            // обработать модули, которые требуется отключить
            var disablingEntriesResults = PerformHandling(GetOrEmpty(entriesByState, ModuleState.Disabled), entry =>
            {
                // бросить событие Disabling, выполнить обработчики
                foreach (var handler in handlers)
                {
                    handler.Disabling(entry);
                }

            }).ToList();
            // проставить новый статус для модулей, которые обработались без ошибок
            foreach (
                var moduleCatalog in
                    disablingEntriesResults.Where(o => o.Error == null)
                        .Select(GetCatalogForEntry)
                        .Where(moduleCatalog => moduleCatalog != null))
            {
                moduleCatalog.SetModuleState(ModuleState.Disabled);
            }
            
            // обработать модули, которые требуется включить
            var enablingEntriesResults = PerformHandling(GetOrEmpty(entriesByState, ModuleState.Enabled), entry =>
            {
                // бросить событие Enabling, выполнить обработчики
                foreach (var handler in handlers)
                {
                    handler.Enabling(entry);
                }
            }).ToList();
            // проставить новый статус для модулей, которые обработались без ошибок
            foreach (
                var moduleCatalog in
                    enablingEntriesResults.Where(o => o.Error == null)
                        .Select(GetCatalogForEntry)
                        .Where(moduleCatalog => moduleCatalog != null))
            {
                moduleCatalog.SetModuleState(ModuleState.Enabled);
            }

            // todo выполнить рекомпозицию контейнера для изменённых модулей


            // выполнить постобработку модулей, которые были отключены
            var disabledEntriesResults = PerformHandling(disablingEntriesResults.Where(o => o.Error == null).Select(o => o.Item), entry =>
            {
                // бросить событие Disabled, выполнить обработчики
                foreach (var handler in handlers)
                {
                    handler.Disabled(entry);
                }
            }).ToList();

            // выполнить постобработку модулей, которые были включены
            var enabledEntriesResults = PerformHandling(enablingEntriesResults.Where(o => o.Error == null).Select(o => o.Item), entry =>
            {
                // бросить событие Enabled, выполнить обработчики
                foreach (var handler in handlers)
                {
                    handler.Enabled(entry);
                }
            }).ToList();

            // todo собрать ошибки обработчиков, которые возникли
        }

        private void SetNewDesireState(ModuleEntry entry, ModuleState newState)
        {
            if (entry.State != newState)
                _desiredStates[entry] = newState;
            else
            {
                _desiredStates.Remove(entry);
            }
        }

        private static IEnumerable<ModuleEntry> GetOrEmpty(IDictionary<ModuleState, ModuleEntry[]> dictionary, ModuleState moduleState)
        {
            return dictionary.ContainsKey(moduleState) ? dictionary[moduleState] : Enumerable.Empty<ModuleEntry>();
        }

        private ModuleCatalog GetCatalogForEntry(Result<ModuleEntry> result)
        {
            return GetModuleCatalogs().SingleOrDefault(catalog => catalog.Entry == result.Item);
        }

        private static IEnumerable<Result<T>> PerformHandling<T>(IEnumerable<T> items, Action<T> handler)
        {
            return items.Select(item =>
            {
                try
                {
                    handler(item);
                    return new Result<T> {Item = item, Error = null};
                }
                catch (Exception exception)
                {
                    return new Result<T> {Item = item, Error = exception};
                }
            }).ToList();
        }

        private class Result<T>
        {
            public T Item { get; set; }
            public Exception Error { get; set; }
        }

        private IEnumerable<ModuleRepositoryCatalog> GetModuleRepos()
        {
            return _mainCatalog.Catalogs.ToTree(GetChilds, catalog => null)
                .DescendantsAndSelf().Select(o => o.Item).OfType<ModuleRepositoryCatalog>();
        }

        private IEnumerable<ModuleCatalog> GetModuleCatalogs()
        {
            return GetModuleRepos().SelectMany(o => o.ModuleCatalogs);
        }

        private IEnumerable<ModuleEntry> GetModuleEntries()
        {
            return GetModuleCatalogs().Select(o => o.Entry);
        }

        private static IEnumerable<ComposablePartCatalog> GetChilds(IEnumerable<ComposablePartDefinition> catalog)
        {
            var aggregateCatalog = catalog as AggregateCatalog;
            return aggregateCatalog != null ? aggregateCatalog.Catalogs : Enumerable.Empty<ComposablePartCatalog>();
        }
    }
}
