﻿using System.Collections.Generic;
using Urish.Framework.Model;

namespace Urish.Framework
{
    public interface IModuleManager
    {
        IEnumerable<ModuleEntry> GetModules();

        void EnableModules(IEnumerable<ModuleEntry> entries);
        void DisableModules(IEnumerable<ModuleEntry> entries);
    }
}
