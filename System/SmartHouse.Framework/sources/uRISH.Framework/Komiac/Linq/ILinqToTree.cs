﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Komiac.Linq
{
    public interface ILinqToTree<out T>
    {
        IEnumerable<ILinqToTree<T>> Children { get; }
        ILinqToTree<T> Parent { get; }
        T Item { get; }
    }

    internal class LinqToTree<T> : ILinqToTree<T>
        where T : class 
    {
        private readonly T _item;
        private readonly Func<T, IEnumerable<T>> _childSelector;
        private readonly Func<T, T> _parentSelector;

        public LinqToTree(T item, Func<T, IEnumerable<T>> childSelector, Func<T, T> parentSelector)
        {
            if (item == null) throw new ArgumentNullException("item");
            if (childSelector == null) throw new ArgumentNullException("childSelector");
            if (parentSelector == null) throw new ArgumentNullException("parentSelector");

            _item = item;
            _childSelector = childSelector;
            _parentSelector = parentSelector;
        }

        public IEnumerable<ILinqToTree<T>> Children
        {
            get
            {
                return _childSelector(_item)
                    .Select(o => new LinqToTree<T>(o, _childSelector, _parentSelector));
            }
        }

        public ILinqToTree<T> Parent {
            get
            {
                return new LinqToTree<T>(_parentSelector(_item), _childSelector, _parentSelector);
            }
        }
        public T Item {
            get { return _item; }
        }
    }
}
