﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Urish.Framework.Komiac.Linq
{
    public static class CollectionsExtensions
    {
        /// <summary>
        /// Возвращает новый словарь полученный путем дополнения содержимого словаря <paramref name="to"/>
        /// элементами словаря <paramref name="from"/>. Словарь <paramref name="to"/> дополняется элементами из 
        /// словаря <paramref name="from"/>, причём при совпадении ключей двух значений, результирующее значение
        /// берётся из словаря <paramref name="from"/>
        /// </summary>
        /// <typeparam name="TKey">Тип ключей в словаре</typeparam>
        /// <typeparam name="TValue">Тип значений в словаре</typeparam>
        /// <param name="to">Словарь, который дополняется значениями другого</param>
        /// <param name="from">Словарь, значения которого дополняют первый</param>
        /// <returns></returns>
        public static IDictionary<TKey, TValue> Append<TKey, TValue>(this IDictionary<TKey, TValue> to,
            IDictionary<TKey, TValue> @from)
        {
            return Append(to, @from, null);
        }

        public static IDictionary<TKey, TValue> Append<TKey, TValue>(this IDictionary<TKey, TValue> to,
            IDictionary<TKey, TValue> @from, IEqualityComparer<TKey> keyComparer)
        {
            if (to == null) throw new ArgumentNullException("to");
            if (@from == null) throw new ArgumentNullException("from");
            return to.Keys.Union(@from.Keys, keyComparer).Select(
                o => @from.Keys.Contains(o, keyComparer)
                    ? new KeyValuePair<TKey, TValue>(o, @from[o])
                    : new KeyValuePair<TKey, TValue>(o, to[o]))
                .ToDictionary(o => o.Key, o => o.Value);
        }

        public static IEnumerable<T> EmptyIfNull<T>(this IEnumerable<T> coll)
        {
            return coll ?? Enumerable.Empty<T>();
        }
    }
}
