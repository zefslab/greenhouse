﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Komiac.Linq
{
    /// <summary>
    /// Defines extension methods for querying an ILinqTree
    /// </summary>
    public static class LinqToTreeExtensions
    {
        #region primary Linq methods

        /// <summary>
        /// Returns a collection of descendant elements.
        /// </summary>
        public static IEnumerable<ILinqToTree<T>> Descendants<T>(this ILinqToTree<T> adapter)
        {
            foreach (var child in adapter.Children)
            {
                yield return child;

                foreach (var grandChild in child.Descendants())
                {
                    yield return grandChild;
                }
            }
        }

        /// <summary>
        /// Returns a collection of ancestor elements.
        /// </summary>
        public static IEnumerable<ILinqToTree<T>> Ancestors<T>(this ILinqToTree<T> adapter)
        {
            var parent = adapter.Parent;
            while (parent != null)
            {
                yield return parent;
                parent = parent.Parent;
            }
        }

        /// <summary>
        /// Returns a collection of child elements.
        /// </summary>
        public static IEnumerable<ILinqToTree<T>> Elements<T>(this ILinqToTree<T> adapter)
        {
            foreach (var child in adapter.Children)
            {
                yield return child;
            }
        }

        #endregion

        #region 'AndSelf' implementations

        /// <summary>
        /// Returns a collection containing this element and all child elements.
        /// </summary>
        public static IEnumerable<ILinqToTree<T>> ElementsAndSelf<T>(this ILinqToTree<T> adapter)
        {
            yield return adapter;

            foreach (var child in adapter.Elements())
            {
                yield return child;
            }
        }

        /// <summary>
        /// Returns a collection of ancestor elements.
        /// </summary>
        public static IEnumerable<ILinqToTree<T>> AncestorsAndSelf<T>(this ILinqToTree<T> adapter)
        {

            yield return adapter;

            foreach (var child in adapter.Ancestors())
            {
                yield return child;
            }
        }

        /// <summary>
        /// Returns a collection containing this element and all descendant elements.
        /// </summary>
        public static IEnumerable<ILinqToTree<T>> DescendantsAndSelf<T>(this ILinqToTree<T> adapter)
        {
            yield return adapter;

            foreach (var child in adapter.Descendants())
            {
                yield return child;
            }
        }

        #endregion

        #region Method which take type parameters

        /// <summary>
        /// Returns a collection of descendant elements.
        /// </summary>
        public static IEnumerable<ILinqToTree<T>> Descendants<T, K>(this ILinqToTree<T> adapter)
        {
            return adapter.Descendants().Where(i => i.Item is K);
        }

        #endregion
    }

    public static class LinqToTreeEnumerableExtensions
    {
        /// <summary>
        /// Applies the given function to each of the items in the supplied
        /// IEnumerable.
        /// </summary>
        private static IEnumerable<ILinqToTree<T>> DrillDown<T>(
            this IEnumerable<ILinqToTree<T>> items,
            Func<ILinqToTree<T>, IEnumerable<ILinqToTree<T>>> function)
        {
            return items.SelectMany(function);
        }

        /// <summary>
        /// Returns a collection of descendant elements.
        /// </summary>
        public static IEnumerable<ILinqToTree<T>> Descendants<T>(
            this IEnumerable<ILinqToTree<T>> items)
        {
            if (items == null) throw new ArgumentNullException("items");

            return items.DrillDown(i => i.Descendants());
        }

        /// <summary>
        /// Returns a collection containing this element and all descendant elements.
        /// </summary>
        public static IEnumerable<ILinqToTree<T>> DescendantsAndSelf<T>(
            this IEnumerable<ILinqToTree<T>> items)
        {
            if (items == null) throw new ArgumentNullException("items");
            return items.DrillDown(i => i.DescendantsAndSelf());
        }

        /// <summary>
        /// Returns a collection of ancestor elements.
        /// </summary>
        public static IEnumerable<ILinqToTree<T>> Ancestors<T>(
            this IEnumerable<ILinqToTree<T>> items)
        {
            if (items == null) throw new ArgumentNullException("items");
            return items.DrillDown(i => i.Ancestors());
        }

        /// <summary>
        /// Returns a collection containing this element and all ancestor elements.
        /// </summary>
        public static IEnumerable<ILinqToTree<T>> AncestorsAndSelf<T>(
            this IEnumerable<ILinqToTree<T>> items)
        {
            if (items == null) throw new ArgumentNullException("items");
            return items.DrillDown(i => i.AncestorsAndSelf());
        }

        /// <summary>
        /// Returns a collection of child elements.
        /// </summary>
        public static IEnumerable<ILinqToTree<T>> Elements<T>(
            this IEnumerable<ILinqToTree<T>> items)
        {
            if (items == null) throw new ArgumentNullException("items");
            return items.DrillDown(i => i.Elements());
        }

        /// <summary>
        /// Returns a collection containing this element and all child elements.
        /// </summary>
        public static IEnumerable<ILinqToTree<T>> ElementsAndSelf<T>(
            this IEnumerable<ILinqToTree<T>> items)
        {
            if (items == null) throw new ArgumentNullException("items");
            return items.DrillDown(i => i.ElementsAndSelf());
        }

        public static IEnumerable<ILinqToTree<TElement>> ToTree<TElement>(this IEnumerable<TElement> items, Func<TElement,IEnumerable<TElement>> childSelector, Func<TElement, TElement> parentSelector)
            where TElement : class 
        {
            if (items == null) throw new ArgumentNullException("items");
            if (childSelector == null) throw new ArgumentNullException("childSelector");
            if (parentSelector == null) throw new ArgumentNullException("parentSelector");
            return items.Select(o => new LinqToTree<TElement>(o, childSelector, parentSelector));
        }
    }
}
