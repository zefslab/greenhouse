﻿using System;
using System.Linq;

namespace Komiac.Extensions
{
    public static class CommonExtensions
    {
        public static bool In<T>(this T obj, params T[] coll)
        {
            if (Equals(obj, null))
                throw new NullReferenceException();
            if (coll == null)
                throw new ArgumentNullException("coll");

            return coll.Contains(obj);
        }
    }
}
