﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Routing;

namespace Urish.Framework.Komiac.Extensions
{
    public static class HttpRouteCollectionExtensions
    {
        public static IDictionary<string, IHttpRoute> ToRouteDictionary(this HttpRouteCollection routeCollection)
        {
            if (routeCollection == null) 
                throw new ArgumentNullException("routeCollection");
            var routes = new KeyValuePair<string, IHttpRoute>[routeCollection.Count];
            routeCollection.CopyTo(routes, 0); // todo не разрешает выполнять эту операцию в GlobalConfiguration.Configuration.Routes
            return routes.ToDictionary(k => k.Key, v => v.Value);
        }
    }
}
