﻿namespace Urish.Framework
{
    /// <summary>
    /// Интерфейс, реализуемый компонентами, для которых предусмотрена предварительная инициализация в ядре 
    /// перед работой с ними
    /// </summary>
    public interface IStartupInitializable
    {
        /// <summary>
        /// Вызывается ядром сразу после его создания
        /// Порядок его вызова для модулей не регламентирован
        /// </summary>
        void Initialize();
    }
}
