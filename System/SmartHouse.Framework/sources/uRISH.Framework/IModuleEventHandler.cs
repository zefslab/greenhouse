﻿using System;
using System.Reflection;
using Urish.Framework.Model;

namespace Urish.Framework
{
    public interface IModuleEventHandler
    {
        void Enabled(ModuleEntry entry);
        void Enabling(ModuleEntry entry);
        void Disabled(ModuleEntry entry);
        void Disabling(ModuleEntry entry);
    }

    public abstract class ModuleEventHandler : IModuleEventHandler
    {
        public virtual void Enabled(ModuleEntry entry)
        {
        }

        public virtual void Enabling(ModuleEntry entry)
        {
        }

        public virtual void Disabled(ModuleEntry entry)
        {
        }

        public virtual void Disabling(ModuleEntry entry)
        {
        }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class ModuleEventHandlerMetadataAttribute : Attribute
    {
        public ModuleEventHandlerMetadataAttribute()
        {
            Order = int.MaxValue;
            Destination = ModuleEventHandlerDestination.Any;
        }
        public int Order { get; set; }

        public ModuleEventHandlerDestination Destination { get; set; }
    }

    public enum ModuleEventHandlerDestination
    {
        Own = 1,
        Another = 2,
        Any = 3
    }

    public static class ModuleEventHandlerExtensions
    {
        public static ModuleEventHandlerMetadataAttribute GetMetadataOrDefault(this IModuleEventHandler handler)
        {
            if (handler == null) throw new ArgumentNullException("handler");
            return handler.GetType().GetCustomAttribute<ModuleEventHandlerMetadataAttribute>()
                   ?? new ModuleEventHandlerMetadataAttribute();
        }
    }
}

// todo нужно выделить два типа обработчиков,
// * обработчик собственного модуля
// * обработчик других модулей
// * обработчик любых модулей
// может стоит ввести параметр перечисления
// также ввести свойство приоритет в обработчики, чтобы задавать их
// очерёдность в пределах одного типа

// * если нужно, чтобы один обработчик выполнился до другого, либо после, то нужно 
// либо вешать его на предшествующее событие (Enabling выполняется всегда перед Enabled), 
// либо для обработчиков одного события ставить приоритетные числа в порядке нужной 
// очередности их выполнения в пределах модуля
// очерёдность выполнения между модулями регламентируется из зависимотями, те, от которых 
// модуль зависит выполняются раньше, чем собственные