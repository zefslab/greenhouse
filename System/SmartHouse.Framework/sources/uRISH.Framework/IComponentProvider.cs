﻿using System;
using System.Collections.Generic;

namespace Urish.Framework
{
    public interface IComponentProvider
    {
        T GetOne<T>();

        T GetOne<T>(string contractName);

        object GetOne(Type componentType);

        object GetOne(Type componentType, string contractName);

        T GetOneOrDefault<T>();

        T GetOneOrDefault<T>(string contractName);

        object GetOneOrDefault(Type componentType);

        object GetOneOrDefault(Type componentType, string contractName);

        IEnumerable<T> GetAll<T>();

        IEnumerable<T> GetAll<T>(string contractName);

        IEnumerable<object> GetAll(Type componentType);

        IEnumerable<object> GetAll(Type componentType, string contractName);

        Lazy<T> RequestOne<T>();

        Lazy<T> RequestOne<T>(string contractName);

        Lazy<object> RequestOne(Type componentType);

        Lazy<object> RequestOne(Type componentType, string contractName);

        IEnumerable<Lazy<T>> RequestAll<T>();

        IEnumerable<Lazy<T>> RequestAll<T>(string contractName);

        IEnumerable<Lazy<object>> RequestAll(Type componentType);

        IEnumerable<Lazy<object>> RequestAll(Type componentType, string contractName);

        Type FindTypeForContract(Type contractType);
        Type FindTypeForContract(string contractName);

        IEnumerable<Type> FindTypesForContract(Type contractType);
        IEnumerable<Type> FindTypesForContract(string contractName);
    }
}
