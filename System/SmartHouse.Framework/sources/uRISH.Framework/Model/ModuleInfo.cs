﻿using System;
using System.Xml.Serialization;

namespace Urish.Framework.Model
{
    /// <summary>
    /// Класс описания модуля ядра, полученная из конфигурационного файла module.config.
    /// </summary>
    [XmlRoot("info", Namespace = ModuleEntry.ModuleNamespace)]
    public class ModuleInfo
    {
        /// <summary>
        /// Уникальный идентификатор модуля.
        /// </summary>
        [XmlElement("id", Namespace = ModuleEntry.ModuleNamespace)]
        public Guid Id { get; set; }

        /// <summary>
        /// Дружественное наименование модуля.
        /// </summary>
        [XmlElement("name", Namespace = ModuleEntry.ModuleNamespace)]
        public string Name { get; set; }

        /// <summary>
        /// Развёрнутое описание модуля.
        /// </summary>
        [XmlElement("description", Namespace = ModuleEntry.ModuleNamespace)]
        public string Description { get; set; }

        [XmlElement("version", Namespace = ModuleEntry.ModuleNamespace)]
        public string _Version { get; set; }
        /// <summary>
        /// Версия модуля.
        /// </summary>
        [XmlIgnore]
        public ModuleVersion Version {
            get {return new ModuleVersion(_Version); }
        }

        /// <summary>
        /// Имя группы, к которой относится модуля.
        /// </summary>
        [XmlElement("groupName", Namespace = ModuleEntry.ModuleNamespace)]
        public string GroupName { get; set; }

        /// <summary>
        /// Данные автора (правообладателя) модуля.
        /// </summary>
        [XmlElement("author", Namespace = ModuleEntry.ModuleNamespace)]
        public ModuleAuthor Author { get; set; }
    }
}
