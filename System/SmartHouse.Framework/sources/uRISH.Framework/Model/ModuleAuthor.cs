﻿using System.Xml.Serialization;

namespace Urish.Framework.Model
{
    /// <summary>
    /// Данные об авторе (правообладателя) модуля.
    /// </summary>
    [XmlRoot("author", Namespace = ModuleEntry.ModuleNamespace)]
    public class ModuleAuthor
    {
        /// <summary>
        /// Имя автора (правообладателя), его ФИО, либо другое наименование.
        /// </summary>
        [XmlElement("name", Namespace = ModuleEntry.ModuleNamespace)]
        public string Name { get; set; }
        /// <summary>
        /// Наименование компании автора (правообладателя).
        /// </summary>
        [XmlElement("company", Namespace = ModuleEntry.ModuleNamespace)]
        public string Company { get; set; }
        /// <summary>
        /// Строка данных об авторских правах автора (правообладателя)
        /// </summary>
        [XmlElement("copyright", Namespace = ModuleEntry.ModuleNamespace)]
        public string Copyright { get; set; }
        /// <summary>
        /// Строка данных с контактами автора (правообладателя)
        /// </summary>
        [XmlElement("contacts", Namespace = ModuleEntry.ModuleNamespace)]
        public string Contacts { get; set; }
    }
}
