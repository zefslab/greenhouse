﻿using System.Xml.Serialization;

namespace Urish.Framework.Model
{
    [XmlRoot("assembly", Namespace = ModuleEntry.ModuleNamespace)]
    public class ModuleAssembly
    {
        [XmlAttribute("name")]
        public string Name { get; set; }
    }
}
