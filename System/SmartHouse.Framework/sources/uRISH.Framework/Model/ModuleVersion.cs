﻿using System;
using System.Globalization;

namespace Urish.Framework
{
    /// <summary>
    /// Сериализуемый аналог класса System.Version.
    /// </summary>
    [Serializable]
    public class ModuleVersion : ICloneable, IComparable
    {
        private int _major;
        private int _minor;
        private int _build;
        private int _revision;
        /// <summary>
        /// Получает основной номер версии.
        /// </summary>
        /// <value></value>
        public int Major
        {
            get
            {
                return _major;
            }
            set
            {
                _major = value;
            }
        }
        /// <summary>
        /// Получает дополнительный номер версии.
        /// </summary>
        /// <value></value>
        public int Minor
        {
            get
            {
                return _minor;
            }
            set
            {
                _minor = value;
            }
        }
        /// <summary>
        /// Получает номер построения версии, либо -1, если номер построения не определён.
        /// </summary>
        /// <value></value>
        public int Build
        {
            get
            {
                return _build;
            }
            set
            {
                _build = value;
            }
        }
        /// <summary>
        /// Получает номер редакции версии, либо -1, если номер редакции не определён.
        /// </summary>
        /// <value></value>
        public int Revision
        {
            get
            {
                return _revision;
            }
            set
            {
                _revision = value;
            }
        }
        /// <summary>
        /// Создаёт новый объект <see cref="ModuleVersion"/>.
        /// </summary>
        public ModuleVersion()
        {
            _build = -1;
            _revision = -1;
            _major = 0;
            _minor = 0;
        }
        /// <summary>
        /// Создаёт новый объект <see cref="ModuleVersion"/>.
        /// </summary>
        /// <param name="version">Version.</param>
        public ModuleVersion(string version)
        {
            _build = -1;
            _revision = -1;
            if (version == null)
            {
                throw new ArgumentNullException("version");
            }
            var chArray1 = new[] { '.' };
            var textArray1 = version.Split(chArray1);
            var num1 = textArray1.Length;
            if ((num1 < 2) || (num1 > 4))
            {
                throw new ArgumentException("Arg_VersionString");
            }
            _major = int.Parse(textArray1[0], CultureInfo.InvariantCulture);
            if (_major < 0)
            {
                throw new ArgumentOutOfRangeException("version", "ArgumentOutOfRange_Version");
            }
            _minor = int.Parse(textArray1[1], CultureInfo.InvariantCulture);
            if (_minor < 0)
            {
                throw new ArgumentOutOfRangeException("version", "ArgumentOutOfRange_Version");
            }
            num1 -= 2;
            if (num1 > 0)
            {
                _build = int.Parse(textArray1[2], CultureInfo.InvariantCulture);
                if (_build < 0)
                {
                    throw new ArgumentOutOfRangeException("build", "ArgumentOutOfRange_Version");
                }
                num1--;
                if (num1 > 0)
                {
                    _revision = int.Parse(textArray1[3], CultureInfo.InvariantCulture);
                    if (_revision < 0)
                    {
                        throw new ArgumentOutOfRangeException("revision", "ArgumentOutOfRange_Version");
                    }
                }
            }
        }
        /// <summary>
        /// Создаёт новый объект <see cref="ModuleVersion"/>.
        /// </summary>
        /// <param name="major">Основной номер версии.</param>
        /// <param name="minor">Дополнительный номер версии.</param>
        public ModuleVersion(int major, int minor)
        {
            _build = -1;
            _revision = -1;
            if (major < 0)
            {
                throw new ArgumentOutOfRangeException("major", "ArgumentOutOfRange_Version");
            }
            if (minor < 0)
            {
                throw new ArgumentOutOfRangeException("minor", "ArgumentOutOfRange_Version");
            }
            _major = major;
            _minor = minor;
            _major = major;
        }
        /// <summary>
        /// Создаёт новый объект <see cref="ModuleVersion"/>.
        /// </summary>
        /// <param name="major">Основной номер версии.</param>
        /// <param name="minor">Дополнительный номер версии.</param>
        /// <param name="build">Номер построения версии.</param>
        public ModuleVersion(int major, int minor, int build)
        {
            _build = -1;
            _revision = -1;
            if (major < 0)
            {
                throw new ArgumentOutOfRangeException("major", "ArgumentOutOfRange_Version");
            }
            if (minor < 0)
            {
                throw new ArgumentOutOfRangeException("minor", "ArgumentOutOfRange_Version");
            }
            if (build < 0)
            {
                throw new ArgumentOutOfRangeException("build", "ArgumentOutOfRange_Version");
            }
            _major = major;
            _minor = minor;
            _build = build;
        }
        /// <summary>
        /// Создаёт новый объект <see cref="ModuleVersion"/>.
        /// </summary>
        /// <param name="major">Основной номер версии.</param>
        /// <param name="minor">Дополнительный номер версии.</param>
        /// <param name="build">Номер построения версии.</param>
        /// <param name="revision">Номер редакции версии.</param>
        public ModuleVersion(int major, int minor, int build, int revision)
        {
            _build = -1;
            _revision = -1;
            if (major < 0)
            {
                throw new ArgumentOutOfRangeException("major", "ArgumentOutOfRange_Version");
            }
            if (minor < 0)
            {
                throw new ArgumentOutOfRangeException("minor", "ArgumentOutOfRange_Version");
            }
            if (build < 0)
            {
                throw new ArgumentOutOfRangeException("build", "ArgumentOutOfRange_Version");
            }
            if (revision < 0)
            {
                throw new ArgumentOutOfRangeException("revision", "ArgumentOutOfRange_Version");
            }
            _major = major;
            _minor = minor;
            _build = build;
            _revision = revision;
        }
        #region ICloneable Members
        /// <summary>
        /// Клонирует текущий объект.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            var version1 = new ModuleVersion
            {
                _major = _major,
                _minor = _minor,
                _build = _build,
                _revision = _revision
            };
            return version1;
        }
        #endregion
        #region IComparable Members

        /// <summary>
        /// Сравнивает текущий объект с другим.
        /// </summary>
        /// <param name="version">Версия модуля</param>
        /// <returns></returns>
        public int CompareTo(object version)
        {
            if (version == null)
            {
                return 1;
            }
            if (!(version is ModuleVersion))
            {
                throw new ArgumentException("Arg_MustBeVersion");
            }
            var version1 = (ModuleVersion)version;
            if (_major != version1.Major)
            {
                if (_major > version1.Major)
                {
                    return 1;
                }
                return -1;
            }
            if (_minor != version1.Minor)
            {
                if (_minor > version1.Minor)
                {
                    return 1;
                }
                return -1;
            }
            if (_build != version1.Build)
            {
                if (_build > version1.Build)
                {
                    return 1;
                }
                return -1;
            }
            if (_revision == version1.Revision)
            {
                return 0;
            }
            if (_revision > version1.Revision)
            {
                return 1;
            }
            return -1;
        }
        #endregion
        /// <summary>
        /// Проверяет текущий объект версии с другим на равенство.
        /// </summary>
        /// <param name="obj">Версия модуля.</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (!(obj is ModuleVersion))
            {
                return false;
            }
            var version1 = (ModuleVersion)obj;
            return ((_major == version1.Major) && (_minor == version1.Minor)) &&
                   (_build == version1.Build) && (_revision == version1.Revision);
        }
        /// <summary>
        /// Получает хеш-код объекта.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            var num1 = 0;
            num1 |= ((_major & 15) << 0x1c);
            num1 |= ((_minor & 0xff) << 20);
            num1 |= ((_build & 0xff) << 12);
            return (num1 | _revision & 0xfff);
        }
        /// <summary>
        /// Оператор ==s с заданным v1.
        /// </summary>
        /// <param name="v1">V1.</param>
        /// <param name="v2">V2.</param>
        /// <returns></returns>
        public static bool operator ==(ModuleVersion v1, ModuleVersion v2)
        {
            return v1 != null && v1.Equals(v2);
        }
        /// <summary>
        /// Оператор &gt;s с заданным v1.
        /// </summary>
        /// <param name="v1">V1.</param>
        /// <param name="v2">V2.</param>
        /// <returns></returns>
        public static bool operator >(ModuleVersion v1, ModuleVersion v2)
        {
            return (v2 < v1);
        }
        /// <summary>
        /// Оператор &gt;=s с заданным v1.
        /// </summary>
        /// <param name="v1">V1.</param>
        /// <param name="v2">V2.</param>
        /// <returns></returns>
        public static bool operator >=(ModuleVersion v1, ModuleVersion v2)
        {
            return (v2 <= v1);
        }
        /// <summary>
        /// Оператор !=s с заданным v1.
        /// </summary>
        /// <param name="v1">V1.</param>
        /// <param name="v2">V2.</param>
        /// <returns></returns>
        public static bool operator !=(ModuleVersion v1, ModuleVersion v2)
        {
            return (v1 != v2);
        }
        /// <summary>
        /// Оператор &lt;s с заданным v1.
        /// </summary>
        /// <param name="v1">V1.</param>
        /// <param name="v2">V2.</param>
        /// <returns></returns>
        public static bool operator <(ModuleVersion v1, ModuleVersion v2)
        {
            if (v1 == null)
            {
                throw new ArgumentNullException("v1");
            }
            return (v1.CompareTo(v2) < 0);
        }
        /// <summary>
        /// Оператор &lt;=s с заданным v1.
        /// </summary>
        /// <param name="v1">V1.</param>
        /// <param name="v2">V2.</param>
        /// <returns></returns>
        public static bool operator <=(ModuleVersion v1, ModuleVersion v2)
        {
            if (v1 == null)
            {
                throw new ArgumentNullException("v1");
            }
            return (v1.CompareTo(v2) <= 0);
        }
        /// <summary>
        /// Возвращает строкое представление версии.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (_build == -1)
            {
                return ToString(2);
            }
            if (_revision == -1)
            {
                return ToString(3);
            }
            return ToString(4);
        }
        /// <summary>
        /// Возвращает строковое представление версии.
        /// </summary>
        /// <param name="fieldCount">Число полей.</param>
        /// <returns></returns>
        public string ToString(int fieldCount)
        {
            object[] objArray1;
            switch (fieldCount)
            {
                case 0:
                    {
                        return string.Empty;
                    }
                case 1:
                    {
                        return (_major.ToString(CultureInfo.InvariantCulture));
                    }
                case 2:
                    {
                        return (_major + "." + _minor);
                    }
            }
            if (_build == -1)
            {
                throw new ArgumentException(string.Format("ArgumentOutOfRange_Bounds_Lower_Upper {0},{1}", "0", "2"), "fieldCount");
            }
            if (fieldCount == 3)
            {
                objArray1 = new object[] { _major, ".", _minor, ".", _build };
                return string.Concat(objArray1);
            }
            if (_revision == -1)
            {
                throw new ArgumentException(string.Format("ArgumentOutOfRange_Bounds_Lower_Upper {0},{1}", "0", "3"), "fieldCount");
            }
            if (fieldCount == 4)
            {
                objArray1 = new object[] { _major, ".", _minor, ".", _build, ".", _revision };
                return string.Concat(objArray1);
            }
            throw new ArgumentException(string.Format("ArgumentOutOfRange_Bounds_Lower_Upper {0},{1}", "0", "4"), "fieldCount");
        }
    }
}
