﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Urish.Framework.Model
{
    /// <summary>
    /// Содержит информацию о подключенному в систему модулю.
    /// </summary>
    [XmlRoot("module", Namespace = ModuleNamespace)]
    public class ModuleEntry
    {
        public const string ModuleNamespace = "urish://module/config";
        /// <summary>
        /// Получает класс с описанием компонента.
        /// </summary>
        [XmlElement("info", Namespace = ModuleNamespace)]
        public virtual ModuleInfo Info { get; set; }
        /// <summary>
        /// Возвращает путь к основной директории модуля, где он расположен.
        /// </summary>
        [XmlIgnore]
        public virtual string RootDirectory { get; set; }

        /// <summary>
        /// Возвращает текущее состояние модуля в системе.
        /// </summary>
        [XmlIgnore]
        public virtual ModuleState State { get; protected set; }

        [XmlArray("exportedAssemblies", Namespace = ModuleNamespace)]
        [XmlArrayItem("assembly", Namespace = ModuleNamespace)]
        public virtual List<ModuleAssembly> ExportedAssemblies { get; set; }

        // public ModuleConfiguration Configuration { get; set; }
    }

    /// <summary>
    /// Состояния модулей.
    /// </summary>
    public enum ModuleState
    {
        /// <summary>
        /// Модуль отключён и его компоненты недоступны.
        /// </summary>
        Disabled = 0,
        /// <summary>
        /// Модуль подключён и его компоненты доступны.
        /// </summary>
        Enabled = 1
    }
}
