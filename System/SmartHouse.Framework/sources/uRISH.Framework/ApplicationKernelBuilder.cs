﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.ComponentModel.Composition.Registration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Urish.Framework.Composition;

namespace Urish.Framework
{

    /// <summary>
    /// Внутренний класс билдера модульного ядра, управляющий его первичных созданием. Ядро создаётся только с его помощью.
    /// </summary>
    public partial class ApplicationKernel
    {
        public class Builder
        {
            private readonly HashSet<Type> _registeredTypes = new HashSet<Type>();
            private readonly HashSet<Assembly> _registeredAssemblies = new HashSet<Assembly>();
            private readonly HashSet<string> _registeredPaths = new HashSet<string>();
            private readonly CompositionBatch _registeredInstances = new CompositionBatch();

            private readonly ICollection<Action<RegistrationBuilder>> _conventions =
                new Collection<Action<RegistrationBuilder>>();

            /// <summary>
            /// Строит мольное ядро с MEF контейнером на основе собранной информации, отслеживая корректность его построения
            /// </summary>
            /// <returns></returns>
            public ApplicationKernel Build()
            {
                var kernel = new ApplicationKernel 
                { _mainCatalog = new AggregateCatalog() };

                // сбор настроек механизма регистрации для обнаружения типов каталогами
                var registrationBuilder = new RegistrationBuilder();
                foreach (var action in _conventions)
                {
                    action(registrationBuilder);
                }

                // регистрация компонентов
                // обязательные стандартные компоненты
                kernel._mainCatalog.Catalogs.Add(new AssemblyCatalog(typeof(ApplicationKernel).Assembly));

                // пользовательские компоненты
                kernel._mainCatalog.Catalogs.Add(new TypeCatalog(_registeredTypes, registrationBuilder));
                foreach (var assembly in _registeredAssemblies)
                {
                    kernel._mainCatalog.Catalogs.Add(new AssemblyCatalog(assembly, registrationBuilder));
                }
                foreach (var registeredPath in _registeredPaths)
                {
                    var attr = File.GetAttributes(registeredPath);
                    if((attr & FileAttributes.Directory) != 0)
                        kernel._mainCatalog.Catalogs.Add(new ModuleRepositoryCatalog(registeredPath, registrationBuilder));
                    else
                        kernel._mainCatalog.Catalogs.Add(new AssemblyCatalog(registeredPath, registrationBuilder));
                }

                kernel._container = new CompositionContainer(new DictinctCatalog(kernel._mainCatalog));
                kernel._componentProvider = new ComponentProvider(kernel._container);
                kernel._moduleManager = new ModuleManager(kernel._mainCatalog, kernel._componentProvider);


                _registeredInstances.AddExportedValue<IModuleManager>(kernel._moduleManager);
                _registeredInstances.AddExportedValue<IComponentProvider>(kernel._componentProvider);

                kernel._container.Compose(_registeredInstances);

                FindMissingDependencies(kernel); //debug

                var modules = kernel._moduleManager.GetModules();
                kernel._moduleManager.EnableModules(modules);
                kernel._moduleManager.ApplyChanges();


                // инициализировать компоненты, требующие предварительной инициализации перед запуском ядра
                var init = kernel.ComponentProvider.GetAll<IStartupInitializable>().ToList();
                init.ForEach(o => o.Initialize());
                
                return kernel;
            }

            private void FindMissingDependencies(ApplicationKernel kernel)
            {
                var importDefinitions = kernel._container.Catalog.Parts
                    .SelectMany(o => o.ImportDefinitions.Select(o1 => new {Part = o, Import = o1}))
                    .GroupBy(o=>o.Import, o=>o.Part, new ImportDefinitionEqByContract());
                    //.SelectMany(o => o.ImportDefinitions).Distinct(new ImportDefinitionEqByContract());

                var missingImports = importDefinitions.Where(o=>!kernel._container.Catalog.GetExports(o.Key).Any());
                if(missingImports.Any())
                    Debug.WriteLine(missingImports.OrderBy(o=>o.Key.ContractName).Aggregate(
                        new StringBuilder().AppendLine("Не найдены экспорты следующих импортов:"),
                        (builder, import) => builder
                            .AppendLine('\t'+ import.Key.ContractName + " для классов:")
                            .Append(import.Distinct(new ComposablePartEqByTypeName()).OrderBy(o=>o.ToString())
                                .Aggregate(new StringBuilder(), (builder2, part) => builder2.AppendLine("\t\t" + part.ToString())))));
            }

            private class ImportDefinitionEqByContract : IEqualityComparer<ImportDefinition>
            {
                public bool Equals(ImportDefinition x, ImportDefinition y)
                {
                    if (x == null && y == null) return true;
                    if (x == null || y == null) return false;
                    return x.ContractName == y.ContractName;
                }

                public int GetHashCode(ImportDefinition obj)
                {
                    return obj.ContractName.GetHashCode();
                }
            }

            private class ComposablePartEqByTypeName : IEqualityComparer<ComposablePartDefinition>
            {
                public bool Equals(ComposablePartDefinition x, ComposablePartDefinition y)
                {
                    if (x == null && y == null) return true;
                    if (x == null || y == null) return false;
                    return x.ToString() == y.ToString();
                }

                public int GetHashCode(ComposablePartDefinition obj)
                {
                    return obj.ToString().GetHashCode();
                }
            }

            /// <summary>
            /// Регистрирует тип в качестве составляющей части создаваемого ядра 
            /// </summary>
            /// <typeparam name="T"></typeparam>
            public void RegisterType<T>() where T : class
            {
                var type = typeof(T);
                if (!_registeredTypes.Contains(type))
                {
                    _registeredTypes.Add(type);
                }
            }

            public void RegisterInstance<T>(T instance)
            {
                if (instance == null) throw new ArgumentNullException("instance");
                _registeredInstances.AddExportedValue<T>(instance);
            }

            /// <summary>
            /// Регистрирует сборку в качестве составляющей части создаваемого ядра
            /// </summary>
            /// <param name="assembly"></param>
            public void RegisterModule(Assembly assembly)
            {
                if (assembly == null) throw new ArgumentNullException("assembly");
                if (!_registeredAssemblies.Contains(assembly)) _registeredAssemblies.Add(assembly);
            }

            /// <summary>
            /// Регистрирует адрес файла сборки, либо путь к директории модуля в качестве составляющей части создаваемого ядра
            /// Путь высчитывается относительно базовой директории домена приложения
            /// </summary>
            /// <param name="path"></param>
            public void RegisterPath(string path)
            {
                var fullPath = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path));
                if (Directory.Exists(fullPath) || File.Exists(fullPath))
                {
                    if (!_registeredPaths.Contains(fullPath)) _registeredPaths.Add(fullPath);
                }
                else
                {
                    throw new Exception(string.Format("Путь \"{0}\"(\"{1}\") не найден в системе", path, fullPath));
                }
            }

            /// <summary>
            /// Получает коллекцию действий для регистрации пользовательских поведений механизма регистрации компонентов ядра
            /// </summary>
            public ICollection<Action<RegistrationBuilder>> Conventions
            {
                get { return _conventions; }
            }
        }
    }
}
