﻿using System;
using System.Threading;

namespace Urish.Framework.Composition
{
    internal sealed class Lock : IDisposable
    {
        private int isDisposed;
        private ReaderWriterLockSlim _thisLock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

        public void Dispose()
        {
            if (Interlocked.CompareExchange(ref isDisposed, 1, 0) == 0)
            {
                _thisLock.Dispose();
            }
        }

        public void EnterReadLock()
        {
            _thisLock.EnterReadLock();
        }

        public void EnterWriteLock()
        {
            _thisLock.EnterWriteLock();
        }

        public void ExitReadLock()
        {
            _thisLock.ExitReadLock();
        }

        public void ExitWriteLock()
        {
            _thisLock.ExitWriteLock();
        }
    }
}
