﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Linq;

namespace Urish.Framework.Composition
{
    public class DictinctCatalog : ComposablePartCatalog
    {
        private ComposablePartCatalog innerCatalog;

        public DictinctCatalog(ComposablePartCatalog innerCatalog)
        {
            if (innerCatalog == null) 
                throw new ArgumentNullException("innerCatalog");
            this.innerCatalog = innerCatalog;
        }

        public override IQueryable<ComposablePartDefinition> Parts
        {
            get { return innerCatalog.Parts/*.Distinct(new ComposablePartDefinitionEqualityComparer())*/; }
        }

        public override IEnumerable<Tuple<ComposablePartDefinition, ExportDefinition>> GetExports(ImportDefinition definition)
        {
            var tuples = innerCatalog.GetExports(definition).ToList();
            var exports = tuples.Distinct(new TupleEqComparer()).ToList();
            return exports;
        }

        private class ComposablePartDefinitionEqualityComparer : IEqualityComparer<ComposablePartDefinition>
        {
            public bool Equals(ComposablePartDefinition x, ComposablePartDefinition y)
            {
                if (x == null && y == null) return true;
                if (x != null && y != null)
                {
                    return x.ToString() == y.ToString();
                }
                return false;
            }

            public int GetHashCode(ComposablePartDefinition obj)
            {
                return obj.GetHashCode();
            }
        }

        private class ExportDefinitionEqualityComparer : IEqualityComparer<ExportDefinition>
        {
            public bool Equals(ExportDefinition x, ExportDefinition y)
            {
                if (x == null && y == null) return true;
                if (x != null && y != null)
                {
                    return x.ContractName == y.ContractName;
                }
                return false;
            }

            public int GetHashCode(ExportDefinition obj)
            {
                return obj.GetHashCode();
            }
        }

        private class TupleEqComparer : IEqualityComparer<Tuple<ComposablePartDefinition, ExportDefinition>>
        {
            private readonly ComposablePartDefinitionEqualityComparer cpdec = 
                new ComposablePartDefinitionEqualityComparer();
            private readonly ExportDefinitionEqualityComparer edec =
                new ExportDefinitionEqualityComparer();

            public bool Equals(Tuple<ComposablePartDefinition, ExportDefinition> x, Tuple<ComposablePartDefinition, ExportDefinition> y)
            {
                return cpdec.Equals(x.Item1, y.Item1) && edec.Equals(x.Item2, y.Item2);
            }

            public int GetHashCode(Tuple<ComposablePartDefinition, ExportDefinition> obj)
            {
                return obj.Item1.ToString().GetHashCode() + obj.Item2.ContractName.GetHashCode();
            }
        }
    }
}
