﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using Urish.Framework.Komiac.Linq;
using Urish.Framework.Model;

namespace Urish.Framework.Composition
{
    /// <summary>
    /// Реализация каталога MEF для подключения компонентов со специализированной структурой
    /// </summary>
    public class ModuleCatalog : ComposablePartCatalog, INotifyComposablePartCatalogChanged, ICompositionElement
    {
        public const string ModuleConfigFile = "module.config";
        public const string MobuleBinDir = "bin";

        private string _modulePath;
        private string _fullModulePath;
        private string _configPath;
        private string _binPath;
        private readonly ReflectionContext _reflectionContext;
        private readonly ICompositionElement _definitionOrigin;
        private AggregateCatalog _binDirectoryAggregateCatalog;
        private ModuleEntryImpl _moduleEntry;
        private bool _isLoaded = false;

        public event EventHandler<ComposablePartCatalogChangeEventArgs> Changed;
        public event EventHandler<ComposablePartCatalogChangeEventArgs> Changing;

        public ModuleCatalog(string modulePath)
        {
            Initialize(modulePath);
        }

        public ModuleCatalog(string modulePath, ICompositionElement definitionOrigin)
        {
            if (string.IsNullOrWhiteSpace(modulePath))
                throw new ArgumentException("Параметр modulePath не задан, либо задан неверно",
                    "modulePath");
            _definitionOrigin = definitionOrigin;
            Initialize(modulePath);
        }

        public ModuleCatalog(string modulePath, ReflectionContext reflectionContext)
        {
            if (reflectionContext == null) throw new ArgumentNullException("reflectionContext");
            if (string.IsNullOrWhiteSpace(modulePath))
                throw new ArgumentException("Параметр modulePath не задан, либо задан неверно",
                    "modulePath");
            _reflectionContext = reflectionContext;
            Initialize(modulePath);
        }

        public ModuleCatalog(string modulePath, ReflectionContext reflectionContext,
            ICompositionElement definitionOrigin)
        {
            if (reflectionContext == null) throw new ArgumentNullException("reflectionContext");
            if (definitionOrigin == null) throw new ArgumentNullException("definitionOrigin");
            if (string.IsNullOrWhiteSpace(modulePath))
                throw new ArgumentException("Параметр modulePath не задан, либо задан неверно",
                    "modulePath");
            _reflectionContext = reflectionContext;
            _definitionOrigin = definitionOrigin;
            Initialize(modulePath);
        }

        public void SetModuleState(ModuleState state)
        {
            _moduleEntry.SetState(state);
        }

        protected override void Dispose(bool disposing)
        {
            // todo
            base.Dispose(disposing);
        }

        private void Initialize(string modulePath)
        {
            _modulePath = modulePath;
            _fullModulePath = GetFullPath(modulePath);
            _configPath = Path.Combine(FullModulePath, ModuleConfigFile);
            _binPath = Path.Combine(FullModulePath, MobuleBinDir);
            
            // проверить, имеется ли в папке файл module.config
            if (!File.Exists(_configPath))
            {
                throw new Exception(
                    string.Format("Путь \"{0}\" не содержит файл манифеста модуля {1}",
                        _fullModulePath, ModuleConfigFile));
            }
            // проверить наличие папки bin
            if(!Directory.Exists(_binPath))
            {
                throw new Exception(string.Format(
                    "Путь \"{0}\" не содержит каталога {1} с програмными библиотеками модуля",
                    _fullModulePath, MobuleBinDir));
            }
            // считать файл module.config, получить метаданные модуля ModuleInfo
            var configFile = File.OpenRead(_configPath);
            var xmlSerializer = new XmlSerializer(typeof(ModuleEntry));
            var moduleEntry = xmlSerializer.Deserialize(configFile) as ModuleEntry;
            if (moduleEntry != null)
            {
                _moduleEntry = new ModuleEntryImpl
                {
                    Info = moduleEntry.Info,
                    RootDirectory = _fullModulePath
                };
                _moduleEntry.SetState(ModuleState.Disabled);
            }
            else
            {
                throw new Exception("Не удалось считать файл " + _configPath);
            }

            // попытаться загрузить в DirectoryCatalog перечисленные в config сборки из bin
            var binDirectoryCatalogs = moduleEntry.ExportedAssemblies.EmptyIfNull()
                .Select(o => o.Name + ".dll")
                .Select(o => (_reflectionContext != null)
                    ? new DirectoryCatalog(_binPath, o, _reflectionContext, this)
                    : new DirectoryCatalog(_binPath, o, this));
            _binDirectoryAggregateCatalog = new AggregateCatalog(binDirectoryCatalogs);
        }

        private static string GetFullPath(string path)
        {
            if (!Path.IsPathRooted(path))
            {
                path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path);
            }
            return Path.GetFullPath(path);
        }

        private string GetDisplayName()
        {
            return string.Format(CultureInfo.CurrentCulture,
                "{0} (Id=\"{1}\" Name=\"{2}\" Path=\"{3}\")", GetType().Name, Entry.Info.Id.ToString("D"),
                Entry.Info.Name, Entry.RootDirectory);
        }

        public override IEnumerator<ComposablePartDefinition> GetEnumerator()
        {
            return _binDirectoryAggregateCatalog.GetEnumerator();
        }

        public override IEnumerable<Tuple<ComposablePartDefinition, ExportDefinition>> GetExports(ImportDefinition definition)
        {
            return _binDirectoryAggregateCatalog.GetExports(definition);
        }

        public override string ToString()
        {
            return GetDisplayName();
        }

        string ICompositionElement.DisplayName {
            get { return GetDisplayName(); }
        }

        ICompositionElement ICompositionElement.Origin {
            get { return _definitionOrigin; }
        }

        public string FullModulePath
        {
            get { return _fullModulePath; }
        }

        public ReadOnlyCollection<string> LoadedFiles
        {
            get
            {
                return new ReadOnlyCollection<string>(
                    _binDirectoryAggregateCatalog.Catalogs.Cast<DirectoryCatalog>()
                        .SelectMany(o => o.LoadedFiles).ToList());
            }
        }

        public string ModulePath
        {
            get { return _modulePath; }
        }

        public ModuleEntry Entry
        {
            get { return _moduleEntry; }
        }

        private class ModuleEntryImpl : ModuleEntry
        {
            public void SetState(ModuleState state)
            {
                State = state;
            }
        }
    }
}
