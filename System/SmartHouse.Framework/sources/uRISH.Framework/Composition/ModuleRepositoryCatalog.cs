﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Urish.Framework.Composition
{
    /// <summary>
    /// Каталог MEF, выполняющий рекурсивный поиск компонентов по заданному адресу в файловой системе
    /// </summary>
    public class ModuleRepositoryCatalog : ComposablePartCatalog, INotifyComposablePartCatalogChanged, ICompositionElement
    {
        private Dictionary<string, ModuleCatalog> _moduleCatalogs;
        private readonly ICompositionElement _definitionOrigin;
        private readonly ReflectionContext _reflectionContext;
        private ReadOnlyCollection<string> _loadedModules; 
        private string _fullPath;
        private string _path;
        private Lock _thisLock;

        public event EventHandler<ComposablePartCatalogChangeEventArgs> Changed;
        public event EventHandler<ComposablePartCatalogChangeEventArgs> Changing;

        public ModuleRepositoryCatalog(string path)
        {
            if (string.IsNullOrWhiteSpace(path)) throw new ArgumentException("Параметр path не задан, либо задан неверно", "path");
            _thisLock = new Lock();
            Initialize(path);
        }

        public ModuleRepositoryCatalog(string path, ICompositionElement definitionOrigin)
        {

            if (string.IsNullOrWhiteSpace(path)) throw new ArgumentException("Параметр path не задан, либо задан неверно", "path");
            if (definitionOrigin == null) throw new ArgumentNullException("definitionOrigin");

            _thisLock = new Lock();
            _definitionOrigin = definitionOrigin;
            Initialize(path);
        }

        public ModuleRepositoryCatalog(string path, ReflectionContext reflectionContext)
        {
            if (string.IsNullOrWhiteSpace(path)) throw new ArgumentException("Параметр path не задан, либо задан неверно", "path");
            if(reflectionContext == null) throw new ArgumentNullException("reflectionContext");

            _thisLock = new Lock();
            _reflectionContext = reflectionContext;
            Initialize(path);
        }

        public ModuleRepositoryCatalog(string path, ICompositionElement definitionOrigin, ReflectionContext reflectionContext)
        {
            if (string.IsNullOrWhiteSpace(path)) throw new ArgumentException("Параметр path не задан, либо задан неверно", "path");
            if (definitionOrigin == null) throw new ArgumentNullException("definitionOrigin");
            if (reflectionContext == null) throw new ArgumentNullException("reflectionContext");

            _thisLock = new Lock();
            _definitionOrigin = definitionOrigin;
            _reflectionContext = reflectionContext;
            Initialize(path);
        }

        protected override void Dispose(bool disposing)
        {
            // todo
            base.Dispose(disposing);
        }

        public override IEnumerator<ComposablePartDefinition> GetEnumerator()
        {
            return (from def in _moduleCatalogs.SelectMany(o => o.Value) select def).GetEnumerator();
        }

        public override IEnumerable<Tuple<ComposablePartDefinition, ExportDefinition>> GetExports(ImportDefinition definition)
        {
            return
                (from export in _moduleCatalogs.SelectMany(o => o.Value.GetExports(definition))
                    select export);
        }

        private void Initialize(string path)
        {
            _path = path;
            _fullPath = GetFullPath(path);
            _moduleCatalogs = new Dictionary<string, ModuleCatalog>();
            _loadedModules = new ReadOnlyCollection<string>(GetModulesPath());
            foreach (var pathToModule in _loadedModules)
            {
                var catalog = CreateModuleCatalogGuarded(pathToModule);
                if (catalog != null)
                {
                    _moduleCatalogs.Add(pathToModule, catalog);
                }
            }
        }

        public void Refresh()
        {
            // todo
        }

        private ModuleCatalog CreateModuleCatalogGuarded(string pathToModule)
        {
            try
            {
                return (_reflectionContext != null)
                    ? new ModuleCatalog(pathToModule, _reflectionContext, this)
                    : new ModuleCatalog(pathToModule, this);
                // todo
            }
            catch (Exception)
            {
                throw;
            }
            
        }

        private string[] GetModulesPath()
        {
            var modulePaths = new List<string>();
            // найти в папке все подпапки по всей внутренней вложенности с файлами module.config внутри
            var rootDir = new DirectoryInfo(_fullPath);
            SearchModuleDirectories(rootDir, modulePaths);
            return modulePaths.ToArray();
        }

        private void SearchModuleDirectories(DirectoryInfo directory, List<string> findedPaths)
        {
            if (IsModuleDirectory(directory))
            {
                findedPaths.Add(directory.FullName);
            }
            else
            {
                foreach (var subDirectory in directory.EnumerateDirectories())
                {
                    SearchModuleDirectories(subDirectory, findedPaths);
                }
            }
        }

        private bool IsModuleDirectory(DirectoryInfo directory)
        {
            return directory.EnumerateFiles().Any(o => o.Name == ModuleCatalog.ModuleConfigFile);
        }

        private static string GetFullPath(string path)
        {
            if (!System.IO.Path.IsPathRooted(path))
            {
                path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path);
            }
            return System.IO.Path.GetFullPath(path);
        }

        protected virtual void OnChanged(ComposablePartCatalogChangeEventArgs e)
        {
            EventHandler<ComposablePartCatalogChangeEventArgs> changed = Changed;
            if (changed != null)
            {
                changed(this, e);
            }
        }

        protected virtual void OnChanging(ComposablePartCatalogChangeEventArgs e)
        {
            EventHandler<ComposablePartCatalogChangeEventArgs> changing = Changing;
            if (changing != null)
            {
                changing(this, e);
            }
        }

        public override string ToString()
        {
            return GetDisplayName();
        }

        public string FullPath
        {
            get { return _fullPath; }
        }

        public string Path
        {
            get { return _path; }
        }

        string ICompositionElement.DisplayName {
            get { return GetDisplayName(); }
        }

        private string GetDisplayName()
        {
            return string.Format(CultureInfo.CurrentCulture, "{0} (Path=\"{1}\")", GetType().Name, _path);
        }

        ICompositionElement ICompositionElement.Origin {
            get { return _definitionOrigin; }
        }

        public ReadOnlyCollection<ModuleCatalog> ModuleCatalogs
        {
            get
            {
                return _moduleCatalogs.Select(o => o.Value)
                    .Where(o => o != null).ToList().AsReadOnly();
            }
        }
    }
}
