﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace Urish.Framework.Web
{
    /// <summary>
    /// Базовый класс провайдера маршрутов роутинга MVC контроллеров в модулях
    /// </summary>
    public class ComponentRouteProvider : IRouteProvider
    {
        private RouteCollection _routes = new RouteCollection();

        void IRouteProvider.FillRoutes(RouteCollection routes)
        {
            foreach (var route in _routes) routes.Add(route);
        }

        /// <summary>
        /// Регистрирует маршруты контроллера, заданные атрибутами AttributeRouting
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        public void MapController<TController>() where TController : IController
        {
            var config = new ComponentConfiguration();
            config.AddRoutesFromController<TController>();
            _routes.MapAttributeRoutes(config);
        }

        public void MapRoute(string name, string url)
        {
            _routes.MapRoute(name, url);
        }

        public void MapRoute(string name, string url, object defaults)
        {
            _routes.MapRoute(name, url, defaults);
        }

        public void MapRoute(string name, string url, string[] namespaces)
        {
            _routes.MapRoute(name, url, namespaces);
        }

        public void MapRoute(string name, string url, object defaults, object constraints)
        {
            _routes.MapRoute(name, url, defaults, constraints);
        }

        public void MapRoute(string name, string url, object defaults, string[] namespaces)
        {
            _routes.MapRoute(name, url, defaults, namespaces);
        }

        public void MapRoute(string name, string url, object defaults, object constraints, string[] namespaces)
        {
            _routes.MapRoute(name, url, defaults, constraints, namespaces);
        }

        public void IgnoreRoute(string url)
        {
            _routes.IgnoreRoute(url);
        }

        public void IgnoreRoute(string url, object constraints)
        {
            _routes.IgnoreRoute(url, constraints);
        }

        private class ComponentConfiguration : Configuration
        {
            public override Type FrameworkControllerType
            {
                get { return typeof(IController); }
            }
        }
    }
}
