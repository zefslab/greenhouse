﻿using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Urish.Framework.Web
{
    public class ApiRouteProvider : IApiRouteProvider
    {
        private HttpRouteCollection _routes = new HttpRouteCollection();

        void IApiRouteProvider.FillApiRoutes(HttpRouteCollection routes)
        {
            foreach (var route in _routes) routes.Add("", route);
        }

        /// <summary>
        /// Регистрирует маршруты api контроллера, заданные атрибутами AttributeRouting
        /// </summary>

        public void MapApiController<TController>() where TController : IHttpController
        {
            var configuration = new Configuration();
            configuration.AddRoutesFromController<TController>();
            _routes.MapHttpAttributeRoutes(configuration);
            var routes = new RouteBuilder(configuration).BuildAllRoutes().Cast<HttpAttributeRoute>().ToList();
            routes.ForEach(r => _routes.Add(r.RouteName, r));

        }

        public void MapApiRoute(string name, string url)
        {
            _routes.MapHttpRoute(name, url);
        }

        public void MapApiRoute(string name, string url, object defaults)
        {
            _routes.MapHttpRoute(name, url, defaults);
        }

        public void MapApiRoute(string name, string url, string[] namespaces)
        {
            _routes.MapHttpRoute(name, url, namespaces);
        }

        public void MapApiRoute(string name, string url, object defaults, object constraints)
        {
            _routes.MapHttpRoute(name, url, defaults, constraints);
        }

        public void MapApiRoute(string name, string url, object defaults, string[] namespaces)
        {
            _routes.MapHttpRoute(name, url, defaults, namespaces);
        }

        public void MapApiRoute(string name, string url, object defaults, object constraints, HttpMessageHandler handlers)
        {
            _routes.MapHttpRoute(name, url, defaults, constraints, handlers);
        }

        public void IgnoreRoute(string name, string url)
        {
            _routes.IgnoreRoute(name, url);
        }

        public void IgnoreRoute(string name, string url, object constraints)
        {
            _routes.IgnoreRoute(name, url, constraints);
        }

        private class Configuration : HttpWebConfiguration
        {
            public Configuration()
            {
                InMemory = false;
                AutoGenerateRouteNames = true;
            }
            public override Type FrameworkControllerType
            {
                get { return typeof(IHttpController); }
            }
        }
    }
}
