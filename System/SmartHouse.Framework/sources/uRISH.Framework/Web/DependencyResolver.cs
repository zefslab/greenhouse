﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Dependencies;
using IDependencyResolver = System.Web.Mvc.IDependencyResolver;

namespace Urish.Framework.Web
{
    /// <summary>
    /// Класс, предоставляющий механизм разрешения зависимостей контроллеров MVC с помощью MEF контейнера
    /// </summary>
    public class DependencyResolverWrapper : IDependencyResolver, System.Web.Http.Dependencies.IDependencyResolver
    {
        private readonly IDependencyResolver _dependencyResolver;
        private readonly System.Web.Http.Dependencies.IDependencyResolver _dependencyResolver2; 
        private readonly IComponentProvider _componentProvider;

        public DependencyResolverWrapper(IComponentProvider componentProvider, 
            IDependencyResolver dependencyResolver)
        {
            if (componentProvider == null)
            { throw new ArgumentNullException("componentProvider"); }
            if (dependencyResolver == null)
            { throw new ArgumentNullException("dependencyResolver"); }

            _componentProvider = componentProvider;
            _dependencyResolver = dependencyResolver;
        }

        public DependencyResolverWrapper(IComponentProvider componentProvider,
            System.Web.Http.Dependencies.IDependencyResolver dependencyResolver)
        {
            if (componentProvider == null) 
                throw new ArgumentNullException("componentProvider");
            if (dependencyResolver == null) 
                throw new ArgumentNullException("dependencyResolver");

            _componentProvider = componentProvider;
            _dependencyResolver2 = dependencyResolver;
        }

        public object GetService(Type serviceType)
        {
            var service = _componentProvider.GetOneOrDefault(serviceType);
            if (service != null) return service;

            service = _dependencyResolver != null ? _dependencyResolver.GetService(serviceType) : null;
            if (service != null) return service;

            service = _dependencyResolver2 != null ? _dependencyResolver2.GetService(serviceType) : null;
            return service;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            var services = _componentProvider.GetAll(serviceType).ToList().AsEnumerable();
            if (services.Any()) return services;

            services = _dependencyResolver != null
                ? _dependencyResolver.GetServices(serviceType)
                : Enumerable.Empty<object>();
            if (services != null && services.Any()) return services;

            services = _dependencyResolver2 != null
                ? _dependencyResolver2.GetServices(serviceType)
                : Enumerable.Empty<object>();
            return services;
        }

        public IDependencyScope BeginScope()
        {
            return this;
        }

        public void Dispose()
        {
        }
    }
}
