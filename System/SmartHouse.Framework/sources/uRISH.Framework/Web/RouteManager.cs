﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Web.Http;
using System.Web.Routing;
using Urish.Framework.Komiac.Extensions;

namespace Urish.Framework.Web
{
    /// <summary>
    /// Сервис, управляющий сбором маршрутов для подсистемы маршрутизации MVC
    /// </summary>
    [Export(typeof(IStartupInitializable))]
    [Export(typeof(RouteManager))]
    public class RouteManager : IPartImportsSatisfiedNotification, IStartupInitializable
    {
        [ImportMany(typeof(IRouteProvider), AllowRecomposition = true)]
        private readonly ICollection<IRouteProvider> _routeProviders = new List<IRouteProvider>();

        [ImportMany(typeof(IApiRouteProvider), AllowRecomposition = true)]
        private readonly ICollection<IApiRouteProvider> _apiRouteProviders = new List<IApiRouteProvider>();

        private readonly IEnumerable<RouteBase> _preDefRoutes;

        public RouteManager()
        {
            _preDefRoutes = RouteTable.Routes.ToList();
        }

        public void OnImportsSatisfied()
        {
            UpdateRoutes();
        }

        // todo UpdateRoutes нужно вызывать при изменении состояний модулей, а не только при импорте провайдеров,
        // провайдеры следует оставить для возможности задавать маршруты вручную
        private void UpdateRoutes()
        {
            using (RouteTable.Routes.GetWriteLock())
            {
                var updatedConfiguration = new HttpConfiguration(new HttpRouteCollection());
                updatedConfiguration.MapHttpAttributeRoutes();
                updatedConfiguration.EnsureInitialized();
                var updatedRoutes = updatedConfiguration.Routes.ToRouteDictionary();

                // todo предустановленные маршруты стираются, нужно из восстанавливать
                GlobalConfiguration.Configuration.Routes.Clear();
                foreach (var route in updatedRoutes)
                {
                    GlobalConfiguration.Configuration.Routes.Add(route.Key, route.Value);
                }

                foreach (var defRoute in _preDefRoutes)
                {
                    RouteTable.Routes.Add(defRoute);
                }
            }
        }

        public void Initialize()
        {
            UpdateRoutes();
        }
    }
}
