﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;

namespace Urish.Framework.Web
{
    /// <summary>
    /// Фабрика контроллеров MVC, получающая экземпляры из MEF контейнера
    /// </summary>
    public class ControllerFactory : DefaultControllerFactory
    {
        private readonly IComponentProvider _componentProvider;

        public ControllerFactory(IComponentProvider componentProvider)
        {
            if (componentProvider == null)
                throw new ArgumentNullException("componentProvider");
            _componentProvider = componentProvider;
        }

        /// <summary>
        /// Возвращает инстанцию контроллера по его типу
        /// </summary>
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if(controllerType == null)
                throw new ArgumentNullException("controllerType");

            var controller = _componentProvider.GetOneOrDefault(controllerType) as IController;
            return controller ?? base.GetControllerInstance(requestContext, controllerType);
        }

        /// <summary>
        /// Возвращает тип контроллера по его имени и данных роутинга запроса
        /// </summary>
        protected override Type GetControllerType(RequestContext requestContext, string controllerName)
        {
            if(requestContext == null)
                throw new ArgumentNullException("requestContext");
            if(controllerName == null)
                throw new ArgumentNullException("controllerName");

            // получить из роута данные о пространстве имён контроллера
            var routeData = requestContext.RouteData;
            object routeNamespacesObj;
            if (routeData != null && routeData.DataTokens.TryGetValue("Namespaces", out routeNamespacesObj))
            {
                var routeNamespaces = routeNamespacesObj as IEnumerable<string>;
                if (routeNamespaces != null && routeNamespaces.Any())
                {
                    var namespaceHash = new HashSet<string>(routeNamespaces, StringComparer.OrdinalIgnoreCase);
                    var matchType = GetMatchedTypeFromComponentProvider(routeData.Route, controllerName, namespaceHash);
                    if (matchType != null) return matchType;
                }
            }
            
            return base.GetControllerType(requestContext, controllerName);
        }

        private Type GetMatchedTypeFromComponentProvider(RouteBase route, string controllerName, IEnumerable<string> namespaces)
        {
            // получить FullName имена типов контроллеров
            var fullNames = namespaces.Select(o => string.Concat(o, ".", controllerName, "Controller")).ToArray();
            // найти в контейнере соответствующий имени контроллера и пространствам имён тип контроллерая
            var matchTypes = fullNames.SelectMany(o => _componentProvider.FindTypesForContract(o)).ToArray();
            // если он только один, вернуть его, иначе ошибка
            switch (matchTypes.Length)
            {
                case 0:
                    return null;
                case 1:
                    return matchTypes[0];
                default:
                    throw CreateAmbiguousControllerException(route, controllerName, matchTypes);
            }
        }

        internal static InvalidOperationException CreateAmbiguousControllerException(RouteBase route, string controllerName, ICollection<Type> matchingTypes)
        {
            // we need to generate an exception containing all the controller types
            var typeList = new StringBuilder();
            foreach (Type matchedType in matchingTypes)
            {
                typeList.AppendLine();
                typeList.Append(matchedType.FullName);
            }

            string errorText;
            var castRoute = route as Route;
            if (castRoute != null)
            {
                errorText = String.Format(
                    CultureInfo.CurrentCulture,
                    "Multiple types were found that match the controller named '{0}'. "
                    + "This can happen if the route that services this request ('{1}') does not specify namespaces to search for a controller that matches the request. "
                    + "If this is the case, register this route by calling an overload of the 'MapRoute' method that takes a 'namespaces' parameter.{3}{3}"
                    + "The request for '{0}' has found the following matching controllers:{2}",
                    controllerName,
                    castRoute.Url,
                    typeList,
                    Environment.NewLine);
            }
            else
            {
                errorText = String.Format(
                    CultureInfo.CurrentCulture,
                    "Multiple types were found that match the controller named '{0}'. "
                    + "This can happen if the route that services this request does not specify namespaces to search for a controller that matches the request. "
                    + "If this is the case, register this route by calling an overload of the 'MapRoute' method that takes a 'namespaces' parameter.{2}{2}"
                    + "The request for '{0}' has found the following matching controllers:{1}",
                    controllerName,
                    typeList,
                    Environment.NewLine);
            }

            return new InvalidOperationException(errorText);
        }
    }
}
