﻿using System.Web.Http;
using System.Web.Routing;

namespace Urish.Framework.Web
{
    public interface IRouteProvider
    {
        /// <summary>
        /// При реализации заполняет коллекцию маршрутов MVC данными о маршрутах к контроллерам модуля
        /// </summary>
        void FillRoutes(RouteCollection routes);
    }

    public interface IApiRouteProvider
    {
        /// <summary>
        /// При реализации заполняет коллекцию маршрутов WebAPI данными о маршрутах к контроллерам модуля
        /// </summary>
        void FillApiRoutes(HttpRouteCollection routes);
    }
}
