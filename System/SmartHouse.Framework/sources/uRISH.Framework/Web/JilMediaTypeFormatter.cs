﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Urish.Framework.Web
{
    public class JilMediaTypeFormatter : JsonMediaTypeFormatter
    {
        private readonly Options _jilOptions;

        public JilMediaTypeFormatter()
        {
            _jilOptions = new Options(dateFormat:DateTimeFormat.ISO8601);
            SupportedEncodings.Add(new UTF8Encoding(false, true));
            SupportedEncodings.Add(new UnicodeEncoding(false, true, true));
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));
        }
        public override bool CanReadType(Type type)
        {
            if(type == null)
                throw new ArgumentNullException("type");
            return true;
        }

        public override bool CanWriteType(Type type)
        {
            if (type == null) 
                throw new ArgumentNullException("type");
            return true;
        }

        public override Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
            return Task.FromResult(DeserializeFromStream(type, readStream));
        }

        private object DeserializeFromStream(Type type, Stream readStream)
        {
            try
            {
                using (var reader = new StreamReader(readStream))
                {
                    var method = typeof (JSON).GetMethod("Deserialize",
                        new [] {typeof (TextReader), typeof (Options)});
                    var generic = method.MakeGenericMethod(type);
                    return generic.Invoke(this, new object[] {reader, _jilOptions});
                }
            }
            catch
            {
                return null;
            }
        }

        public override Task WriteToStreamAsync(Type type, object value, Stream writeStream, HttpContent content,
            TransportContext transportContext)
        {
            using (var streamWriter = new StreamWriter(writeStream))
            {
                JSON.Serialize(value, streamWriter, _jilOptions);
                return Task.FromResult(writeStream);
            }
        }
    }
}
