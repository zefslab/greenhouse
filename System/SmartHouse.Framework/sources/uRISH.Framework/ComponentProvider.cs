﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.ComponentModel.Composition.ReflectionModel;
using System.Linq;

namespace Urish.Framework
{
    /// <summary>
    /// Класс доступа к содержимому контейнера MEF ядра
    /// Предоставляет методы для запроса классов компонентов по их типам, типам и/или именам контрактов
    /// </summary>
    internal class ComponentProvider : IComponentProvider
    {
        private readonly CompositionContainer _container;

        public ComponentProvider(CompositionContainer container)
        {
            _container = container;
        }

        public T GetOne<T>()
        {
            return _container.GetExportedValue<T>();
        }

        public T GetOne<T>(string contractName)
        {
            return _container.GetExportedValue<T>(contractName);
        }

        public object GetOne(Type componentType)
        {
            var export = _container.GetExports(componentType, null, null).Single();
            return export != null ? export.Value : null;
        }

        public object GetOne(Type componentType, string contractName)
        {
            var export = _container.GetExports(componentType, null, contractName).Single();
            return export != null ? export.Value : null;
        }

        public T GetOneOrDefault<T>()
        {
            return _container.GetExportedValueOrDefault<T>();
        }

        public T GetOneOrDefault<T>(string contractName)
        {
            return _container.GetExportedValueOrDefault<T>(contractName);
        }

        public object GetOneOrDefault(Type componentType)
        {
            var export = _container.GetExports(componentType, null, null).SingleOrDefault();
            return export != null ? export.Value : null;
        }

        public object GetOneOrDefault(Type componentType, string contractName)
        {
            var export = _container.GetExports(componentType, null, contractName).SingleOrDefault();
            return export != null ? export.Value : null;
        }

        public IEnumerable<T> GetAll<T>()
        {
            return _container.GetExportedValues<T>();
        }

        public IEnumerable<T> GetAll<T>(string contractName)
        {
            return _container.GetExportedValues<T>(contractName);
        }

        public IEnumerable<object> GetAll(Type componentType)
        {
            return _container.GetExports(componentType, null, null).Select(o => o != null ? o.Value : null);
        }

        public IEnumerable<object> GetAll(Type componentType, string contractName)
        {
            return _container.GetExports(componentType, null, contractName).Select(o => o != null ? o.Value : null);
        }

        public Lazy<T> RequestOne<T>()
        {
            return _container.GetExport<T>();
        }

        public Lazy<T> RequestOne<T>(string contractName)
        {
            return _container.GetExport<T>(contractName);
        }

        public Lazy<object> RequestOne(Type componentType)
        {
            return _container.GetExports(componentType, null, null).Single();
        }

        public Lazy<object> RequestOne(Type componentType, string contractName)
        {
            return _container.GetExports(componentType, null, contractName).Single();
        }

        public IEnumerable<Lazy<T>> RequestAll<T>()
        {
            return _container.GetExports<T>();
        }

        public IEnumerable<Lazy<T>> RequestAll<T>(string contractName)
        {
            return _container.GetExports<T>(contractName);
        }

        public IEnumerable<Lazy<object>> RequestAll(Type componentType)
        {
            return _container.GetExports(componentType, null, null);
        }

        public IEnumerable<Lazy<object>> RequestAll(Type componentType, string contractName)
        {
            return _container.GetExports(componentType, null, contractName);
        }

        public Type FindTypeForContract(Type contractType)
        {
            return
                _container.Catalog.Parts.Select(o => ComposablePartExportType(o, contractType.FullName))
                    .First(t => t != null);
        }

        public Type FindTypeForContract(string contractName)
        {
            return
                _container.Catalog.Parts.Select(o => ComposablePartExportType(o, contractName))
                    .First(t => t != null);
        }

        public IEnumerable<Type> FindTypesForContract(Type contractType)
        {
            return
                _container.Catalog.Parts.Select(o => ComposablePartExportType(o, contractType.FullName))
                    .Where(t => t != null)
                    .ToList();
        }

        public IEnumerable<Type> FindTypesForContract(string contractName)
        {
            return
                _container.Catalog.Parts.Select(o => ComposablePartExportType(o, contractName))
                    .Where(t => t != null)
                    .ToList();
        }

        private Type ComposablePartExportType(ComposablePartDefinition part, string contractName)
        {
            return
                part.ExportDefinitions.Any(
                    definition =>
                    definition.Metadata.ContainsKey("ExportTypeIdentity")
                    && definition.Metadata["ExportTypeIdentity"].Equals(contractName))
                    ? ReflectionModelServices.GetPartType(part).Value.UnderlyingSystemType
                    : null;
        }
    }
}
