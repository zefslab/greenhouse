﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web;

namespace Urish.Framework.Bootstrap
{
    /// <summary>
    /// Класс, выполняющий асинхронную инициализацию модульного ядра при запуске его в Web среде
    /// </summary>
    public class WarmupHttpModule :  IHttpModule
    {
        private HttpApplication _context;
        private static object _synLock = new object();
        private static IList<Action> _awaitingActions = new List<Action>();

        public static void StartWarmupMode()
        {
            lock (_synLock)
            {
                if(_awaitingActions == null)
                    _awaitingActions = new List<Action>();
            }
        }

        public static void CompleteWarmupMode()
        {
            IList<Action> temp;
            lock (_synLock)
            {
                temp = _awaitingActions;
                _awaitingActions = null;
            }
            if(temp != null)
                foreach (var action in temp)
                    action();
        }

        private static bool IsWarmupMode
        {
            get
            {
                lock (_synLock)
                {
                    return _awaitingActions != null;
                }
            }
        }

        public void Init(HttpApplication context)
        {
            _context = context;
            _context.AddOnBeginRequestAsync(BeginRequest_BeginEventHandle, BeginRequest_EndEventHandle, null);
        }

        private IAsyncResult BeginRequest_BeginEventHandle(object sender, EventArgs e, AsyncCallback callback, object extradata)
        {
            if (!IsWarmupMode)
            {
                var asyncResult = new DoneAsyncResult(extradata);
                callback(asyncResult);
                return asyncResult;
            }
            else
            {
                var asyncResult = new WarmupAsyncResult(callback, extradata);
                Action temp = asyncResult.Completed;
                lock (_synLock)
                {
                    if (IsWarmupMode)
                    {
                        temp = null;
                        _awaitingActions.Add(asyncResult.Completed);
                    }
                }
                if (temp != null)
                    temp();
                return asyncResult;
            }
        }

        private void BeginRequest_EndEventHandle(IAsyncResult ar)
        {
        }

        public void Dispose()
        {
        }

        private class WarmupAsyncResult : IAsyncResult
        {
            private readonly EventWaitHandle _eventWaitHandle = new AutoResetEvent(false/*initialState*/);
            private readonly AsyncCallback _cb;
            private readonly object _asyncState;
            private bool _isCompleted;

            public WarmupAsyncResult(AsyncCallback cb, object asyncState)
            {
                _cb = cb;
                _asyncState = asyncState;
                _isCompleted = false;
            }

            public void Completed()
            {
                _isCompleted = true;
                _eventWaitHandle.Set();
                _cb(this);
            }

            bool IAsyncResult.CompletedSynchronously
            {
                get { return false; }
            }

            bool IAsyncResult.IsCompleted
            {
                get { return _isCompleted; }
            }

            object IAsyncResult.AsyncState
            {
                get { return _asyncState; }
            }

            WaitHandle IAsyncResult.AsyncWaitHandle
            {
                get { return _eventWaitHandle; }
            }
        }

        private class DoneAsyncResult : IAsyncResult
        {
            private readonly object _asyncState;
            private static readonly WaitHandle _waitHandle = new ManualResetEvent(true/*initialState*/);

            public DoneAsyncResult(object asyncState)
            {
                _asyncState = asyncState;
            }

            bool IAsyncResult.CompletedSynchronously
            {
                get { return true; }
            }

            bool IAsyncResult.IsCompleted
            {
                get { return true; }
            }

            WaitHandle IAsyncResult.AsyncWaitHandle
            {
                get { return _waitHandle; }
            }

            object IAsyncResult.AsyncState
            {
                get { return _asyncState; }
            }
        }
    }
}
