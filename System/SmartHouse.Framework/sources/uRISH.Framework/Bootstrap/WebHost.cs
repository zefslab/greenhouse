﻿using System;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Urish.Framework.Web;

namespace Urish.Framework.Bootstrap
{
    /// <summary>
    /// Класс интеграции модульного ядра в Web приложение ASP.NET
    /// </summary>
    public class WebHost
    {
        private volatile ApplicationKernel _applicationKernel;
        private volatile Exception _initializationException;
        private volatile Exception _previousInitializationException;

        private readonly object _synLock = new object();

        public void OnApplicationStart(HttpApplication context)
        {
            StartInitializationThread(context);
        }

        public void OnBeginRequest(HttpApplication context)
        {
            if (_initializationException != null)
            {
                var restartInitialization = false;
                lock (_synLock)
                {
                    if (_initializationException != null)
                    {
                        _previousInitializationException = _initializationException;
                        _initializationException = null;
                        restartInitialization = true;
                    }
                }

                if (restartInitialization)
                {
                    StartInitializationThread(context);
                }
            }

            if (_previousInitializationException != null)
            {
                throw new ApplicationException("Error during application initialization",
                    _previousInitializationException);
            }

            if (_applicationKernel != null)
            {
                BeginRequest(context);
            }
        }

        public void OnEndRequest(HttpApplication context)
        {
            if (_applicationKernel != null)
            {
                EndRequest(context);
            }
        }

        private void BeginRequest(HttpApplication context)
        {
            //throw new NotImplementedException();
        }

        private void EndRequest(HttpApplication context)
        {
            //throw new NotImplementedException();
        }

        private void StartInitializationThread(HttpApplication context)
        {
            WarmupHttpModule.StartWarmupMode();
            ThreadPool.QueueUserWorkItem(state =>
            {
                try
                {
                    _applicationKernel = InitializeApplicationKernel(context);
                }
                catch (Exception exception)
                {
                    lock (_synLock)
                    {
                        _initializationException = exception;
                        _previousInitializationException = null;
                    }
                }
                finally
                {
                    WarmupHttpModule.CompleteWarmupMode();
                }
            });
        }

        private ApplicationKernel InitializeApplicationKernel(HttpApplication context)
        {
            var builder = new ApplicationKernel.Builder();
            PreInitializeApplicationInternal(builder);
            PreInitializeApplication(builder);
            var kernel = builder.Build();
            PostInitializeApplicationInternal(kernel);
            PostInitializeApplication(kernel);
            return kernel;
        }

        private void PreInitializeApplicationInternal(ApplicationKernel.Builder builder)
        {
            builder.RegisterInstance<IPrincipal>(new FakePrincipal()); // todo надо будет добавлять юзера правильно!!!
            builder.RegisterInstance<HttpConfiguration>(GlobalConfiguration.Configuration);

            // предписывает контейнеру создавать новые экземпляры MVC контроллеров при каждом обращении к нему.
            //builder.Conventions.Add(
            //    registrationBuilder =>
            //    registrationBuilder.ForTypesDerivedFrom<IController>()
            //        .SetCreationPolicy(CreationPolicy.NonShared));
        }

        private void PostInitializeApplicationInternal(ApplicationKernel kernel)
        {
            ControllerBuilder.Current.SetControllerFactory(new ControllerFactory(kernel.ComponentProvider));
            DependencyResolver.SetResolver(new DependencyResolverWrapper(kernel.ComponentProvider, DependencyResolver.Current));
            GlobalConfiguration.Configuration.DependencyResolver =
                new DependencyResolverWrapper(kernel.ComponentProvider,
                    GlobalConfiguration.Configuration.DependencyResolver);
        }

        protected virtual void PreInitializeApplication(ApplicationKernel.Builder builder)
        {
        }

        protected virtual void PostInitializeApplication(ApplicationKernel kernel)
        {
        }

        private class FakePrincipal : IPrincipal
        {
            public FakePrincipal()
            {
                Identity = null;
            }

            public bool IsInRole(string role)
            {
                return true;
            }

            public IIdentity Identity { get; private set; }
        }
    }
}
